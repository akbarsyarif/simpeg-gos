<div class="menu-heading">.:: Administrator Menu ::.</div>
<div id='cssmenu'>
	<ul>
		<li>
			<a href="javascript:void(0)" onClick="openTab('<?= site_url('dashboard') ?>', 'Dasbor')"><i class="fa fa-fw fa-newspaper-o"></i> Dasbor</a>
		</li>
		<li class="has-sub">
			<a href="javascript:void(0)"><span><i class="fa fa-fw fa-wrench"></i> Pengaturan</span></a>
			<ul>
				<li>
					<a href="javascript:void(0)" onClick="openTab('<?= site_url('settings/pengaturan_umum') ?>', 'Pengaturan Umum')">Pengaturan Umum</a>
				</li>
			</ul>
		</li>
		<li class="has-sub">
			<a href="javascript:void(0)"><span><i class="fa fa-fw fa-database"></i> Data Pokok</span></a>
			<ul>
				<li><a href="javascript:void(0)" onClick="openTab('<?= site_url('golongan/manage_golongan') ?>', 'Golongan')">Golongan</a></li>
				<li><a href="javascript:void(0)" onClick="openTab('<?= site_url('eselon/manage_eselon') ?>', 'Eselon')">Eselon</a></li>
				<li><a href="javascript:void(0)" onClick="openTab('<?= site_url('skpd/manage_skpd') ?>', 'SKPD')">SKPD</a></li>
				<li class="has-sub">
					<a href="javascript:void(0)"><span>Jabatan</span></a>
					<ul>
						<li><a href="javascript:void(0)" onClick="openTab('<?= site_url('jabatan_struktural/manage_jabatan') ?>', 'Jabatan Struktural')">Jabatan Struktural</a></li>
						<li><a href="javascript:void(0)" onClick="openTab('<?= site_url('jabatan_fungsional_umum/tabs') ?>', 'Jabatan Fungsional Umum')">Jabatan Fungsional Umum</a></li>
						<li><a href="javascript:void(0)" onClick="openTab('<?= site_url('jabatan_fungsional_tertentu/tabs') ?>', 'Jabatan Fungsional Tertentu')">Jabatan Fungsional Tertentu</a></li>
					</ul>
				</li>
				<li class="has-sub">
					<a href="javascript:void(0)"><span>Pendidikan</span></a>
					<ul>
						<li><a href="javascript:void(0)" onClick="openTab('<?= site_url('keilmuan/manage_keilmuan') ?>', 'Bidang Keilmuan')">Bidang Keilmuan</a></li>
						<li><a href="javascript:void(0)" onClick="openTab('<?= site_url('jurusan/manage_jurusan') ?>', 'Jurusan')">Jurusan</a></li>
						<li><a href="javascript:void(0)" onClick="openTab('<?= site_url('pendidikan/manage_pendidikan') ?>', 'Pendidikan')">Pendidikan</a></li>
					</ul>
				</li>
			</ul>
		</li>
		<li>
			<a href="javascript:void(0)" onClick="openTab('<?= site_url('pegawai/manage_pegawai') ?>', 'Pegawai')"><i class="fa fa-fw fa-user"></i> Pegawai</a>
		</li>
		<li>
			<a href="javascript:void(0)" onClick="openTab('<?= site_url('users/manage_user') ?>', 'Pengguna')"><i class="fa fa-fw fa-users"></i> Pengguna</a>
		</li>
	</ul>
</div>