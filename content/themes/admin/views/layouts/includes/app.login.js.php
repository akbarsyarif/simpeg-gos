<script type="text/javascript">
	$('#login-form').form({
		url: '<?= site_url("api/users/auth/auth") ?>',
		onSubmit: function() {
			$('#login-form [type="submit"]').button('loading');
		},
		success: function(data) {
			var data = JSON.parse(data);

			if ( data.metadata.code != "200" ) {
				$.messager.alert('Warning', data.metadata.message, 'warning');
				console.log(data);
			}
			else {
				window.location = '<?= site_url("web") ?>';
			}

			$('#login-form [type="submit"]').button('reset');
			$('#login-form').form('clear');
		},
		onLoadError: function(e) {
			$('#login-form [type="submit"]').button('reset');
			$.messager.alert('Error', 'Tidak dapat menghubungi server', 'error');
			console.log(e);
		}
	})
</script>