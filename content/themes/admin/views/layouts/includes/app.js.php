<script type="text/javascript">
	function openTab(url, title) {
		if ( checkTab(title) == false ) {
			$('#main-tabs').tabs('add', {
				title: title,
				href: url,
				closable: true
			});
		} else {
			updateTab(url, title);
			selectTab(title);
		}
	}

	function checkTab(title) {
		return $('#main-tabs').tabs('exists', title);
	}

	function selectTab(title) {
		$('#main-tabs').tabs('select', title);
	}

	function updateTab(url, title) {
		var tab = $('#main-tabs').tabs('getTab', title);
		tab.panel('refresh');
	}

	function logOut() {
		window.location = '<?= site_url('logout') ?>';
	}

	$(document).ready(function() {		
		// custom validation rules
		$.extend($.fn.validatebox.defaults.rules, {
			equals: {
				validator: function(value, param) {
					let a = $(param[0]).val();
					return (value == a);
				},
				message: 'Value does not match'
			}
		});

		// datebox custom format
		$.fn.datebox.defaults.formatter = function(date) {
			var y = date.getFullYear();
			var m = date.getMonth()+1;
			var d = date.getDate();
			return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
		}

		$.fn.datebox.defaults.parser = function(s){
			if (!s) return new Date();
			var ss = (s.split('-'));
			var y = parseInt(ss[0],10);
			var m = parseInt(ss[1],10);
			var d = parseInt(ss[2],10);
			if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
				return new Date(y,m-1,d);
			} else {
				return new Date();
			}
		}

		$.fn.tabs.defaults.onLoad = function() {
			$('.select2-single').select2();
		}
	});
</script>