<!DOCTYPE html>
<html>
	<head>
		<title><?= $app_title ?> | Login</title>
		<!-- css -->
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/bower_components/bootstrap/dist/css/bootstrap-theme.min.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/third_party/jquery-easyui/themes/default/easyui.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/third_party/jquery-easyui/themes/icon.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/bower_components/font-awesome/css/font-awesome.min.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/simpeg.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/login.css') ?>">

		<!-- javascript -->
		<script type="text/javascript" src="<?= base_url('assets/bower_components/jquery/dist/jquery.min.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/third_party/jquery-easyui/jquery.easyui.min.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/bower_components/jquery-form/dist/jquery.form.min.js') ?>"></script>
	</head>
	<body>
		<div id="login-window" 
			class="easyui-window" 
			title="<?= $app_title ?> | <?= $app_description ?>" 
			style="width:550px;height:300px;padding:5px"
			data-options="collapsible:false,minimizable:false,maximizable:false,closable:false,draggable:false,resizable:false,footer:'#footer'">
			<div id="login-window-layout" class="easyui-layout" data-options="fit:true">
				<div data-options="region:'west'" style="width:250px;padding:10px">
					<div class="login-heading">
						<a href="<?= site_url() ?>"><img src="<?= base_url('assets/images/bkd-luwu-heading-1.png') ?>"></a>
					</div>
					Solusi untuk menangani berbagai hal dalam urusan kepegawaian mulai dari penyimpanan dan pemusatan data secara terkomputerisasi sehingga memudahkan dalam meningkatkan kebutuhan administrasi kepegawaian.
				</div>
				<div data-options="region:'center'" style="padding:10px">
					<form id="login-form" method="post">
						<div class="form-group">
							<label>Username</label>
							<input class="easyui-textbox form-control" type="text" name="username" data-options="width:'100%'" />
						</div>
						<div class="form-group">
							<label>Password</label>
							<input class="easyui-passwordbox" name="password" iconWidth="28" data-options="width:'100%'" />
						</div>
						<div class="form-group">
							<div class="checkbox">
								<label><input type="checkbox" name="remember" value="true" /> Ingat saya</label>
							</div>
						</div>
						<button type="submit" class="btn btn-sm btn-default"><i class="fa fa-fw fa-check-circle"></i> Login</button>
					</form>
				</div>
			</div>
		</div>
		<div id="footer" style="padding:5px">
			<span>Copyright &copy; 2017 Lagos Intermedia</span>
			<span class="pull-right"><?= $app_version ?></span>
		</div>

		<!-- includes -->
		<?php require 'includes/app.login.js.php' ?>
	</body>
</script>