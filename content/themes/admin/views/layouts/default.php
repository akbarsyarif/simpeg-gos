<!DOCTYPE html>
<html>
	<head>
		<title><?= $app_title ?></title>
		<!-- css -->
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/bower_components/bootstrap/dist/css/bootstrap-theme.min.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/third_party/jquery-easyui/themes/default/easyui.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/third_party/jquery-easyui/themes/icon.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/bower_components/font-awesome/css/font-awesome.min.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/third_party/cssmenu/cssmenu.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/bower_components/select2/dist/css/select2.min.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/simpeg.css') ?>">

		<!-- javascript -->
		<script type="text/javascript" src="<?= base_url('assets/bower_components/jquery/dist/jquery.min.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/third_party/jquery-easyui/jquery.easyui.min.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/third_party/jquery-easyui/extensions/datagrid-view/datagrid-bufferview.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/third_party/easyui-datagrid-mergeCells-extension-master/easyui-datagrid-mergeEqualCells.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/bower_components/jquery-form/dist/jquery.form.min.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/bower_components/moment/min/moment-with-locales.min.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/third_party/cssmenu/cssmenu.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/bower_components/select2/dist/js/select2.min.js') ?>"></script>
	</head>
	<body>
		<div id="main-layout" class="easyui-layout" fit="true">
			<div id="left-pane" data-options="region:'west',split:false,hideCollapsedContent:false,title:'<i class=\'fa fa-fw fa-users\'></i> <?= $app_title ?> <?= $app_version ?>'" style="width:280px">
				<div id="profile1" style="padding:10px">
					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object" src="<?= base_url('assets/images/logo-luwu.png') ?>" alt="Image">
						</a>
						<div class="media-body">
							<h4 class="media-heading">Administrator</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna.</p>
						</div>
					</div>
				</div>
				<div id="left-menu">
					<?php require 'includes/administrator_menu.php' ?>
				</div>
			</div>
			<div id="right-pane" data-options="region:'center'">
				<div class="easyui-layout" data-options="fit:true">
					<div data-options="region:'north',border:false" style="overflow:hidden">
						<nav id="top-menu" class="navbar navbar-default" role="navigation">
							<div class="pull-right">
								<a href="javascript:void(0)" id="account-menu" class="easyui-menubutton" data-options="menu:'#account-submenu'"><?= $this->ion_auth->user()->row()->name ?></a>
								<div id="account-submenu" style="width:150px;">
									<div onClick="openTab('<?= site_url('users/profile') ?>', 'Akun Saya')">Akun Saya</div>
									<div onClick="logOut()">Keluar</div>
								</div>
							</div>
						</nav>
					</div>
					<div data-options="region:'center',border:false">
						<div id="main-tabs" class="easyui-tabs" data-options="fit:true,border:false">
							<div title="Selamat Datang">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
								consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
								cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
								proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="bottom-pane" data-options="region:'south'" style="overflow:hidden">
				<span><?= $app_title ?> <?= $app_version ?></span>
				<span class="pull-right">Copyright &copy; 2017 Lagos Intermedia</span>
			</div>
		</div>

		<!-- includes -->
		<?php require 'includes/app.js.php' ?>

		<!-- app.js -->
		<script type="text/javascript" src="<?= base_url('assets/js/app.js') ?>"></script>
	</body>
</html>