/**
 * chain select kelurahan
 * @param  {[object]} obj
 *         obj.kecamatan_id : id kecamatan
 *         obj.url : url web service
 *         obj.target : object target to load response
 *         obj.selected : default selected option
 *         obj.beforeSend : an action to call before ajax send
 *         obj.complete : an action to call after ajax send
 * @return {[type]}     [description]
 */
function ajaxDropdown(obj) {
	let target     = obj.target;
	let url        = obj.url;
	let method     = typeof obj.method != 'undefined' ? obj.method : 'GET';
	let dataType   = typeof obj.dataType != 'undefined' ? obj.method : 'JSON';
	let data       = typeof obj.data != 'undefined' ? obj.data : {};
	let emptyFirst = typeof obj.emptyFirst != 'undefined' ? obj.emptyFirst : true;
	let firstLabel = typeof obj.firstLabel != 'undefined' ? obj.firstLabel : '- Pilih -';
	let result     = {value: '', label: ''};
	let selected   = typeof obj.selected != 'undefined' ? obj.selected : '';

	return $.ajax({
		url: url,
		method: method,
		data: data,
		dataType: dataType,
		beforeSend: function(xhr) {
			if (typeof obj.beforeSend === 'function') {
				obj.beforeSend(xhr);
			}
		},
		success: function(response) {
			if (response.metadata.code != 200 || response.response.total == 0) {
				console.log(response);
				alert(response.metadata.message);
				html = emptyFirst ? '<option value="">'+ firstLabel +'</option>' : '';
			} else {
				html = emptyFirst ? '<option value="">'+ firstLabel +'</option>' : '';

				// override response format
				if (typeof obj.processResult === 'object') {
					result = obj.processResult;
				}

				$(target).html(function() {
					let data = response.response;
					for (i=0; i<=data.rows.length - 1; i++) {
						let current_option = data.rows[i];

						let value = current_option[result.value];
						let label = current_option[result.label];

						if (typeof result.label === 'object') {
							label = '';
							for (a=0; a<=result.label.length - 1; a++) {
								label += (a == 0) ? current_option[result.label[a]] : ' - ' + current_option[result.label[a]];
							};
						}

						if (typeof selected != 'undefined') {
							if (selected == value) {
								html += '<option selected value="'+ value +'">'+ label +'</option>';
							} else {
								html += '<option value="'+ value +'">'+ label +'</option>';
							}
						} else {
							html += '<option value="'+ value +'">'+ label +'</option>';
						}
					}

					return html;
				});
			}
		},
		error: function(xhr) {
			console.log(xhr);
			$(target).html('<option value="">'+ firstLabel +'</option>');
		},
		complete: function() {
			if (typeof obj.complete === 'function') {
				obj.complete();
			}
		}
	})
}

$(document).ready(function() {
	$('select').select2();
});