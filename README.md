# SIMPEG GOS #

Sistem Informasi Manajemen Kepegawaian

### Cara Install ###

* Copy isi folder public sesuai nama foldernya masing-masing. Misalnya isi folder config, copy ke folder application/config
* Edit isi filenya sesuai konfigurasi server yang digunakan
* Untuk database menggunakan MySQL, buat database baru dengan nama simpeg (sesuaikan dengan konfigurasi di file database.php)
* Import file simpeg.sql yang terletak di folder sql/simpeg.sql
* Untuk membuka aplikasi akses di alamat http://localhost/simpeg/public
