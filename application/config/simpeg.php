<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Application title
|--------------------------------------------------------------------------
|
| application title
|
*/
$config['app_title'] = 'SIMPEG';

/*
|--------------------------------------------------------------------------
| Application description
|--------------------------------------------------------------------------
|
| application description
|
*/
$config['app_description'] = 'Sistem Informasi Manajemen Kepegawaian';

/*
|--------------------------------------------------------------------------
| Application version
|--------------------------------------------------------------------------
|
| application version number
|
*/
$config['app_version'] = 'v.1.0';

/*
|--------------------------------------------------------------------------
| Admin theme
|--------------------------------------------------------------------------
|
| WARNING: You MUST set this value!
|
| admin theme directory name
|
| 	Default: 'admin'
|
*/
$config['admin_theme'] = 'admin';

/*
|--------------------------------------------------------------------------
| Client theme
|--------------------------------------------------------------------------
|
| WARNING: You MUST set this value!
|
| client theme directory name
|
| 	Default: 'client'
|
*/
$config['client_theme'] = 'client';