<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_pegawai extends SP_Admin_Controller {

	public function index()
	{
		$this->load->view('manage_pegawai/manage_pegawai');
	}

}

/* End of file Manage_pegawai.php */
/* Location: ./application/modules/pegawai/controllers/Manage_pegawai.php */