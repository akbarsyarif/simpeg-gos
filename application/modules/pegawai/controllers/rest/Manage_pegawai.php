<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_pegawai extends SP_Rest_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_pegawai');
	}

	/**
	 * response for jeasyui datagrid
	 */
	public function datagrid_get()
	{
		$query = [];

		// search params
		if ( isset($_GET['jabatan']) && !empty($_GET['jabatan']) ) $query['where']['jabatan LIKE'] = '%' . $this->input->get('jabatan', TRUE) . '%';
		if ( isset($_GET['id_eselon']) && !empty($_GET['id_eselon']) ) $query['where']['id_eselon'] = $this->input->get('id_eselon', TRUE);
		if ( isset($_GET['id_skpd']) && !empty($_GET['id_skpd']) ) $query['where']['id_skpd'] = $this->input->get('id_skpd', TRUE);

		$count = $this->m_pegawai->count($query);

		// set limit and offset
		$offset = isset($_GET['page']) ? $this->input->get('page', TRUE) : 1;
		$limit  = isset($_GET['rows']) ? $this->input->get('rows', TRUE) : 10;
		$offset = ($offset - 1) * $limit;
		$query['limit_offset'] = [$limit, $offset];

		// sorting
		$sort = $this->input->get('sort', TRUE);
		$order = $this->input->get('order', TRUE);
		$split_sort = explode(',', $sort);
		$split_order = explode(',', $order);

		if ( isset($_GET['sort']) && isset($_GET['order']) ) {
			if ( is_array($split_sort) && count($split_sort) > 0 ) {
				foreach ($split_sort as $index => $field) {
					$orders[$field] = $split_order[$index];
				}
				$query['order_by'] = $orders;
			} else {
				$query['order_by'] = "{$sort} {$order}";
			}
		}

		$data = $this->m_pegawai->get($query);

		$this->response([
			'total' => $count,
			'rows' => $data['data']
		]);
	}

}

/* End of file Manage_pegawai.php */
/* Location: ./application/modules/pegawai/controllers/rest/Manage_pegawai.php */