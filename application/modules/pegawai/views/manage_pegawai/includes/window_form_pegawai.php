<div id="window-form-jabatan-fungsional-umum"
	style="display:none"
	data-options="width:500,inline:true">
	<form id="form-jabatan-fungsional-umum" class="form-horizontal" role="form" onsubmit="doSubmitFormJabatanFungsionalUmum(this); return false" onreset="doResetFormJabatanFungsionalUmum(this)">
		<input type="hidden" name="id" />
		<div class="panel-content">
			<div class="form-group">
				<label for="id_klp_1" class="col-xs-3 control-label">Kelompok 1</label>
				<div class="col-xs-9">
					<?= dinamyc_dropdown(array(
						'name' => 'id_klp_1',
						'table' => 'v_klp_jabatan_fungsional_umum_1',
						'key' => 'id',
						'label' => array('kode', 'kelompok'),
						'default' => '',
						'empty_first' => TRUE,
						'empty_first_label' => '- Pilih Kelompok Usulan -',
						'attr' => 'id="manage-jabatan-fungsional-umum-klp-1" class="form-control input-sm easyui-validatebox select2-single" style="width:100%" data-options="required:true"'
					)) ?>
				</div>
			</div>
			<div class="form-group">
				<label for="id_klp_2" class="col-xs-3 control-label">Kelompok 2</label>
				<div class="col-xs-9">
					<?= form_dropdown(array(
						'name' => 'id_klp_2',
						'options' => array(
							'' => '- Pilih Kelompok Usulan -'
						),
						'selected' => '',
						'class' => 'form-control input-sm easyui-validatebox select2-single',
						'id' => 'manage-jabatan-fungsional-umum-klp-2',
						'style' => 'width:100%',
						'data-options' => 'required:true'
					)) ?>
				</div>
			</div>
			<div class="form-group">
				<label for="kode" class="col-xs-3 control-label">Kode</label>
				<div class="col-xs-9">
					<input type="text" name="kode" class="form-control input-sm easyui-validatebox" data-options="required:true" />
				</div>
			</div>
			<div class="form-group">
				<label for="jabatan" class="col-xs-3 control-label">Jabatan</label>
				<div class="col-xs-9">
					<input type="text" name="jabatan" class="form-control input-sm easyui-validatebox" data-options="required:true" />
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-xs-9 col-xs-offset-3">
					<button type="submit" class="btn btn-sm btn-success btn-round" data-loading-text="<i class='fa fa-fw fa-circle-o-notch fa-spin'></i> loading..."><i class="fa fa-fw fa-check-circle"></i> Simpan</button> 
					<button type="reset" class="btn btn-sm btn-default btn-round"><i class="fa fa-fw fa-times-circle"></i> Batal</button> 
				</div>
			</div>
		</div>
	</form>
</div>