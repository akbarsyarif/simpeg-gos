<script type="text/javascript">
	function addJabatanFungsionalUmum(obj) {
		$('#window-form-pegawai').dialog({
			iconCls: "icon-add",
			title: "Tambah Jabatan",
			onBeforeOpen: function() {
				// clear form fields
				$('#form-pegawai').form('reset');
			}
		});
	}

	function editJabatanFungsionalUmum(obj) {
		var data = $('#table-pegawai').datagrid('getSelected');
		
		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin diedit', 'warning');
		} else {
			$('#window-form-pegawai').dialog({
				iconCls: "icon-edit",
				title: "Edit Jabatan",
				onBeforeOpen: function() {
					// set fields value
					$('#form-pegawai').find('[name="id"]').val(data.id);
					$('#form-pegawai').find('[name="id_klp_1"]').val(data.id_klp_1).trigger('change');
					$('#form-pegawai').find('[name="kode"]').val(data.kode);
					$('#form-pegawai').find('[name="jabatan"]').val(data.jabatan);
				}
			});
		}
	}

	function doSubmitFormJabatanFungsionalUmum(obj) {
		let data = $(obj).serialize();
		let id = $(obj).find('[name="id"]').val();
		if ( typeof id !== "undefined" && id !== "" ) {
			var method = "PUT";
		} else {
			var method = "POST";
		}

		$.ajax({
			url: '<?= site_url('api/pegawai/manage_pegawai') ?>',
			method: method,
			dataType: 'json',
			data: data,
			beforeSend: function(xhr) {
				isValid = $(obj).form('validate');
				if ( isValid ) {
					$(obj).find('[type="submit"]').button('loading');
				}
				return isValid;
			},
			success: function(xhr) {
				if ( typeof xhr === "object" ) {
					if ( xhr.metadata.code != 200 ) {
						$.messager.alert('Error', xhr.metadata.message, 'error');
					} else {
						if ( method === "PUT" ) {
							$('#window-form-pegawai').dialog('close');
						}
						$('#table-pegawai').datagrid('reload');
						$(obj).form('reset');
					}
				} else {
					$.messager.alert('Error', 'Server error', 'error');
					console.log(xhr);
				}
			},
			error: function(xhr) {
				let error = JSON.parse(xhr.responseText);
				if ( typeof error == "object" ) {
					var message = error.metadata.message;
				} else {
					var message = "Tidak dapat menghubungi server";
				}
				$.messager.alert('Error', message, 'error');
				console.log(error);
			},
			complete: function() {
				$(obj).find('[type="submit"]').button('reset');
			}
		});
	}

	function doResetFormJabatanFungsionalUmum(obj) {
		$('#window-form-pegawai').dialog('close');
		$(obj).form('clear');
	}

	function removeJabatanFungsionalUmum(obj) {
		var data = $('#table-pegawai').datagrid('getSelected');

		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin dihapus', 'warning');
		} else {
			$.messager.confirm('Hapus Data', 'Anda yakin ingin menghapus data jabatan <b>'+ data.kode + ' - ' + data.jabatan +'</b>?', function(r) {
				if ( r ) {
					$('#table-pegawai').datagrid('loading');

					$.ajax({
						url: '<?= site_url('api/pegawai/manage_pegawai') ?>',
						method: 'delete',
						data: {
							id: data.id
						},
						dataType: 'json',
						success: function(xhr) {
							if ( typeof xhr === "object" ) {
								if ( xhr.metadata.code != 200 ) {
									$.messager.alert('Error', xhr.metadata.message, 'error');
								} else {
									$('#table-pegawai').datagrid('reload');
								}
							} else {
								$.messager.alert('Error', 'Server error', 'error');
								console.log(xhr);
							}
						},
						error: function(xhr) {
							let error = JSON.parse(xhr.responseText);
							if ( typeof error == "object" ) {
								var message = error.metadata.message;
							} else {
								var message = "Tidak dapat menghubungi server";
							}
							$.messager.alert('Error', message, 'error');
							console.log(error);
						},
						complete: function() {
							$('#table-pegawai').datagrid('loaded');
						}
					})
				}
			});
		}
	}

	function searchJabatanFungsionalUmum(obj) {
		$('#window-search-pegawai').dialog();
	}

	function doSearchJabatanFungsionalUmum(obj) {
		var jabatan = $(obj).find('[name="jabatan"]').val();
		var id_eselon = $(obj).find('[name="id_eselon"]').val();
		var id_skpd = $(obj).find('[name="id_skpd"]').val();

		$('#table-pegawai').datagrid('load', {
			jabatan: jabatan,
			id_eselon: id_eselon,
			id_skpd: id_skpd,
		});

		$('#window-search-pegawai').dialog('close');
	}

	function doRefreshJabatanFungsionalUmum(obj) {
		$('#table-pegawai').datagrid('load', {});
		$('#window-search-pegawai').dialog('close');
		$(obj).form('clear');
	}

	function loadKlpJabatanFungsionalUmum2() {
		let id_klp_1 = $('#manage-pegawai-klp-1').combobox('getValue');
		if ( typeof id_klp_1 == "undefined" || id_klp_1 == "" ) {
			$('#manage-pegawai-klp-2').combobox('loadData', []);
			$('#manage-pegawai-klp-2').combobox('setValue', '');
		} else {
			$('#manage-pegawai-klp-2').combobox('reload', {
				id_klp_1: id_klp_1
			});
		}
	}

	$(document).ready(function() {
		$('#table-pegawai').datagrid({
			title: '<i class="fa fa-fw fa-user"></i> Pegawai',
			url: '<?= site_url('api/pegawai/manage_pegawai/datagrid') ?>',
			method: 'get',
			pagination: true,
			rownumbers: true,
			singleSelect: true,
			toolbar: '#toolbar-table-pegawai',
			pageSize: 20,
			emptyMsg: 'Tidak ada data ditemukan',
			multiSort: true,
			columns: [[
				{
					title: 'Nip',
					field: 'nip_baru',
					width: 200,
					align: 'center'
				},
				{
					title: 'Nama',
					field: 'nama',
					width: 200,
				}
			]]
		});
	});
</script>