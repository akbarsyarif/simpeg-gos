<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Pegawai extends MY_Model {

	public $table = 'pegawai';
	public $view = 'v_pegawai';
	public $pk = 'id';

	public function __construct()
	{
		parent::__construct();
	}

}

/* End of file M_Pegawai.php */
/* Location: ./application/models/M_Pegawai.php */