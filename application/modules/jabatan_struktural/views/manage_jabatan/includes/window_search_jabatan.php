<div id="window-search-jabatan-struktural"
	title="Pencarian Jabatan",
	data-options="width:500,iconCls:'icon-search',inline:true"
	style="display:none">
	<form id="form-search-jabatan-struktural" method="post" class="form-horizontal" role="form" onsubmit="doSearchJabatanStruktural(this); return false" onreset="doRefreshJabatanStruktural(this)">
		<div class="panel-content">
			<div class="form-group">
				<label class="control-label col-xs-3">Jabatan</label>
				<div class="col-xs-9">
					<input name="jabatan" type="text" class="form-control input-sm" />
				</div>
			</div>
			<div class="form-group">
				<label for="id_eselon" class="col-xs-3 control-label">Eselon</label>
				<div class="col-xs-9">
					<?= dinamyc_dropdown(array(
						'name' => 'id_eselon',
						'table' => 'v_eselon',
						'order_by' => 'eselon ASC',
						'key' => 'id',
						'label' => 'eselon',
						'default' => '',
						'empty_first' => TRUE,
						'empty_first_label' => '- Pilih Eselon -',
						'attr' => 'class="form-control input-sm easyui-validatebox easyui-combobox" style="width:100%" data-options="required:true"'
					)) ?>
				</div>
			</div>
			<div class="form-group">
				<label for="id_skpd" class="col-xs-3 control-label">SKPD</label>
				<div class="col-xs-9">
					<?= dinamyc_dropdown(array(
						'name' => 'id_skpd',
						'table' => 'v_skpd',
						'order_by' => 'skpd ASC',
						'key' => 'id',
						'label' => 'skpd',
						'default' => '',
						'empty_first' => TRUE,
						'empty_first_label' => '- Pilih SKPD -',
						'attr' => 'class="form-control input-sm easyui-validatebox easyui-combobox" style="width:100%" data-options="required:true"'
					)) ?>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-xs-9 col-xs-offset-3">
					<button type="submit" class="btn btn-sm btn-default btn-round"><i class="fa fa-fw fa-search"></i> Cari</button>	
					<button type="reset" class="btn btn-sm btn-default btn-round"><i class="fa fa-fw fa-refresh"></i> Segarkan</button>	
				</div>
			</div>
		</div>
	</form>
</div>