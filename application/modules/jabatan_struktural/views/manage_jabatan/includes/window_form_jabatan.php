<div id="window-form-jabatan-struktural"
	style="display:none"
	data-options="width:500,inline:true">
	<form id="form-jabatan-struktural" class="form-horizontal" role="form" onsubmit="doSubmitFormJabatanStruktural(this); return false" onreset="doResetFormJabatanStruktural(this)">
		<input type="hidden" name="id" />
		<div class="panel-content">
			<div class="form-group">
				<label for="jabatan" class="col-xs-3 control-label">Jabatan</label>
				<div class="col-xs-9">
					<input type="text" name="jabatan" class="form-control input-sm easyui-validatebox" data-options="required:true" />
				</div>
			</div>
			<div class="form-group">
				<label for="id_eselon" class="col-xs-3 control-label">Eselon</label>
				<div class="col-xs-9">
					<?= dinamyc_dropdown(array(
						'name' => 'id_eselon',
						'table' => 'v_eselon',
						'order_by' => 'eselon ASC',
						'key' => 'id',
						'label' => 'eselon',
						'default' => '',
						'empty_first' => TRUE,
						'empty_first_label' => '- Pilih Eselon -',
						'attr' => 'id="id_eselon" class="form-control input-sm easyui-validatebox easyui-combobox" style="width:100%" data-options="required:true"'
					)) ?>
				</div>
			</div>
			<div class="form-group">
				<label for="id_skpd" class="col-xs-3 control-label">SKPD</label>
				<div class="col-xs-9">
					<?= dinamyc_dropdown(array(
						'name' => 'id_skpd',
						'table' => 'v_skpd',
						'order_by' => 'skpd ASC',
						'key' => 'id',
						'label' => 'skpd',
						'default' => '',
						'empty_first' => TRUE,
						'empty_first_label' => '- Pilih SKPD -',
						'attr' => 'id="id_skpd" class="form-control input-sm easyui-validatebox easyui-combobox" style="width:100%" data-options="required:true"'
					)) ?>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-xs-9 col-xs-offset-3">
					<button type="submit" class="btn btn-sm btn-success btn-round" data-loading-text="<i class='fa fa-fw fa-circle-o-notch fa-spin'></i> loading..."><i class="fa fa-fw fa-check-circle"></i> Simpan</button> 
					<button type="reset" class="btn btn-sm btn-default btn-round"><i class="fa fa-fw fa-times-circle"></i> Batal</button> 
				</div>
			</div>
		</div>
	</form>
</div>