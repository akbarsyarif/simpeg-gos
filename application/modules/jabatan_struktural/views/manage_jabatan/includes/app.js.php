<script type="text/javascript">
	function addJabatanStruktural(obj) {
		$('#window-form-jabatan-struktural').dialog({
			iconCls: "icon-add",
			title: "Tambah Jabatan",
			onBeforeOpen: function() {
				// clear form fields
				$('#form-jabatan-struktural').form('reset');
			}
		});
	}

	function editJabatanStruktural(obj) {
		var data = $('#table-jabatan-struktural').datagrid('getSelected');
		
		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin diedit', 'warning');
		} else {
			$('#window-form-jabatan-struktural').dialog({
				iconCls: "icon-edit",
				title: "Edit Jabatan",
				onBeforeOpen: function() {
					// set fields value
					$('#form-jabatan-struktural').find('[name="id"]').val(data.id);
					$('#form-jabatan-struktural').find('[name="jabatan"]').val(data.jabatan);
					$('#form-jabatan-struktural').find('#id_eselon').combobox('select', data.id_eselon);
					$('#form-jabatan-struktural').find('#id_skpd').combobox('select', data.id_skpd);
				}
			});
		}
	}

	function doSubmitFormJabatanStruktural(obj) {
		let data = $(obj).serialize();
		let id = $(obj).find('[name="id"]').val();
		if ( typeof id !== "undefined" && id !== "" ) {
			var method = "PUT";
		} else {
			var method = "POST";
		}

		$.ajax({
			url: '<?= site_url('api/jabatan_struktural/manage_jabatan') ?>',
			method: method,
			dataType: 'json',
			data: data,
			beforeSend: function(xhr) {
				isValid = $(obj).form('validate');
				if ( isValid ) {
					$(obj).find('[type="submit"]').button('loading');
				}
				return isValid;
			},
			success: function(xhr) {
				if ( typeof xhr === "object" ) {
					if ( xhr.metadata.code != 200 ) {
						$.messager.alert('Error', xhr.metadata.message, 'error');
					} else {
						if ( method === "PUT" ) {
							$('#window-form-jabatan-struktural').dialog('close');
						}
						$('#table-jabatan-struktural').datagrid('reload');
						$(obj).form('reset');
					}
				} else {
					$.messager.alert('Error', 'Server error', 'error');
					console.log(xhr);
				}
			},
			error: function(xhr) {
				let error = JSON.parse(xhr.responseText);
				if ( typeof error == "object" ) {
					var message = error.metadata.message;
				} else {
					var message = "Tidak dapat menghubungi server";
				}
				$.messager.alert('Error', message, 'error');
				console.log(error);
			},
			complete: function() {
				$(obj).find('[type="submit"]').button('reset');
			}
		});
	}

	function doResetFormJabatanStruktural(obj) {
		$('#window-form-jabatan-struktural').dialog('close');
		$(obj).form('clear');
	}

	function removeJabatanStruktural(obj) {
		var data = $('#table-jabatan-struktural').datagrid('getSelected');

		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin dihapus', 'warning');
		} else {
			$.messager.confirm('Hapus Data', 'Anda yakin ingin menghapus data kelompok jabatan <b>'+ data.kelompok +'</b>?', function(r) {
				if ( r ) {
					$('#table-jabatan-struktural').datagrid('loading');

					$.ajax({
						url: '<?= site_url('api/jabatan_struktural/manage_jabatan') ?>',
						method: 'delete',
						data: {
							id: data.id
						},
						dataType: 'json',
						success: function(xhr) {
							if ( typeof xhr === "object" ) {
								if ( xhr.metadata.code != 200 ) {
									$.messager.alert('Error', xhr.metadata.message, 'error');
								} else {
									$('#table-jabatan-struktural').datagrid('reload');
								}
							} else {
								$.messager.alert('Error', 'Server error', 'error');
								console.log(xhr);
							}
						},
						error: function(xhr) {
							let error = JSON.parse(xhr.responseText);
							if ( typeof error == "object" ) {
								var message = error.metadata.message;
							} else {
								var message = "Tidak dapat menghubungi server";
							}
							$.messager.alert('Error', message, 'error');
							console.log(error);
						},
						complete: function() {
							$('#table-jabatan-struktural').datagrid('loaded');
						}
					})
				}
			});
		}
	}

	function searchJabatanStruktural(obj) {
		$('#window-search-jabatan-struktural').dialog();
	}

	function doSearchJabatanStruktural(obj) {
		var jabatan = $(obj).find('[name="jabatan"]').val();
		var id_eselon = $(obj).find('[name="id_eselon"]').val();
		var id_skpd = $(obj).find('[name="id_skpd"]').val();

		$('#table-jabatan-struktural').datagrid('load', {
			jabatan: jabatan,
			id_eselon: id_eselon,
			id_skpd: id_skpd,
		});

		$('#window-search-jabatan-struktural').dialog('close');
	}

	function doRefreshJabatanStruktural(obj) {
		$('#table-jabatan-struktural').datagrid('load', {});
		$('#window-search-jabatan-struktural').dialog('close');
		$(obj).form('clear');
	}

	$(document).ready(function() {
		$('#table-jabatan-struktural').datagrid({
			title: '<i class="fa fa-fw fa-list"></i> Jabatan',
			url: '<?= site_url('api/jabatan_struktural/manage_jabatan/datagrid') ?>',
			method: 'get',
			pagination: true,
			fit: true,
			rownumbers: true,
			fitColumns: true,
			singleSelect: true,
			toolbar: '#toolbar-table-jabatan-struktural',
			pageSize: 20,
			striped: true,
			emptyMsg: 'Tidak ada data ditemukan',
			multiSort: true,
			columns: [[
				{
					title: 'Jabatan',
					field: 'jabatan',
					width: 130,
					sortable: true
				},
				{
					title: 'Eselon',
					field: 'eselon',
					width: 30,
					align: 'center',
					sortable: true
				},
				{
					title: 'SKPD',
					field: 'skpd',
					width: 130,
					sortable: true
				}
			]]
		});
	});
</script>