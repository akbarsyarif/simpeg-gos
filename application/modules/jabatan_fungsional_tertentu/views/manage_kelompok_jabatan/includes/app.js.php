<script type="text/javascript">
	function addKelompokJabatanFungsionalTertentu(obj) {
		$('#window-form-kelompok-jabatan-fungsional-tertentu').dialog({
			iconCls: "icon-add",
			title: "Tambah Kelompok Jabatan",
			onBeforeOpen: function() {
				// clear form fields
				$('#form-kelompok-jabatan-fungsional-tertentu').form('reset');
			}
		});
	}

	function editKelompokJabatanFungsionalTertentu(obj) {
		var data = $('#table-kelompok-jabatan-fungsional-tertentu').datagrid('getSelected');
		
		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin diedit', 'warning');
		} else {
			$('#window-form-kelompok-jabatan-fungsional-tertentu').dialog({
				iconCls: "icon-edit",
				title: "Edit Kelompok Jabatan",
				onBeforeOpen: function() {
					// set fields value
					$('#form-kelompok-jabatan-fungsional-tertentu').find('[name="id"]').val(data.id);
					$('#form-kelompok-jabatan-fungsional-tertentu').find('[name="kelompok"]').val(data.kelompok);
				}
			});
		}
	}

	function doSubmitFormKelompokJabatanFungsionalTertentu(obj) {
		let data = $(obj).serialize();
		let id = $(obj).find('[name="id"]').val();
		if ( typeof id !== "undefined" && id !== "" ) {
			var method = "PUT";
		} else {
			var method = "POST";
		}

		$.ajax({
			url: '<?= site_url('api/jabatan_fungsional_tertentu/manage_kelompok_jabatan') ?>',
			method: method,
			dataType: 'json',
			data: data,
			beforeSend: function(xhr) {
				isValid = $(obj).form('validate');
				if ( isValid ) {
					$(obj).find('[type="submit"]').button('loading');
				}
				return isValid;
			},
			success: function(xhr) {
				if ( typeof xhr === "object" ) {
					if ( xhr.metadata.code != 200 ) {
						$.messager.alert('Error', xhr.metadata.message, 'error');
					} else {
						if ( method === "PUT" ) {
							$('#window-form-kelompok-jabatan-fungsional-tertentu').dialog('close');
						}
						$('#table-kelompok-jabatan-fungsional-tertentu').datagrid('reload');
						$(obj).form('reset');
					}
				} else {
					$.messager.alert('Error', 'Server error', 'error');
					console.log(xhr);
				}
			},
			error: function(xhr) {
				let error = JSON.parse(xhr.responseText);
				if ( typeof error == "object" ) {
					var message = error.metadata.message;
				} else {
					var message = "Tidak dapat menghubungi server";
				}
				$.messager.alert('Error', message, 'error');
				console.log(error);
			},
			complete: function() {
				$(obj).find('[type="submit"]').button('reset');
			}
		});
	}

	function doResetFormKelompokJabatanFungsionalTertentu(obj) {
		$('#window-form-kelompok-jabatan-fungsional-tertentu').dialog('close');
		$('#form-kelompok-jabatan-fungsional-tertentu').form('clear');
	}

	function removeKelompokJabatanFungsionalTertentu(obj) {
		var data = $('#table-kelompok-jabatan-fungsional-tertentu').datagrid('getSelected');

		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin dihapus', 'warning');
		} else {
			$.messager.confirm('Hapus Data', 'Anda yakin ingin menghapus data kelompok jabatan <b>'+ data.kelompok +'</b>?', function(r) {
				if ( r ) {
					$('#table-kelompok-jabatan-fungsional-tertentu').datagrid('loading');

					$.ajax({
						url: '<?= site_url('api/jabatan_fungsional_tertentu/manage_kelompok_jabatan') ?>',
						method: 'delete',
						data: {
							id: data.id
						},
						dataType: 'json',
						success: function(xhr) {
							if ( typeof xhr === "object" ) {
								if ( xhr.metadata.code != 200 ) {
									$.messager.alert('Error', xhr.metadata.message, 'error');
								} else {
									$('#table-kelompok-jabatan-fungsional-tertentu').datagrid('reload');
								}
							} else {
								$.messager.alert('Error', 'Server error', 'error');
								console.log(xhr);
							}
						},
						error: function(xhr) {
							let error = JSON.parse(xhr.responseText);
							if ( typeof error == "object" ) {
								var message = error.metadata.message;
							} else {
								var message = "Tidak dapat menghubungi server";
							}
							$.messager.alert('Error', message, 'error');
							console.log(error);
						},
						complete: function() {
							$('#table-kelompok-jabatan-fungsional-tertentu').datagrid('loaded');
						}
					})
				}
			});
		}
	}

	function searchKelompokJabatanFungsionalTertentu(obj) {
		$('#window-search-kelompok-jabatan-fungsional-tertentu').dialog();
	}

	function doSearchKelompokJabatanFungsionalTertentu(obj) {
		var kelompok = $(obj).find('[name="kelompok"]').val();

		$('#table-kelompok-jabatan-fungsional-tertentu').datagrid('load', {
			kelompok: kelompok
		});

		$('#window-search-kelompok-jabatan-fungsional-tertentu').dialog('close');
	}

	function doRefreshKelompokJabatanFungsionalTertentu(obj) {
		$('#table-kelompok-jabatan-fungsional-tertentu').datagrid('load', {});
		$('#window-search-kelompok-jabatan-fungsional-tertentu').dialog('close');
		$('#form-search-eselon').form('clear');
	}

	$(document).ready(function() {
		$('#table-kelompok-jabatan-fungsional-tertentu').datagrid({
			title: '<i class="fa fa-fw fa-list"></i> Kelompok Jabatan',
			url: '<?= site_url('api/jabatan_fungsional_tertentu/manage_kelompok_jabatan/datagrid') ?>',
			method: 'get',
			pagination: true,
			fit: true,
			rownumbers: true,
			fitColumns: true,
			singleSelect: true,
			toolbar: '#toolbar-table-kelompok-jabatan-fungsional-tertentu',
			pageSize: 20,
			striped: true,
			emptyMsg: 'Tidak ada data ditemukan',
			multiSort: true,
			columns: [[
				{
					title: 'Kelompok',
					field: 'kelompok',
					width: 130,
					sortable: true
				}
			]]
		});
	});
</script>