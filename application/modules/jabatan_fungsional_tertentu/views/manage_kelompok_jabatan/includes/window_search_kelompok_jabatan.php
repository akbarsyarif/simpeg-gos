<div id="window-search-kelompok-jabatan-fungsional-tertentu"
	title="Pencarian Kelompok Jabatan",
	data-options="width:500,iconCls:'icon-search',inline:true"
	style="display:none">
	<form id="form-search-kelompok-jabatan-fungsional-tertentu" method="post" class="form-horizontal" role="form" onsubmit="doSearchKelompokJabatanFungsionalTertentu(this); return false" onreset="doRefreshKelompokJabatanFungsionalTertentu(this)">
		<div class="panel-content">
			<div class="form-group">
				<label class="control-label col-xs-4">Kelompok Jabatan</label>
				<div class="col-xs-8">
					<input name="kelompok" type="text" class="form-control input-sm" />
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-xs-8 col-xs-offset-4">
					<button type="submit" class="btn btn-sm btn-default btn-round"><i class="fa fa-fw fa-search"></i> Cari</button>	
					<button type="reset" class="btn btn-sm btn-default btn-round"><i class="fa fa-fw fa-refresh"></i> Segarkan</button>	
				</div>
			</div>
		</div>
	</form>
</div>