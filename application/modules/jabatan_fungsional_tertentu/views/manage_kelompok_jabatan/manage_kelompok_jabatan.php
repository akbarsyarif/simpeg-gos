<div id="toolbar-table-kelompok-jabatan-fungsional-tertentu">
	<table cellpadding="0" cellspacing="0" class="tb-toolbar">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td><a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="addKelompokJabatanFungsionalTertentu(this)"><i class="fa fa-fw fa-plus"></i> Tambah</a></td>
						<td><a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="editKelompokJabatanFungsionalTertentu(this)"><i class="fa fa-fw fa-edit"></i> Edit</a></td>
						<td><a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="removeKelompokJabatanFungsionalTertentu(this)"><i class="fa fa-fw fa-trash"></i> Hapus</a></td>
					</tr>
				</table>
			</td>
			<td align="right">
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td><a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="searchKelompokJabatanFungsionalTertentu(this)"><i class="fa fa-fw fa-search"></i> Pencarian</a></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
<table id="table-kelompok-jabatan-fungsional-tertentu"></table>

<?php require 'includes/window_search_kelompok_jabatan.php' ?>
<?php require 'includes/window_form_kelompok_jabatan.php' ?>

<!-- include javascript -->
<?php require 'includes/app.js.php' ?>
