<div id="window-search-jabatan-fungsional-tertentu"
	title="Pencarian Jabatan",
	data-options="width:500,iconCls:'icon-search',inline:true"
	style="display:none">
	<form id="form-search-jabatan-fungsional-tertentu" method="post" class="form-horizontal" role="form" onsubmit="doSearchJabatanFungsionalTertentu(this); return false" onreset="doRefreshJabatanFungsionalTertentu(this)">
		<div class="panel-content">
			<div class="form-group">
				<label for="id_klp" class="col-xs-4 control-label">Kelompok Jabatan</label>
				<div class="col-xs-8">
					<?= dinamyc_dropdown(array(
						'name' => 'id_klp',
						'table' => 'klp_jabatan_fungsional_tertentu',
						'order_by' => 'kelompok ASC',
						'key' => 'id',
						'label' => 'kelompok',
						'default' => '',
						'empty_first' => TRUE,
						'empty_first_label' => '- Semua -',
						'attr' => 'class="form-control input-sm easyui-combobox" style="width:100%"'
					)) ?>
				</div>
			</div>
			<div class="form-group">
				<label for="tignkat" class="col-xs-4 control-label">Tingkat</label>
				<div class="col-xs-8">
					<?= form_dropdown(array(
						'name' => 'tingkat',
						'options' => array(
							'' => '- Semua -',
							'Terampil' => 'Terampil',
							'Ahli' => 'Ahli'
						),
						'selected' => '',
						'class' => 'form-control input-sm easyui-combobox',
						'style' => 'width:100%'
					)) ?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-4">Jabatan</label>
				<div class="col-xs-8">
					<input name="jabatan" type="text" class="form-control input-sm" />
				</div>
			</div>
			<div class="form-group">
				<label for="id_golongan" class="col-xs-4 control-label">Golongan</label>
				<div class="col-xs-8">
					<?= dinamyc_dropdown(array(
						'name' => 'id_golongan',
						'table' => 'v_golongan',
						'order_by' => 'golongan ASC',
						'key' => 'id',
						'label' => array('golongan', 'pangkat'),
						'default' => '',
						'empty_first' => TRUE,
						'empty_first_label' => '- Semua -',
						'attr' => 'class="form-control input-sm easyui-combobox" style="width:100%"'
					)) ?>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-xs-8 col-xs-offset-4">
					<button type="submit" class="btn btn-sm btn-default btn-round"><i class="fa fa-fw fa-search"></i> Cari</button>	
					<button type="reset" class="btn btn-sm btn-default btn-round"><i class="fa fa-fw fa-refresh"></i> Segarkan</button>	
				</div>
			</div>
		</div>
	</form>
</div>