<div id="window-form-jabatan-fungsional-tertentu"
	style="display:none"
	data-options="width:500,inline:true">
	<form id="form-jabatan-fungsional-tertentu" class="form-horizontal" role="form" onsubmit="doSubmitFormJabatanFungsionalTertentu(this); return false" onreset="doResetFormJabatanFungsionalTertentu(this)">
		<input type="hidden" name="id" />
		<div class="panel-content">
			<div class="form-group">
				<label for="id_klp" class="col-xs-4 control-label">Kelompok Jabatan</label>
				<div class="col-xs-8">
					<?= dinamyc_dropdown(array(
						'name' => 'id_klp',
						'table' => 'klp_jabatan_fungsional_tertentu',
						'order_by' => 'kelompok ASC',
						'key' => 'id',
						'label' => 'kelompok',
						'default' => '',
						'empty_first' => TRUE,
						'empty_first_label' => '- Pilih Kelompok -',
						'attr' => 'id="id_klp" class="form-control input-sm easyui-validatebox easyui-combobox" style="width:100%" data-options="required:true"'
					)) ?>
				</div>
			</div>
			<div class="form-group">
				<label for="tingkat" class="col-xs-4 control-label">Tingkat</label>
				<div class="col-xs-8">
					<?= form_dropdown(array(
						'name' => 'tingkat',
						'options' => array(
							'' => '- Pilih Tingkat -',
							'Terampil' => 'Terampil',
							'Ahli' => 'Ahli'
						),
						'selected' => '',
						'id' => 'tingkat',
						'class' => 'form-control input-sm easyui-validatebox easyui-combobox',
						'style' => 'width:100%',
						'data-options' => 'required:true'
					)) ?>
				</div>
			</div>
			<div class="form-group">
				<label for="jabatan" class="col-xs-4 control-label">Jabatan</label>
				<div class="col-xs-8">
					<input type="text" name="jabatan" class="form-control input-sm easyui-validatebox" data-options="required:true" />
				</div>
			</div>
			<div class="form-group">
				<label for="id_golongan" class="col-xs-4 control-label">Golongan</label>
				<div class="col-xs-8">
					<?= dinamyc_dropdown(array(
						'name' => 'id_golongan',
						'table' => 'v_golongan',
						'order_by' => 'golongan ASC',
						'key' => 'id',
						'label' => array('golongan', 'pangkat'),
						'default' => '',
						'empty_first' => TRUE,
						'empty_first_label' => '- Pilih Golongan -',
						'attr' => 'id="id_golongan" class="form-control input-sm easyui-validatebox easyui-combobox" style="width:100%" data-options="required:true"'
					)) ?>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-xs-8 col-xs-offset-4">
					<button type="submit" class="btn btn-sm btn-success btn-round" data-loading-text="<i class='fa fa-fw fa-circle-o-notch fa-spin'></i> loading..."><i class="fa fa-fw fa-check-circle"></i> Simpan</button> 
					<button type="reset" class="btn btn-sm btn-default btn-round"><i class="fa fa-fw fa-times-circle"></i> Batal</button> 
				</div>
			</div>
		</div>
	</form>
</div>