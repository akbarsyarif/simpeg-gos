<script type="text/javascript">
	function addJabatanFungsionalTertentu(obj) {
		$('#window-form-jabatan-fungsional-tertentu').dialog({
			iconCls: "icon-add",
			title: "Tambah Jabatan",
			onBeforeOpen: function() {
				// clear form fields
				$('#form-jabatan-fungsional-tertentu').form('reset');
			}
		});
	}

	function editJabatanFungsionalTertentu(obj) {
		var data = $('#table-jabatan-fungsional-tertentu').datagrid('getSelected');
		
		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin diedit', 'warning');
		} else {
			$('#window-form-jabatan-fungsional-tertentu').dialog({
				iconCls: "icon-edit",
				title: "Edit Jabatan",
				onBeforeOpen: function() {
					// set fields value
					$('#form-jabatan-fungsional-tertentu').find('[name="id"]').val(data.id);
					$('#form-jabatan-fungsional-tertentu').find('#id_klp').combobox('select', data.id_klp);
					$('#form-jabatan-fungsional-tertentu').find('#tingkat').combobox('select', data.tingkat);
					$('#form-jabatan-fungsional-tertentu').find('[name="jabatan"]').val(data.jabatan);
					$('#form-jabatan-fungsional-tertentu').find('#id_golongan').combobox('select', data.id_golongan);
				}
			});
		}
	}

	function doSubmitFormJabatanFungsionalTertentu(obj) {
		let data = $(obj).serialize();
		let id = $(obj).find('[name="id"]').val();
		if ( typeof id !== "undefined" && id !== "" ) {
			var method = "PUT";
		} else {
			var method = "POST";
		}

		$.ajax({
			url: '<?= site_url('api/jabatan_fungsional_tertentu/manage_jabatan') ?>',
			method: method,
			dataType: 'json',
			data: data,
			beforeSend: function(xhr) {
				isValid = $(obj).form('validate');
				if ( isValid ) {
					$(obj).find('[type="submit"]').button('loading');
				}
				return isValid;
			},
			success: function(xhr) {
				if ( typeof xhr === "object" ) {
					if ( xhr.metadata.code != 200 ) {
						$.messager.alert('Error', xhr.metadata.message, 'error');
					} else {
						if ( method === "PUT" ) {
							$('#window-form-jabatan-fungsional-tertentu').dialog('close');
						}
						$('#table-jabatan-fungsional-tertentu').datagrid('reload');
						$(obj).form('reset');
					}
				} else {
					$.messager.alert('Error', 'Server error', 'error');
					console.log(xhr);
				}
			},
			error: function(xhr) {
				let error = JSON.parse(xhr.responseText);
				if ( typeof error == "object" ) {
					var message = error.metadata.message;
				} else {
					var message = "Tidak dapat menghubungi server";
				}
				$.messager.alert('Error', message, 'error');
				console.log(error);
			},
			complete: function() {
				$(obj).find('[type="submit"]').button('reset');
			}
		});
	}

	function doResetFormJabatanFungsionalTertentu(obj) {
		$('#window-form-jabatan-fungsional-tertentu').dialog('close');
		$(obj).form('clear');
	}

	function removeJabatanFungsionalTertentu(obj) {
		var data = $('#table-jabatan-fungsional-tertentu').datagrid('getSelected');

		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin dihapus', 'warning');
		} else {
			$.messager.confirm('Hapus Data', 'Anda yakin ingin menghapus data jabatan <b>'+ data.kelompok + ' - ' + data.tingkat + ' - ' + data.jabatan +'</b>?', function(r) {
				if ( r ) {
					$('#table-jabatan-fungsional-tertentu').datagrid('loading');

					$.ajax({
						url: '<?= site_url('api/jabatan_fungsional_tertentu/manage_jabatan') ?>',
						method: 'delete',
						data: {
							id: data.id
						},
						dataType: 'json',
						success: function(xhr) {
							if ( typeof xhr === "object" ) {
								if ( xhr.metadata.code != 200 ) {
									$.messager.alert('Error', xhr.metadata.message, 'error');
								} else {
									$('#table-jabatan-fungsional-tertentu').datagrid('reload');
								}
							} else {
								$.messager.alert('Error', 'Server error', 'error');
								console.log(xhr);
							}
						},
						error: function(xhr) {
							let error = JSON.parse(xhr.responseText);
							if ( typeof error == "object" ) {
								var message = error.metadata.message;
							} else {
								var message = "Tidak dapat menghubungi server";
							}
							$.messager.alert('Error', message, 'error');
							console.log(error);
						},
						complete: function() {
							$('#table-jabatan-fungsional-tertentu').datagrid('loaded');
						}
					})
				}
			});
		}
	}

	function searchJabatanFungsionalTertentu(obj) {
		$('#window-search-jabatan-fungsional-tertentu').dialog();
	}

	function doSearchJabatanFungsionalTertentu(obj) {
		var id_klp = $(obj).find('[name="id_klp"]').val();
		var tingkat = $(obj).find('[name="tingkat"]').val();
		var jabatan = $(obj).find('[name="jabatan"]').val();
		var id_golongan = $(obj).find('[name="id_golongan"]').val();

		$('#table-jabatan-fungsional-tertentu').datagrid('load', {
			id_klp: id_klp,
			tingkat: tingkat,
			jabatan: jabatan,
			id_golongan: id_golongan,
		});

		$('#window-search-jabatan-fungsional-tertentu').dialog('close');
	}

	function doRefreshJabatanFungsionalTertentu(obj) {
		$('#table-jabatan-fungsional-tertentu').datagrid('load', {});
		$('#window-search-jabatan-fungsional-tertentu').dialog('close');
		$(obj).form('clear');
	}

	$(document).ready(function() {
		$('#table-jabatan-fungsional-tertentu').datagrid({
			title: '<i class="fa fa-fw fa-list"></i> Jabatan',
			url: '<?= site_url('api/jabatan_fungsional_tertentu/manage_jabatan/datagrid') ?>',
			method: 'get',
			pagination: true,
			fit: true,
			rownumbers: true,
			fitColumns: true,
			singleSelect: true,
			toolbar: '#toolbar-table-jabatan-fungsional-tertentu',
			pageSize: 20,
			striped: true,
			emptyMsg: 'Tidak ada data ditemukan',
			multiSort: true,
			columns: [[
				{
					title: 'Kelompok',
					field: 'kelompok',
					width: 200,
					sortable: true
				},
				{
					title: 'Tingkat',
					field: 'tingkat',
					width: 50,
					align: 'center',
					sortable: true
				},
				{
					title: 'Jabatan',
					field: 'jabatan',
					width: 100,
					sortable: true
				},
				{
					title: 'Golongan',
					field: 'golongan',
					width: 50,
					sortable: true
				},
				{
					title: 'Pangkat',
					field: 'pangkat',
					width: 130,
					sortable: true
				},
			]],
			onLoadSuccess: function(data) {
				$(this).datagrid("mergeEqualCells", ["kelompok", "tingkat"]);
			}
		});
	});
</script>