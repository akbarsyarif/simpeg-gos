<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Kelompok_jabatan extends MY_Model {

	public $table = 'klp_jabatan_fungsional_tertentu';
	public $view  = 'v_klp_jabatan_fungsional_tertentu';
	public $pk    = 'id';

	public function __construct()
	{
		parent::__construct();
	}

}

/* End of file M_Kelompok_jabatan.php */
/* Location: ./application/models/M_Kelompok_jabatan.php */