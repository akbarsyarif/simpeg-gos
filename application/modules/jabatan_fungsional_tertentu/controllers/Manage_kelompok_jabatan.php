<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_kelompok_jabatan extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('manage_kelompok_jabatan/manage_kelompok_jabatan', $this->data);
	}

}

/* End of file Manage_kelompok_jabatan.php */
/* Location: ./application/controllers/Manage_kelompok_jabatan.php */