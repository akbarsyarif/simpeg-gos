<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_jabatan extends SP_Rest_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_jabatan');
	}

	/**
	 * response for jeasyui datagrid
	 */
	public function datagrid_get()
	{
		$query = [];

		// search params
		if ( isset($_GET['id_klp']) && !empty($_GET['id_klp']) ) $query['where']['id_klp'] = $this->input->get('id_klp', TRUE);
		if ( isset($_GET['tingkat']) && !empty($_GET['tingkat']) ) $query['where']['tingkat'] = $this->input->get('tingkat', TRUE);
		if ( isset($_GET['jabatan']) && !empty($_GET['jabatan']) ) $query['where']['jabatan LIKE'] = '%' . $this->input->get('jabatan', TRUE) . '%';
		if ( isset($_GET['id_golongan']) && !empty($_GET['id_golongan']) ) $query['where']['id_golongan'] = $this->input->get('id_golongan', TRUE);

		$count = $this->m_jabatan->count($query);

		// set limit and offset
		$offset = isset($_GET['page']) ? $this->input->get('page', TRUE) : 1;
		$limit  = isset($_GET['rows']) ? $this->input->get('rows', TRUE) : 10;
		$offset = ($offset - 1) * $limit;
		$query['limit_offset'] = [$limit, $offset];

		// sorting
		$sort = $this->input->get('sort', TRUE);
		$order = $this->input->get('order', TRUE);
		$split_sort = explode(',', $sort);
		$split_order = explode(',', $order);

		if ( isset($_GET['sort']) && isset($_GET['order']) ) {
			if ( is_array($split_sort) && count($split_sort) > 0 ) {
				foreach ($split_sort as $index => $field) {
					$orders[$field] = $split_order[$index];
				}
				$query['order_by'] = $orders;
			} else {
				$query['order_by'] = "{$sort} {$order}";
			}
		}

		$data = $this->m_jabatan->get($query);

		$this->response([
			'total' => $count,
			'rows' => $data['data']
		]);
	}

	/**
	* form post handler
	*/
	public function index_post()
	{
		$rules = [
			[
				'field' => 'id_klp',
				'label' => 'Kelompok Jabatan',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'tingkat',
				'label' => 'Tingkat',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'jabatan',
				'label' => 'Jabatan',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'id_golongan',
				'label' => 'Golongan',
				'rules' => 'required|trim|xss_clean'
			],
		];

		$this->form_validation->set_rules($rules);

		if ( $this->form_validation->run() == FALSE ) {
			$this->response([
				'metadata' => [
					'code' => "500",
					'message' => strip_tags(validation_errors())
				]
			], 500);
		} else {
			$data = [
				'id_klp' => $this->input->post('id_klp'),
				'tingkat' => $this->input->post('tingkat'),
				'jabatan' => $this->input->post('jabatan'),
				'id_golongan' => $this->input->post('id_golongan')
			];

			if ( $this->m_jabatan->insert($data) ) {
				$this->response([
					'metadata' => [
						'code' => "200",
						'message' => "OK"
					]
				]);
			} else {
				$this->reponse([
					'metadata' => [
						'code' => "500",
						'message' => "Server error. Gagal menyimpan data."
					]
				], 500);
			}
		}
	}

	/**
	* update date handler
	*/
	public function index_put()
	{
		$this->form_validation->set_data($this->put());

		$id = $this->put('id', TRUE);

		$rules = [
			[
				'field' => 'id_klp',
				'label' => 'Kelompok Jabatan',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'tingkat',
				'label' => 'Tingkat',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'jabatan',
				'label' => 'Jabatan',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'id_golongan',
				'label' => 'Golongan',
				'rules' => 'required|trim|xss_clean'
			],
		];

		$this->form_validation->set_rules($rules);
		
		if ( $this->form_validation->run() == FALSE ) {
			$this->response([
				'metadata' => [
					'code' => "500",
					'message' => strip_tags(validation_errors())
				]
			], 500);
		} else {
			$data = [
				'id_klp' => $this->put('id_klp', TRUE),
				'tingkat' => $this->put('tingkat', TRUE),
				'jabatan' => $this->put('jabatan', TRUE),
				'id_golongan' => $this->put('id_golongan', TRUE)
			];

			if ( $this->m_jabatan->where('id', $id)->update($data) ) {
				$this->response([
					'metadata' => [
						'code' => "200",
						'message' => "OK"
					]
				]);
			} else {
				$this->response([
					'metadata' => [
						'code' => "500",
						'message' => "Server error. Gagal memperbarui data."
					]
				], 500);
			}
		}
	}

	/**
	 * remove golongan
	 */
	public function index_delete()
	{
		$id = $this->delete('id');

		$result = $this->m_jabatan->where('id', $id)->delete();

		if ( $result == FALSE ) {
			$this->response([
				'metadata' => [
					'code' => "500",
					'message' => "Server error. Gagal menghapus data."
				]
			], 500);
		} else {
			$this->response([
				'metadata' => [
					'code' => "200",
					'message' => "OK"
				]
			]);
		}
	}

}

/* End of file Manage_jabatan.php */
/* Location: ./application/controllers/Manage_jabatan.php */