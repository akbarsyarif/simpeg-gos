<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tabs extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('tabs/tabs', $this->data);
	}

}

/* End of file Tabs.php */
/* Location: ./application/controllers/Tabs.php */