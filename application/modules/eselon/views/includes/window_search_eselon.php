<div id="window-search-eselon"
	title="Pencarian Eselon",
	data-options="width:500,iconCls:'icon-search',modal:true"
	style="display:none">
	<form id="form-search-eselon" method="post" class="form-horizontal" role="form" onSubmit="doSearchEselon(this); return false" onReset="doRefreshEselon(this)">
		<div class="panel-content">
			<div class="form-group">
				<label for="name" class="col-xs-4 control-label">Eselon</label>
				<div class="col-xs-8">
					<input class="form-control input-sm easyui-validatebox" data-options="required:true" type="text" name="eselon" />
				</div>
			</div>
			<div class="form-group">
				<label for="group_id" class="col-xs-4 control-label">Golongan Minimal</label>
				<div class="col-xs-8">
					<?= dinamyc_dropdown(array(
						'name' => 'gol_min',
						'table' => 'golongan',
						'key' => 'id',
						'label' => 'golongan',
						'default' => '',
						'empty_first' => TRUE,
						'empty_first_label' => '- Pilih Golongan -',
						'attr' => 'class="form-control input-sm easyui-validatebox" data-options="required:true"'
					)) ?>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-xs-9 col-xs-offset-3">
					<button type="submit" class="btn btn-sm btn-default btn-round"><i class="fa fa-fw fa-search"></i> Cari</button>
					<button type="reset" class="btn btn-sm btn-default btn-round"><i class="fa fa-fw fa-refresh"></i> Segarkan</button>
				</div>
			</div>
		</div>
	</form>
</div>
