<script type="text/javascript">
	function addEselon(obj) {
		$('#window-form-eselon').dialog({
			iconCls: "icon-add",
			title: "Tambah Eselon",
			onBeforeOpen: function() {
				// clear form fields
				$('#form-eselon').form('reset');

				// enabled validation for username, password and repeat_password
				$('#form-eselon').find('[name="eselon"]').removeAttr('disabled', '').validatebox('enableValidation');
				$('#form-eselon').find('[name="gol_min"]').removeAttr('disabled', '').validatebox('enableValidation');
			}
		});
	}

	function editEselon(obj) {
		var data = $('#table-eselon').datagrid('getSelected');
		
		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin diedit', 'warning');
		} else {
			$('#window-form-eselon').dialog({
				iconCls: "icon-edit",
				title: "Edit Eselon",
				onBeforeOpen: function() {
					// set fields value
					$('#form-eselon').find('[name="id"]').val(data.id);
					$('#form-eselon').find('[name="eselon"]').val(data.eselon);
					$('#form-eselon').find('[name="gol_min"]').val(data.gol_min);

					// disabled validation for username, password and repeat_password
					$('#form-eselon').find('[name="eselon"]').removeAttr('disabled', '').validatebox('enableValidation');
					$('#form-eselon').find('[name="gol_min"]').removeAttr('disabled', '').validatebox('enableValidation');
				}
			});
		}
	}

	function doSubmitEselonForm(obj) {
		let data = $(obj).serialize();
		let id = $(obj).find('[name="id"]').val();
		if ( typeof id !== "undefined" && id !== "" ) {
			var method = "PUT";
		} else {
			var method = "POST";
		}

		$.ajax({
			url: '<?= site_url('api/eselon/manage_eselon') ?>',
			method: method,
			dataType: 'json',
			data: data,
			beforeSend: function(xhr) {
				isValid = $(obj).form('validate');
				if ( isValid ) {
					$(obj).find('[type="submit"]').button('loading');
				}
				return isValid;
			},
			success: function(xhr) {
				if ( typeof xhr === "object" ) {
					if ( xhr.metadata.code != 200 ) {
						$.messager.alert('Error', xhr.metadata.message, 'error');
					} else {
						if ( method === "PUT" ) {
							$('#window-form-eselon').dialog('close');
						}
						$('#table-eselon').datagrid('reload');
						$(obj).form('reset');
					}
				} else {
					$.messager.alert('Error', 'Server error', 'error');
					console.log(xhr);
				}
			},
			error: function(xhr) {
				let error = JSON.parse(xhr.responseText);
				if ( typeof error == "object" ) {
					var message = error.metadata.message;
				} else {
					var message = "Tidak dapat menghubungi server";
				}
				$.messager.alert('Error', message, 'error');
				console.log(error);
			},
			complete: function() {
				$(obj).find('[type="submit"]').button('reset');
			}
		});
	}

	function doResetEselonForm(obj) {
		$('#window-form-eselon').dialog('close');
		$('#form-eselon').form('clear');
	}

	function removeEselon(obj) {
		var data = $('#table-eselon').datagrid('getSelected');

		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin dihapus', 'warning');
		} else {
			$.messager.confirm('Hapus Data', 'Anda yakin ingin menghapus data pengguna <b>'+ data.eselon +'</b>?', function(r) {
				if ( r ) {
					$('#table-eselon').datagrid('loading');

					$.ajax({
						url: '<?= site_url('api/eselon/manage_eselon') ?>',
						method: 'delete',
						data: {
							id: data.id
						},
						dataType: 'json',
						success: function(xhr) {
							if ( typeof xhr === "object" ) {
								if ( xhr.metadata.code != 200 ) {
									$.messager.alert('Error', xhr.metadata.message, 'error');
								} else {
									$('#table-eselon').datagrid('reload');
								}
							} else {
								$.messager.alert('Error', 'Server error', 'error');
								console.log(xhr);
							}
						},
						error: function(xhr) {
							let error = JSON.parse(xhr.responseText);
							if ( typeof error == "object" ) {
								var message = error.metadata.message;
							} else {
								var message = "Tidak dapat menghubungi server";
							}
							$.messager.alert('Error', message, 'error');
							console.log(error);
						},
						complete: function() {
							$('#table-eselon').datagrid('loaded');
						}
					})
				}
			});
		}
	}

	function searchEselon(obj) {
		$('#window-search-eselon').dialog();
	}

	function doSearchEselon(obj) {
		var eselon = $(obj).find('[name="eselon"]').val();
		var gol_min = $(obj).find('[name="gol_min"]').val();

		$('#table-eselon').datagrid('load', {
			eselon: eselon,
			gol_min: gol_min,
		});

		$('#window-search-eselon').dialog('close');
	}

	function doRefreshEselon(obj) {
		$('#table-eselon').datagrid('load', {});
		$('#window-search-eselon').dialog('close');
		$('#form-search-eselon').form('clear');
	}

	$(document).ready(function() {
		$('#table-eselon').datagrid({
			title: '<i class="fa fa-fw fa-list"></i> Data Eselon',
			url: '<?= site_url('api/eselon/manage_eselon/datagrid') ?>',
			method: 'get',
			pagination: true,
			fit: true,
			rownumbers: true,
			fitColumns: true,
			singleSelect: true,
			toolbar: '#toolbar-table-eselon',
			pageSize: 20,
			striped: true,
			emptyMsg: 'Tidak ada data ditemukan',
			multiSort: true,
			columns: [[
				{
					title: 'Eselon',
					field: 'eselon',
					width: 130,
					sortable: true
				},
				{
					title: 'Golongan Minimal',
					field: 'golongan_minimal',
					width: 140,
					sortable: true
				}
			]]
		});
	});
</script>