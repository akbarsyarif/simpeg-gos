<div id="window-form-eselon"
	style="display:none"
	data-options="width:500,inline:true">
	<form id="form-eselon" class="form-horizontal" role="form" onsubmit="doSubmitEselonForm(this); return false" onreset="doResetEselonForm(this)">
		<input type="hidden" name="id" />
		<div class="panel-content">
			<div class="form-group">
				<label for="name" class="col-xs-4 control-label">Eselon</label>
				<div class="col-xs-8">
					<input class="form-control input-sm easyui-validatebox" data-options="required:true" type="text" name="eselon" />
				</div>
			</div>
			<div class="form-group">
				<label for="group_id" class="col-xs-4 control-label">Golongan Minimal</label>
				<div class="col-xs-8">
					<?= dinamyc_dropdown(array(
						'name' => 'gol_min',
						'table' => 'golongan',
						'key' => 'id',
						'label' => 'golongan',
						'default' => '',
						'empty_first' => TRUE,
						'empty_first_label' => '- Pilih Golongan -',
						'attr' => 'class="form-control input-sm easyui-validatebox" data-options="required:true"'
					)) ?>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-xs-8 col-xs-offset-4">
					<button type="submit" class="btn btn-sm btn-success btn-round" data-loading-text="<i class='fa fa-fw fa-circle-o-notch fa-spin'></i> loading..."><i class="fa fa-fw fa-check-circle"></i> Simpan</button> 
					<button type="reset" class="btn btn-sm btn-default btn-round"><i class="fa fa-fw fa-times-circle"></i> Batal</button> 
				</div>
			</div>
		</div>
	</form>
</div>