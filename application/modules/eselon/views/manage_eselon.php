<div id="toolbar-table-eselon">
	<table cellpadding="0" cellspacing="0" class="tb-toolbar">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td><a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="addEselon(this)"><i class="fa fa-fw fa-plus"></i> Tambah</a></td>
						<td><a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="editEselon(this)"><i class="fa fa-fw fa-edit"></i> Edit</a></td>
						<td><a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="removeEselon(this)"><i class="fa fa-fw fa-trash"></i> Hapus</a></td>
					</tr>
				</table>
			</td>
			<td align="right">
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td><a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="searchEselon(this)"><i class="fa fa-fw fa-search"></i> Pencarian</a></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
<table id="table-eselon"></table>

<?php require 'includes/window_search_eselon.php' ?>
<?php require 'includes/window_form_eselon.php' ?>

<!-- include javascript -->
<?php require 'includes/app.js.php' ?>