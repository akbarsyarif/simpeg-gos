<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_eselon extends SP_Rest_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_eselon');
	}

	/**
	 * response for jeasyui datagrid
	 */
	public function datagrid_get()
	{
		$query=[];
		// search params
		if ( isset($_GET['eselon']) && !empty($_GET['eselon']) ) $query['where']['eselon LIKE'] = '%' . $this->input->get('eselon', TRUE) . '%';
		if ( isset($_GET['gol_min']) && !empty($_GET['gol_min']) ) $query['where']['gol_min LIKE'] = '%' . $this->input->get('gol_min', TRUE) . '%';

		$count = $this->m_eselon->count($query);
		
		// set limit and offset
		$offset = isset($_GET['page']) ? $this->input->get('page', TRUE) : 1;
		$limit  = isset($_GET['rows']) ? $this->input->get('rows', TRUE) : 10;
		$offset = ($offset - 1) * $limit;
		$query['limit_offset'] = [$limit, $offset];

		// sorting
		$sort = $this->input->get('sort', TRUE);
		$order = $this->input->get('order', TRUE);
		$split_sort = explode(',', $sort);
		$split_order = explode(',', $order);

		if ( isset($_GET['sort']) && isset($_GET['order']) ) {
			if ( is_array($split_sort) && count($split_sort) > 0 ) {
				foreach ($split_sort as $index => $field) {
					$orders[$field] = $split_order[$index];
				}
				$query['order_by'] = $orders;
			} else {
				$query['order_by'] = "{$sort} {$order}";
			}
		}

		$data = $this->m_eselon->get($query);

		$this->response([
			'total' => $count,
			'rows' => $data['data']
		]);
	}

	/**
	* form post handler
	*/
	public function index_post()
	{
		$rules=
		[
			[
				'field'=>'eselon',
				'label'=>'eselon',
				'rules'=>'required|xss_clean|is_unique[eselon.eselon]'
			],
			[
				'field'=>'gol_min',
				'label'=>'golongan minimal',
				'rules'=>'required|trim|integer|xss_clean'
			]
		];

		$this->form_validation->set_rules($rules);
		if($this->form_validation->run()== FALSE)
		{
			$this->response([
				'metadata'=>[
					'code'=>"400",
					'message'=>strip_tags(validation_errors())
				]
			]);
		}
		else
		{
			$data=
			[
				'eselon'=>$this->input->post('eselon'),
				'gol_min'=>$this->input->post('gol_min')
			];
			if($this->m_eselon->insert($data)>0)
			{
				$this->response([
					'metadata'=>[
						'code'=>"200",
						'message'=>"OK"
					]
				]);
			}
			else
			{
				$this->reponse([
					'metadata'=>[
						'code'=>"400",
						'message'=>"Gagal menyimpan data"
					]
				]);
			}
		}
	}

	/**
	* update date handler
	*/
	public function index_put()
	{
		$this->form_validation->set_data($this->put());
		$id=$this->put('id',TRUE);

		$rules=
		[
			[
				'field'=>'eselon',
				'label'=>'eselon',
				'rules'=>'required|xss_clean'
			],
			[
				'field'=>'gol_min',
				'label'=>'golongan minimal',
				'rules'=>'required|trim|integer|xss_clean'
			]
		];
		$this->form_validation->set_rules($rules);
		if($this->form_validation->run()==FALSE){
			$this->response([
				'metadata'=>[
					'code'=>"400",
					'message'=>strip_tags(validation_errors())
				]
			],400);
		} else {
			$data=[
				'eselon'=>$this->put('eselon',TRUE),
				'gol_min'=>$this->put('gol_min',TRUE)
			];
			if($this->m_eselon->where('id',$id)->update($data)){
				$this->response([
					'metadata'=>[
						'code'=>"200",
						'message'=>"OK"
					]
				]);
			}else {
				$this->response([
					'metadata'=>[
						'code'=>"400",
						'message'=>"Gagal memperbarui data"
					]
				]);
			}
		}
	}

	/**
	 * remove golongan
	 */
	public function index_delete()
	{
		$id = $this->delete('id');

		$result = $this->m_eselon->where('id', $id)->delete();

		if ( $result == FALSE ) {
			$this->response([
				'metadata' => [
					'code' => "500",
					'message' => "Server error, gagal menghapus data."
				]
			], 500);
		} else {
			$this->response([
				'metadata' => [
					'code' => "200",
					'message' => "OK"
				]
			]);
		}
	}

}

/* End of file Manage_golongan.php */
/* Location: ./application/modules/golongan/controllers/rest/Manage_golongan.php */
