<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Eselon extends MY_Model {


	public $table = 'eselon';
	public $view  = 'v_eselon';
	public $pk    = 'id';

	public function __construct()
	{
		parent::__construct();
	}

}

/* End of file M_User.php */
/* Location: ./application/models/M_User.php */
