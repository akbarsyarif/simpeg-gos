<script type="text/javascript">
	function addSkpd(obj) {
		$('#window-form-skpd').dialog({
			iconCls: "icon-add",
			title: "Tambah SKPD",
			onBeforeOpen: function() {
				// clear form fields
				$('#form-skpd').form('reset');

				// enabled validation for username, password and repeat_password
				$('#form-skpd').find('[name="skpd"]').removeAttr('disabled', '').validatebox('enableValidation');
			}
		});
	}

	function editSkpd(obj) {
		var data = $('#table-skpd').datagrid('getSelected');
		
		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin diedit', 'warning');
		} else {
			$('#window-form-skpd').dialog({
				iconCls: "icon-edit",
				title: "Edit SKPD",
				onBeforeOpen: function() {
					// set fields value
					$('#form-skpd').find('[name="id"]').val(data.id);
					$('#form-skpd').find('[name="skpd"]').val(data.skpd);

					// disabled validation for username, password and repeat_password
					$('#form-skpd').find('[name="skpd"]').removeAttr('disabled', '').validatebox('enableValidation');
				}
			});
		}
	}

	function doSubmitSkpdForm(obj) {
		let data = $(obj).serialize();
		let id = $(obj).find('[name="id"]').val();
		if ( typeof id !== "undefined" && id !== "" ) {
			var method = "PUT";
		} else {
			var method = "POST";
		}

		$.ajax({
			url: '<?= site_url('api/skpd/manage_skpd') ?>',
			method: method,
			dataType: 'json',
			data: data,
			beforeSend: function(xhr) {
				isValid = $(obj).form('validate');
				if ( isValid ) {
					$(obj).find('[type="submit"]').button('loading');
				}
				return isValid;
			},
			success: function(xhr) {
				if ( typeof xhr === "object" ) {
					if ( xhr.metadata.code != 200 ) {
						$.messager.alert('Error', xhr.metadata.message, 'error');
					} else {
						if ( method === "PUT" ) {
							$('#window-form-skpd').dialog('close');
						}
						$('#table-skpd').datagrid('reload');
						$(obj).form('reset');
					}
				} else {
					$.messager.alert('Error', 'Server error', 'error');
					console.log(xhr);
				}
			},
			error: function(xhr) {
				let error = JSON.parse(xhr.responseText);
				if ( typeof error == "object" ) {
					var message = error.metadata.message;
				} else {
					var message = "Tidak dapat menghubungi server";
				}
				$.messager.alert('Error', message, 'error');
				console.log(error);
			},
			complete: function() {
				$(obj).find('[type="submit"]').button('reset');
			}
		});
	}

	function doResetSkpdForm(obj) {
		$('#window-form-skpd').dialog('close');
		$('#form-skpd').form('clear');
	}

	function removeSkpd(obj) {
		var data = $('#table-skpd').datagrid('getSelected');

		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin dihapus', 'warning');
		} else {
			$.messager.confirm('Hapus Data', 'Anda yakin ingin menghapus data pengguna <b>'+ data.skpd +'</b>?', function(r) {
				if ( r ) {
					$('#table-skpd').datagrid('loading');

					$.ajax({
						url: '<?= site_url('api/skpd/manage_skpd') ?>',
						method: 'delete',
						data: {
							id: data.id
						},
						dataType: 'json',
						success: function(xhr) {
							if ( typeof xhr === "object" ) {
								if ( xhr.metadata.code != 200 ) {
									$.messager.alert('Error', xhr.metadata.message, 'error');
								} else {
									$('#table-skpd').datagrid('reload');
								}
							} else {
								$.messager.alert('Error', 'Server error', 'error');
								console.log(xhr);
							}
						},
						error: function(xhr) {
							let error = JSON.parse(xhr.responseText);
							if ( typeof error == "object" ) {
								var message = error.metadata.message;
							} else {
								var message = "Tidak dapat menghubungi server";
							}
							$.messager.alert('Error', message, 'error');
							console.log(error);
						},
						complete: function() {
							$('#table-skpd').datagrid('loaded');
						}
					})
				}
			});
		}
	}

	function searchSkpd(obj) {
		$('#window-search-skpd').dialog();
	}

	function doSearchSkpd(obj) {
		var g = $(obj).find('[name="skpd"]').val();

		$('#table-skpd').datagrid('load', {
			skpd: g,
		});

		$('#window-search-skpd').dialog('close');
	}

	function doRefreshSkpd(obj) {
		$('#table-skpd').datagrid('load', {});
		$('#window-search-skpd').dialog('close');
		$('#form-search-skpd').form('clear');
	}

	$(document).ready(function() {
		$('#table-skpd').datagrid({
			title: '<i class="fa fa-fw fa-bank"></i> Data SKPD',
			url: '<?= site_url('api/skpd/manage_skpd/datagrid') ?>',
			method: 'get',
			pagination: true,
			fit: true,
			rownumbers: true,
			fitColumns: true,
			singleSelect: true,
			toolbar: '#toolbar-table-skpd',
			pageSize: 20,
			striped: true,
			emptyMsg: 'Tidak ada data ditemukan',
			multiSort: true,
			columns: [[
				{
					title: 'SKPD',
					field: 'skpd',
					width: 130,
					sortable: true
				},
			]]
		});
	});
</script>