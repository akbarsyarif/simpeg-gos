<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_skpd extends SP_Rest_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_skpd');
	}

	/**
	 * response for jeasyui datagrid
	 */
	public function datagrid_get()
	{
		$query=[];

		// search params
		if ( isset($_GET['skpd']) && !empty($_GET['skpd']) ) $query['where']['skpd LIKE'] = '%' . $this->input->get('skpd', TRUE) . '%';

		$count=$this->m_skpd->count($query);

		// set limit and offset
		$offset = isset($_GET['page']) ? $this->input->get('page', TRUE) : 1;
		$limit  = isset($_GET['rows']) ? $this->input->get('rows', TRUE) : 10;
		$offset = ($offset - 1) * $limit;
		$query['limit_offset'] = [$limit, $offset];

		// sorting
		$sort = $this->input->get('sort', TRUE);
		$order = $this->input->get('order', TRUE);
		$split_sort = explode(',', $sort);
		$split_order = explode(',', $order);

		if ( isset($_GET['sort']) && isset($_GET['order']) ) {
			if ( is_array($split_sort) && count($split_sort) > 0 ) {
				foreach ($split_sort as $index => $field) {
					$orders[$field] = $split_order[$index];
				}
				$query['order_by'] = $orders;
			} else {
				$query['order_by'] = "{$sort} {$order}";
			}
		}

		$result = $this->m_skpd->get($query);

		$this->response([
			'total' => $count,
			'rows' => $result['data']
		]);
	}

	/**
	* form post handler
	*/
	public function index_post()
	{
		$rules=
		[
			[
				'field'=>'skpd',
				'label'=>'nama SKPD',
				'rules'=>'required|xss_clean|min_length[5]|is_unique[skpd.skpd]'
			]
		];

		$this->form_validation->set_rules($rules);
		if($this->form_validation->run()== FALSE)
		{
			$this->response([
				'metadata'=>[
					'code'=>"400",
					'message'=>strip_tags(validation_errors())
				]
			]);
		}
		else
		{
			$data=
			[
				'skpd'=>$this->input->post('skpd'),
			];
			if($this->m_skpd->insert($data)>0)
			{
				$this->response([
					'metadata'=>[
						'code'=>"200",
						'message'=>"OK"
					]
				]);
			}
			else
			{
				$this->reponse([
					'metadata'=>[
						'code'=>"400",
						'message'=>"Gagal menyimpan data"
					]
				]);
			}
		}
	}

	/**
	* update date handler
	*/
	public function index_put()
	{
		$this->form_validation->set_data($this->put());
		$id=$this->put('id',TRUE);

		$rules=
		[
			[
				'field'=>'skpd',
				'label'=>'nama SKPD',
				'rules'=>'required|min_length[5]|xss_clean'
			]
		];
		$this->form_validation->set_rules($rules);
		if($this->form_validation->run()==FALSE){
			$this->response([
				'metadata'=>[
					'code'=>"400",
					'message'=>strip_tags(validation_errors())
				]
			]);
		} else {
			$data=[
				'skpd'=>$this->put('skpd',TRUE)
			];
			if($this->m_skpd->where('id',$id)->update($data)){
				$this->response([
					'metadata'=>[
						'code'=>"200",
						'message'=>"OK"
					]
				]);
			}else {
				$this->response([
					'metadata'=>[
						'code'=>"400",
						'message'=>"Gagal memperbarui data"
					]
				]);
			}
		}
	}

	/**
	 * remove golongan
	 */
	public function index_delete()
	{
		$id = $this->delete('id');

		$result = $this->m_skpd->where('id', $id)->delete();

		if ( $result == FALSE ) {
			$this->response([
				'metadata' => [
					'code' => "500",
					'message' => "Server error, gagal menghapus data."
				]
			], 500);
		} else {
			$this->response([
				'metadata' => [
					'code' => "200",
					'message' => "OK"
				]
			]);
		}
	}

}

/* End of file Manage_golongan.php */
/* Location: ./application/modules/golongan/controllers/rest/Manage_golongan.php */
