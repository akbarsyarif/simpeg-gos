<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Skpd extends MY_Model {

	public $table = 'skpd';
	public $pk    = 'id';
	public $view  = 'v_skpd';

	public function __construct()
	{
		parent::__construct();
	}
}

/* End of file M_User.php */
/* Location: ./application/models/M_User.php */
