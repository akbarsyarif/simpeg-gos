<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->template->build('index', $this->data);
	}
}
