<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Jurusan extends MY_Model {

	public $table = 'pendidikan_jurusan';
	public $pk    = 'id';
	public $view  = 'v_pend_jur';

	public function __construct()
	{
		parent::__construct();
	}
}

/* End of file M_User.php */
/* Location: ./application/models/M_User.php */
