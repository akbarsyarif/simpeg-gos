<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_jurusan extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('manage_jurusan', $this->data);
	}

}

/* End of file Manage_golongan.php */
/* Location: ./application/modules/golongan/controllers/Manage_golongan.php */
