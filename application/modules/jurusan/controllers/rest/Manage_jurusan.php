<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_jurusan extends SP_Rest_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_jurusan');
	}

	/**
	 * response for jeasyui datagrid
	 */
	public function datagrid_get()
	{
		$query=[];

		// search params
		if ( isset($_GET['jurusan']) && !empty($_GET['jurusan']) ) $query['where']['jurusan LIKE'] = '%' . $this->input->get('jurusan', TRUE) . '%';
		if ( isset($_GET['id_pendidikan']) && !empty($_GET['id_pendidikan']) ) $query['where']['id_pendidikan'] = $this->input->get('id_pendidikan', TRUE);
		if ( isset($_GET['id_bidang_keilmuan']) && !empty($_GET['id_bidang_keilmuan']) ) $query['where']['id_bidang_keilmuan'] = $this->input->get('id_bidang_keilmuan', TRUE);

		$count=$this->m_jurusan->count($query);

		// set limit and offset
		$offset = isset($_GET['page']) ? $this->input->get('page', TRUE) : 1;
		$limit  = isset($_GET['rows']) ? $this->input->get('rows', TRUE) : 10;
		$offset = ($offset - 1) * $limit;
		$query['limit_offset'] = [$limit, $offset];
		
		// sorting
		$sort = $this->input->get('sort', TRUE);
		$order = $this->input->get('order', TRUE);
		$split_sort = explode(',', $sort);
		$split_order = explode(',', $order);

		if ( isset($_GET['sort']) && isset($_GET['order']) ) {
			if ( is_array($split_sort) && count($split_sort) > 0 ) {
				foreach ($split_sort as $index => $field) {
					$orders[$field] = $split_order[$index];
				}
				$query['order_by'] = $orders;
			} else {
				$query['order_by'] = "{$sort} {$order}";
			}
		}

		$result = $this->m_jurusan->get($query);

		$this->response([
			'total' => $count,
			'rows' => $result['data']
		]);
	}

	/**
	* form post handler
	*/
	public function index_post()
	{
		$rules=
		[
			[
				'field'=>'jurusan',
				'label'=>'jurusan',
				'rules'=>'required|xss_clean|min_length[3]|is_unique[pendidikan_jurusan.jurusan]'
			],
			[
				'field'=>'id_pendidikan',
				'label'=>'pendidikan',
				'rules'=>'required|integer'
			]
		];

		$this->form_validation->set_rules($rules);
		if($this->form_validation->run()== FALSE)
		{
			$this->response([
				'metadata'=>[
					'code'=>"400",
					'message'=>strip_tags(validation_errors())
				]
			]);
		}
		else
		{
			$id_bidang_keilmuan=(int) $this->input->post('id_bidang_keilmuan');
			$data=
			[
				'jurusan'=>$this->input->post('jurusan'),
				'id_pendidikan'=>$this->input->post('id_pendidikan'),
				'id_bidang_keilmuan'=>($id_bidang_keilmuan==0 ? NULL : $id_bidang_keilmuan)
			];
			if($this->m_jurusan->insert($data)>0)
			{
				$this->response([
					'metadata'=>[
						'code'=>"200",
						'message'=>"OK"
					]
				]);
			}
			else
			{
				$this->reponse([
					'metadata'=>[
						'code'=>"400",
						'message'=>"Gagal menyimpan data"
					]
				]);
			}
		}
	}

	/**
	* update date handler
	*/
	public function index_put()
	{
		$this->form_validation->set_data($this->put());
		$id=$this->put('id',TRUE);

		$rules=
		[
			[
				'field'=>'jurusan',
				'label'=>'jurusan',
				'rules'=>'required|xss_clean|min_length[3]'
			],
			[
				'field'=>'id_pendidikan',
				'label'=>'pendidikan',
				'rules'=>'required|integer'
			]
		];
		$this->form_validation->set_rules($rules);
		if($this->form_validation->run()==FALSE){
			$this->response([
				'metadata'=>[
					'code'=>"400",
					'message'=>strip_tags(validation_errors())
				]
			]);
		} else {
			$id_bidang_keilmuan=(int) $this->put('id_bidang_keilmaun');
			$data=[
				'jurusan'=>$this->put('jurusan'),
				'id_pendidikan'=>$this->put('id_pendidikan'),
				'id_bidang_keilmuan'=>($id_bidang_keilmuan==0 ? NULL : $id_bidang_keilmuan)
			];
			if($this->m_jurusan->where('id',$id)->update($data)){
				$this->response([
					'metadata'=>[
						'code'=>"200",
						'message'=>"OK"
					]
				]);
			}else {
				$this->response([
					'metadata'=>[
						'code'=>"400",
						'message'=>"Gagal memperbarui data"
					]
				]);
			}
		}
	}

	/**
	 * remove golongan
	 */
	public function index_delete()
	{
		$id = $this->delete('id');

		$result = $this->m_jurusan->where('id', $id)->delete();

		if ( $result == FALSE ) {
			$this->response([
				'metadata' => [
					'code' => "500",
					'message' => "Server error, gagal menghapus data."
				]
			], 500);
		} else {
			$this->response([
				'metadata' => [
					'code' => "200",
					'message' => "OK"
				]
			]);
		}
	}

}

/* End of file Manage_golongan.php */
/* Location: ./application/modules/golongan/controllers/rest/Manage_golongan.php */
