<script type="text/javascript">
	function addJurusan(obj) {
		$('#window-form-jurusan').dialog({
			iconCls: "icon-add",
			title: "Tambah Jurusan",
			onBeforeOpen: function() {
				// clear form fields
				$('#form-jurusan').form('reset');

				// enabled validation for username, password and repeat_password
				$('#form-jurusan').find('[name="jurusan"]').removeAttr('disabled', '').validatebox('enableValidation');
				$('#form-jurusan').find('[name="id_pendidikan"]').removeAttr('disabled', '').validatebox('enableValidation');
			}
		});
	}

	function editJurusan(obj) {
		var data = $('#table-jurusan').datagrid('getSelected');
		
		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin diedit', 'warning');
		} else {
			$('#window-form-jurusan').dialog({
				iconCls: "icon-edit",
				title: "Edit Jurusan",
				onBeforeOpen: function() {
					// set fields value
					$('#form-jurusan').find('[name="id"]').val(data.id);
					$('#form-jurusan').find('[name="jurusan"]').val(data.jurusan);
					$('#form-jurusan').find('[name="id_pendidikan"]').val(data.id_pendidikan);
					$('#form-jurusan').find('[name="id_bidang_keilmuan"]').val(data.id_bidang_keilmuan);

					// disabled validation for username, password and repeat_password
					$('#form-jurusan').find('[name="jurusan"]').removeAttr('disabled', '').validatebox('enableValidation');
					$('#form-jurusan').find('[name="id_pendidikan"]').removeAttr('disabled', '').validatebox('enableValidation');
				}
			});
		}
	}

	function doSubmitJurusanForm(obj) {
		let data = $(obj).serialize();
		let id = $(obj).find('[name="id"]').val();
		if ( typeof id !== "undefined" && id !== "" ) {
			var method = "PUT";
		} else {
			var method = "POST";
		}

		$.ajax({
			url: '<?= site_url('api/jurusan/manage_jurusan') ?>',
			method: method,
			dataType: 'json',
			data: data,
			beforeSend: function(xhr) {
				isValid = $(obj).form('validate');
				if ( isValid ) {
					$(obj).find('[type="submit"]').button('loading');
				}
				return isValid;
			},
			success: function(xhr) {
				if ( typeof xhr === "object" ) {
					if ( xhr.metadata.code != 200 ) {
						$.messager.alert('Error', xhr.metadata.message, 'error');
					} else {
						if ( method === "PUT" ) {
							$('#window-form-jurusan').dialog('close');
						}
						$('#table-jurusan').datagrid('reload');
						$(obj).form('reset');
					}
				} else {
					$.messager.alert('Error', 'Server error', 'error');
					console.log(xhr);
				}
			},
			error: function(xhr) {
				let error = JSON.parse(xhr.responseText);
				if ( typeof error == "object" ) {
					var message = error.metadata.message;
				} else {
					var message = "Tidak dapat menghubungi server";
				}
				$.messager.alert('Error', message, 'error');
				console.log(error);
			},
			complete: function() {
				$(obj).find('[type="submit"]').button('reset');
			}
		});
	}

	function doResetJurusanForm(obj) {
		$('#window-form-jurusan').dialog('close');
		$('#form-jurusan').form('clear');
	}

	function removeJurusan(obj) {
		var data = $('#table-jurusan').datagrid('getSelected');

		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin dihapus', 'warning');
		} else {
			$.messager.confirm('Hapus Data', 'Anda yakin ingin menghapus data pengguna <b>'+ data.jurusan +'</b>?', function(r) {
				if ( r ) {
					$('#table-jurusan').datagrid('loading');

					$.ajax({
						url: '<?= site_url('api/jurusan/manage_jurusan') ?>',
						method: 'delete',
						data: {
							id: data.id
						},
						dataType: 'json',
						success: function(xhr) {
							if ( typeof xhr === "object" ) {
								if ( xhr.metadata.code != 200 ) {
									$.messager.alert('Error', xhr.metadata.message, 'error');
								} else {
									$('#table-jurusan').datagrid('reload');
								}
							} else {
								$.messager.alert('Error', 'Server error', 'error');
								console.log(xhr);
							}
						},
						error: function(xhr) {
							let error = JSON.parse(xhr.responseText);
							if ( typeof error == "object" ) {
								var message = error.metadata.message;
							} else {
								var message = "Tidak dapat menghubungi server";
							}
							$.messager.alert('Error', message, 'error');
							console.log(error);
						},
						complete: function() {
							$('#table-jurusan').datagrid('loaded');
						}
					})
				}
			});
		}
	}

	function searchJurusan(obj) {
		$('#window-search-jurusan').dialog();
	}

	function doSearchJurusan(obj) {
		var j = $(obj).find('[name="jurusan"]').val();
		var p = $(obj).find('[name="id_pendidikan"]').val();
		var b = $(obj).find('[name="id_bidang_keilmuan"]').val();

		$('#table-jurusan').datagrid('load', {
			jurusan: j,
			id_pendidikan: p,
			id_bidang_keilmuan: b
		});

		$('#window-search-jurusan').dialog('close');
	}

	function doRefreshJurusan(obj) {
		$('#table-jurusan').datagrid('load', {});
		$('#window-search-jurusan').dialog('close');
		$('#form-search-jurusan').form('clear');
	}

	$(document).ready(function() {
		$('#table-jurusan').datagrid({
			title: '<i class="fa fa-fw fa-mortar-board"></i> Data Jurusan',
			url: '<?= site_url('api/jurusan/manage_jurusan/datagrid') ?>',
			method: 'get',
			pagination: true,
			fit: true,
			rownumbers: true,
			fitColumns: true,
			singleSelect: true,
			toolbar: '#toolbar-table-jurusan',
			pageSize: 20,
			striped: true,
			emptyMsg: 'Tidak ada data ditemukan',
			multiSort: true,
			columns: [[
				{
					title: 'Jurusan',
					field: 'jurusan',
					width: 130,
					sortable: true
				},
				{
					title: 'Pendidikan',
					field: 'pendidikan',
					width: 140,
					sortable: true
				},
				{
					title: 'Bidang Keilmuan',
					field: 'keilmuan',
					width: 140,
					sortable: true
				}
			]]
		});
	});
</script>