<div id="window-form-jurusan"
	style="display:none"
	data-options="width:500,inline:true">
	<form id="form-jurusan" class="form-horizontal" role="form" onsubmit="doSubmitJurusanForm(this); return false" onreset="doResetJurusanForm(this)">
		<input type="hidden" name="id" />
		<div class="panel-content">
			<div class="form-group">
				<label for="name" class="col-xs-4 control-label">Jurusan</label>
				<div class="col-xs-8">
					<input class="form-control input-sm easyui-validatebox" data-options="required:true" type="text" name="jurusan" />
				</div>
			</div>
			<div class="form-group">
				<label for="group_id" class="col-xs-4 control-label">Pendidikan</label>
				<div class="col-xs-8">
					<?= dinamyc_dropdown(array(
						'name' => 'id_pendidikan',
						'table' => 'pendidikan',
						'key' => 'id',
						'label' => 'pendidikan',
						'default' => '',
						'empty_first' => TRUE,
						'empty_first_label' => '- Pilih Pendidikan -',
						'attr' => 'class="form-control input-sm easyui-validatebox" data-options="required:true"'
					)) ?>
				</div>
			</div>
			<div class="form-group">
				<label for="group_id" class="col-xs-4 control-label">Bidang Keilmuan</label>
				<div class="col-xs-8">
					<?= dinamyc_dropdown(array(
						'name' => 'id_bidang_keilmuan',
						'table' => 'bidang_keilmuan',
						'key' => 'id',
						'label' => 'bidang_keilmuan',
						'default' => '',
						'empty_first' => TRUE,
						'empty_first_label' => '- Pilih Bidang Keilmuan -',
						'attr' => 'class="form-control input-sm easyui-validatebox"'
					)) ?>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-xs-8 col-xs-offset-4">
					<button type="submit" class="btn btn-sm btn-success btn-round" data-loading-text="<i class='fa fa-fw fa-circle-o-notch fa-spin'></i> loading..."><i class="fa fa-fw fa-check-circle"></i> Simpan</button> 
					<button type="reset" class="btn btn-sm btn-default btn-round"><i class="fa fa-fw fa-times-circle"></i> Batal</button> 
				</div>
			</div>
		</div>
	</form>
</div>