<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_kelompok_jabatan_2 extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('manage_kelompok_jabatan_2/manage_kelompok_jabatan_2', $this->data);
	}

}

/* End of file Manage_kelompok_jabatan_2.php */
/* Location: ./application/controllers/Manage_kelompok_jabatan_2.php */