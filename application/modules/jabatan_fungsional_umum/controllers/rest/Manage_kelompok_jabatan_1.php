<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_kelompok_jabatan_1 extends SP_Rest_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_kelompok_jabatan_1');
	}

	/**
	 * response for jeasyui datagrid
	 */
	public function datagrid_get()
	{
		$query = [];

		// search params
		if ( isset($_GET['kode']) && !empty($_GET['kode']) ) $query['where']['kode LIKE'] = '%' . $this->input->get('kode', TRUE) . '%';
		if ( isset($_GET['kelompok']) && !empty($_GET['kelompok']) ) $query['where']['kelompok LIKE'] = '%' . $this->input->get('kelompok', TRUE) . '%';
		
		$count = $this->m_kelompok_jabatan_1->count($query);

		// set limit and offset
		$offset = isset($_GET['page']) ? $this->input->get('page', TRUE) : 1;
		$limit  = isset($_GET['rows']) ? $this->input->get('rows', TRUE) : 10;
		$offset = ($offset - 1) * $limit;
		$query['limit_offset'] = [$limit, $offset];

		// sorting
		$sort = $this->input->get('sort', TRUE);
		$order = $this->input->get('order', TRUE);
		$split_sort = explode(',', $sort);
		$split_order = explode(',', $order);

		if ( isset($_GET['sort']) && isset($_GET['order']) ) {
			if ( is_array($split_sort) && count($split_sort) > 0 ) {
				foreach ($split_sort as $index => $field) {
					$orders[$field] = $split_order[$index];
				}
				$query['order_by'] = $orders;
			} else {
				$query['order_by'] = "{$sort} {$order}";
			}
		}

		$data = $this->m_kelompok_jabatan_1->get($query);

		$this->response([
			'total' => $count,
			'rows' => $data['data']
		]);
	}

	/**
	* form post handler
	*/
	public function index_post()
	{
		$rules = [
			[
				'field' => 'kode',
				'label' => 'Kode',
				'rules' => 'required|trim|is_unique[klp_jabatan_fungsional_umum_1.kode]|xss_clean'
			],
			[
				'field' => 'kelompok',
				'label' => 'Kelompok',
				'rules' => 'required|trim|xss_clean'
			],
		];

		$this->form_validation->set_rules($rules);

		if ( $this->form_validation->run() == FALSE ) {
			$this->response([
				'metadata' => [
					'code' => "500",
					'message' => strip_tags(validation_errors())
				]
			], 500);
		} else {
			$data = [
				'kode' => $this->input->post('kode'),
				'kelompok' => $this->input->post('kelompok')
			];

			if ( $this->m_kelompok_jabatan_1->insert($data) ) {
				$this->response([
					'metadata' => [
						'code' => "200",
						'message' => "OK"
					]
				]);
			} else {
				$this->reponse([
					'metadata' => [
						'code' => "500",
						'message' => "Server error. Gagal menyimpan data."
					]
				], 500);
			}
		}
	}

	/**
	* update date handler
	*/
	public function index_put()
	{
		$this->form_validation->set_data($this->put());

		$id = $this->put('id', TRUE);

		$rules = [
			[
				'field' => 'kode',
				'label' => 'Kode',
				'rules' => 'required|trim|conflict_kode_klp_jabatan_fungsional_umum_1['.$id.']|xss_clean'
			],
			[
				'field' => 'kelompok',
				'label' => 'Kelompok',
				'rules' => 'required|trim|xss_clean'
			],
		];

		$this->form_validation->set_rules($rules);
		
		if ( $this->form_validation->run() == FALSE ) {
			$this->response([
				'metadata' => [
					'code' => "500",
					'message' => strip_tags(validation_errors())
				]
			], 500);
		} else {
			$data = [
				'kode' => $this->put('kode', TRUE),
				'kelompok' => $this->put('kelompok', TRUE),
			];

			if ( $this->m_kelompok_jabatan_1->where('id', $id)->update($data) ) {
				$this->response([
					'metadata' => [
						'code' => "200",
						'message' => "OK"
					]
				]);
			} else {
				$this->response([
					'metadata' => [
						'code' => "500",
						'message' => "Server error. Gagal memperbarui data."
					]
				], 500);
			}
		}
	}

	/**
	 * remove golongan
	 */
	public function index_delete()
	{
		$id = $this->delete('id');

		$result = $this->m_kelompok_jabatan_1->where('id', $id)->delete();

		if ( $result == FALSE ) {
			$this->response([
				'metadata' => [
					'code' => "500",
					'message' => "Server error. Gagal menghapus data."
				]
			], 500);
		} else {
			$this->response([
				'metadata' => [
					'code' => "200",
					'message' => "OK"
				]
			]);
		}
	}

}

/* End of file Manage_kelompok_jabatan_1.php */
/* Location: ./application/controllers/Manage_kelompok_jabatan_1.php */