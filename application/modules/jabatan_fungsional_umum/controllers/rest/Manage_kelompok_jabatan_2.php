<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_kelompok_jabatan_2 extends SP_Rest_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_kelompok_jabatan_2');
	}

	/**
	 * response for jeasyui datagrid
	 */
	public function datagrid_get()
	{
		$query = [];

		// search params
		if ( isset($_GET['kode']) && !empty($_GET['kode']) ) $query['where']['kode LIKE'] = '%' . $this->input->get('kode', TRUE) . '%';
		if ( isset($_GET['kelompok']) && !empty($_GET['kelompok']) ) $query['where']['kelompok LIKE'] = '%' . $this->input->get('kelompok', TRUE) . '%';
		if ( isset($_GET['id_klp_1']) && !empty($_GET['id_klp_1']) ) $query['where']['id_klp_1'] = $this->input->get('id_klp_1', TRUE);
		
		$count = $this->m_kelompok_jabatan_2->count($query);

		// set limit and offset
		$offset = isset($_GET['page']) ? $this->input->get('page', TRUE) : 1;
		$limit  = isset($_GET['rows']) ? $this->input->get('rows', TRUE) : 10;
		$offset = ($offset - 1) * $limit;
		$query['limit_offset'] = [$limit, $offset];

		// sorting
		$sort = $this->input->get('sort', TRUE);
		$order = $this->input->get('order', TRUE);
		$split_sort = explode(',', $sort);
		$split_order = explode(',', $order);

		if ( isset($_GET['sort']) && isset($_GET['order']) ) {
			if ( is_array($split_sort) && count($split_sort) > 0 ) {
				foreach ($split_sort as $index => $field) {
					$orders[$field] = $split_order[$index];
				}
				$query['order_by'] = $orders;
			} else {
				$query['order_by'] = "{$sort} {$order}";
			}
		}

		$data = $this->m_kelompok_jabatan_2->get($query);

		$this->response([
			'total' => $count,
			'rows' => $data['data']
		]);
	}

	/**
	 * get user data
	 */
	public function index_get()
	{
		$id = $this->get('id', TRUE);

		if ( $id != NULL ) {
			$result = $this->m_kelompok_jabatan_2->row([
				'where' => [
					'id' => $id
				]
			]);

			if ($result['count'] == 0) {
				$this->response([
					'metadata' => [
						'code' => "400",
						'message' => "Data tidak ditemukan"
					]
				], 400);
			} else {
				$this->response([
					'metadata' => [
						'code' => "200",
						'message' => "OK"
					],
					'response' => $result
				]);
			}
		} else {
			$query = [];

			// search params
			if ( isset($_GET['kode']) && !empty($_GET['kode']) ) $query['where']['kode'] = $this->input->get('kode', TRUE);
			if ( isset($_GET['kelompok']) && !empty($_GET['kelompok']) ) $query['where']['kelompok'] = $this->input->get('kelompok', TRUE);
			if ( isset($_GET['id_klp_1']) && !empty($_GET['id_klp_1']) ) $query['where']['id_klp_1'] = $this->input->get('id_klp_1', TRUE);

			$count = $this->m_kelompok_jabatan_2->count($query);

			// set limit and offset
			$offset = isset($_GET['page']) ? $this->get('page', TRUE) : 1;
			$limit  = isset($_GET['rows']) ? $this->get('rows', TRUE) : 10;
			$offset = ($offset - 1) * $limit;
			$query['limit_offset'] = [$limit, $offset];

			// sorting
			$sort = $this->get('sort', TRUE);
			$order = $this->get('order', TRUE);
			if ( isset($_GET['sort']) && isset($_GET['order']) ) $query['order_by'] = "{$sort} {$order}";

			$result = $this->m_kelompok_jabatan_2->get($query);

			if ($result['count'] == 0) {
				$this->response([
					'metadata' => [
						'code' => "400",
						'message' => "Data tidak ditemukan"
					]
				], 400);
			} else {
				$this->response([
					'metadata' => [
						'code' => "200",
						'message' => "OK"
					],
					'response' => [
						'total' => $count,
						'rows' => $result['data']
					]
				]);
			}
		}
	}

	/**
	* form post handler
	*/
	public function index_post()
	{
		$rules = [
			[
				'field' => 'kode',
				'label' => 'Kode',
				'rules' => 'required|trim|is_unique[klp_jabatan_fungsional_umum_2.kode]|xss_clean'
			],
			[
				'field' => 'kelompok',
				'label' => 'Kelompok',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'id_klp_2',
				'label' => 'Kelompok Jabatan 2',
				'rules' => 'required|trim|xss_clean'
			],
		];

		$this->form_validation->set_rules($rules);

		if ( $this->form_validation->run() == FALSE ) {
			$this->response([
				'metadata' => [
					'code' => "500",
					'message' => strip_tags(validation_errors())
				]
			], 500);
		} else {
			$data = [
				'kode' => $this->input->post('kode'),
				'kelompok' => $this->input->post('kelompok'),
				'id_klp_1' => $this->input->post('id_klp_1'),
			];

			if ( $this->m_kelompok_jabatan_2->insert($data) ) {
				$this->response([
					'metadata' => [
						'code' => "200",
						'message' => "OK"
					]
				]);
			} else {
				$this->reponse([
					'metadata' => [
						'code' => "500",
						'message' => "Server error. Gagal menyimpan data."
					]
				], 500);
			}
		}
	}

	/**
	* update date handler
	*/
	public function index_put()
	{
		$this->form_validation->set_data($this->put());

		$id = $this->put('id', TRUE);

		$rules = [
			[
				'field' => 'kode',
				'label' => 'Kode',
				'rules' => 'required|trim|conflict_kode_klp_jabatan_fungsional_umum_2['.$id.']|xss_clean'
			],
			[
				'field' => 'kelompok',
				'label' => 'Kelompok',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'id_klp_1',
				'label' => 'Kelompok Jabatan 1',
				'rules' => 'required|trim|xss_clean'
			],
		];

		$this->form_validation->set_rules($rules);
		
		if ( $this->form_validation->run() == FALSE ) {
			$this->response([
				'metadata' => [
					'code' => "500",
					'message' => strip_tags(validation_errors())
				]
			], 500);
		} else {
			$data = [
				'kode' => $this->put('kode', TRUE),
				'kelompok' => $this->put('kelompok', TRUE),
				'id_klp_1' => $this->put('id_klp_1', TRUE),
			];

			if ( $this->m_kelompok_jabatan_2->where('id', $id)->update($data) ) {
				$this->response([
					'metadata' => [
						'code' => "200",
						'message' => "OK"
					]
				]);
			} else {
				$this->response([
					'metadata' => [
						'code' => "500",
						'message' => "Server error. Gagal memperbarui data."
					]
				], 500);
			}
		}
	}

	/**
	 * remove golongan
	 */
	public function index_delete()
	{
		$id = $this->delete('id');

		$result = $this->m_kelompok_jabatan_2->where('id', $id)->delete();

		if ( $result == FALSE ) {
			$this->response([
				'metadata' => [
					'code' => "500",
					'message' => "Server error. Gagal menghapus data."
				]
			], 500);
		} else {
			$this->response([
				'metadata' => [
					'code' => "200",
					'message' => "OK"
				]
			]);
		}
	}

	public function combobox_get()
	{
		$id_klp_1 = $this->get('id_klp_1', TRUE);
		
		$query = [];
		
		if ( !is_null($id_klp_1) && !empty($id_klp_1) ) $query['where']['id_klp_1'] = $id_klp_1;

		$result = $this->m_kelompok_jabatan_2->get($query);

		if ( $id_klp_1 == FALSE OR $result == FALSE ) {
			$this->response([]);
		} else {
			$this->response($result['data']);
		}
	}

}

/* End of file Manage_kelompok_jabatan_2.php */
/* Location: ./application/controllers/Manage_kelompok_jabatan_2.php */