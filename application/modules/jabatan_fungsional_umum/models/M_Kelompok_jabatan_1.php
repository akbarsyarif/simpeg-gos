<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Kelompok_jabatan_1 extends MY_Model {

	public $table = 'klp_jabatan_fungsional_umum_1';
	public $view  = 'v_klp_jabatan_fungsional_umum_1';
	public $pk    = 'id';

	public function __construct()
	{
		parent::__construct();
	}

}

/* End of file M_Kelompok_jabatan_1.php */
/* Location: ./application/models/M_Kelompok_jabatan_1.php */