<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Jabatan extends MY_Model {

	public $table = 'jabatan_fungsional_umum';
	public $view  = 'v_jabatan_fungsional_umum';
	public $pk    = 'id';

	public function __construct()
	{
		parent::__construct();
	}

}

/* End of file M_Jabatan.php */
/* Location: ./application/models/M_Jabatan.php */