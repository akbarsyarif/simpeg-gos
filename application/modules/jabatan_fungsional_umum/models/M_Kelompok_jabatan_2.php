<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Kelompok_jabatan_2 extends MY_Model {

	public $table = 'klp_jabatan_fungsional_umum_2';
	public $view  = 'v_klp_jabatan_fungsional_umum_2';
	public $pk    = 'id';

	public function __construct()
	{
		parent::__construct();
	}

}

/* End of file M_Kelompok_jabatan_2.php */
/* Location: ./application/models/M_Kelompok_jabatan_2.php */