<div id="window-form-klp-jabatan-fungsional-umum-2"
	style="display:none"
	data-options="width:500,inline:true">
	<form id="form-klp-jabatan-fungsional-umum-2" class="form-horizontal" role="form" onsubmit="doSubmitFormKlpJabatanFungsionalUmum2(this); return false" onreset="doResetFormKlpJabatanFungsionalUmum2(this)">
		<input type="hidden" name="id" />
		<div class="panel-content">
			<div class="form-group">
				<label for="kode" class="col-xs-4 control-label">Kode</label>
				<div class="col-xs-8">
					<input type="text" name="kode" class="form-control input-sm easyui-validatebox" data-options="required:true" />
				</div>
			</div>
			<div class="form-group">
				<label for="kelompok" class="col-xs-4 control-label">Kelompok</label>
				<div class="col-xs-8">
					<input type="text" name="kelompok" class="form-control input-sm easyui-validatebox" data-options="required:true" />
				</div>
			</div>
			<div class="form-group">
				<label for="id_klp_1" class="col-xs-4 control-label">Kelompok Jabatan 1</label>
				<div class="col-xs-8">
					<?= dinamyc_dropdown(array(
						'name' => 'id_klp_1',
						'table' => 'v_klp_jabatan_fungsional_umum_1',
						'key' => 'id',
						'label' => ['kode', 'kelompok'],
						'default' => '',
						'empty_first' => TRUE,
						'empty_first_label' => '- Pilih Kelompok Jabatan 1 -',
						'attr' => 'id="id_klp_1" class="form-control input-sm easyui-validatebox easyui-combobox" style="width:100%" data-options="required:true"'
					)) ?>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-xs-8 col-xs-offset-4">
					<button type="submit" class="btn btn-sm btn-success btn-round" data-loading-text="<i class='fa fa-fw fa-circle-o-notch fa-spin'></i> loading..."><i class="fa fa-fw fa-check-circle"></i> Simpan</button> 
					<button type="reset" class="btn btn-sm btn-default btn-round"><i class="fa fa-fw fa-times-circle"></i> Batal</button> 
				</div>
			</div>
		</div>
	</form>
</div>