<script type="text/javascript">
	function addKlpJabatanFungsionalUmum2(obj) {
		$('#window-form-klp-jabatan-fungsional-umum-2').dialog({
			iconCls: "icon-add",
			title: "Tambah Jabatan",
			onBeforeOpen: function() {
				// clear form fields
				$('#form-klp-jabatan-fungsional-umum-2').form('reset');
			}
		});
	}

	function editKlpJabatanFungsionalUmum2(obj) {
		var data = $('#table-klp-jabatan-fungsional-umum-2').datagrid('getSelected');
		
		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin diedit', 'warning');
		} else {
			$('#window-form-klp-jabatan-fungsional-umum-2').dialog({
				iconCls: "icon-edit",
				title: "Edit Jabatan",
				onBeforeOpen: function() {
					// set fields value
					$('#form-klp-jabatan-fungsional-umum-2').find('[name="id"]').val(data.id);
					$('#form-klp-jabatan-fungsional-umum-2').find('[name="kode"]').val(data.kode);
					$('#form-klp-jabatan-fungsional-umum-2').find('[name="kelompok"]').val(data.kelompok);
					$('#form-klp-jabatan-fungsional-umum-2').find('#id_klp_1').combobox('select', data.id_klp_1);
				}
			});
		}
	}

	function doSubmitFormKlpJabatanFungsionalUmum2(obj) {
		let data = $(obj).serialize();
		let id = $(obj).find('[name="id"]').val();
		if ( typeof id !== "undefined" && id !== "" ) {
			var method = "PUT";
		} else {
			var method = "POST";
		}

		$.ajax({
			url: '<?= site_url('api/jabatan_fungsional_umum/manage_kelompok_jabatan_2') ?>',
			method: method,
			dataType: 'json',
			data: data,
			beforeSend: function(xhr) {
				isValid = $(obj).form('validate');
				if ( isValid ) {
					$(obj).find('[type="submit"]').button('loading');
				}
				return isValid;
			},
			success: function(xhr) {
				if ( typeof xhr === "object" ) {
					if ( xhr.metadata.code != 200 ) {
						$.messager.alert('Error', xhr.metadata.message, 'error');
					} else {
						if ( method === "PUT" ) {
							$('#window-form-klp-jabatan-fungsional-umum-2').dialog('close');
						}
						$('#table-klp-jabatan-fungsional-umum-2').datagrid('reload');
						$(obj).form('reset');
					}
				} else {
					$.messager.alert('Error', 'Server error', 'error');
					console.log(xhr);
				}
			},
			error: function(xhr) {
				let error = JSON.parse(xhr.responseText);
				if ( typeof error == "object" ) {
					var message = error.metadata.message;
				} else {
					var message = "Tidak dapat menghubungi server";
				}
				$.messager.alert('Error', message, 'error');
				console.log(error);
			},
			complete: function() {
				$(obj).find('[type="submit"]').button('reset');
			}
		});
	}

	function doResetFormKlpJabatanFungsionalUmum2(obj) {
		$('#window-form-klp-jabatan-fungsional-umum-2').dialog('close');
		$(obj).form('clear');
	}

	function removeKlpJabatanFungsionalUmum2(obj) {
		var data = $('#table-klp-jabatan-fungsional-umum-2').datagrid('getSelected');

		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin dihapus', 'warning');
		} else {
			$.messager.confirm('Hapus Data', 'Anda yakin ingin menghapus data kelompok jabatan <b>'+ data.kelompok +'</b>?', function(r) {
				if ( r ) {
					$('#table-klp-jabatan-fungsional-umum-2').datagrid('loading');

					$.ajax({
						url: '<?= site_url('api/jabatan_fungsional_umum/manage_kelompok_jabatan_2') ?>',
						method: 'delete',
						data: {
							id: data.id
						},
						dataType: 'json',
						success: function(xhr) {
							if ( typeof xhr === "object" ) {
								if ( xhr.metadata.code != 200 ) {
									$.messager.alert('Error', xhr.metadata.message, 'error');
								} else {
									$('#table-klp-jabatan-fungsional-umum-2').datagrid('reload');
								}
							} else {
								$.messager.alert('Error', 'Server error', 'error');
								console.log(xhr);
							}
						},
						error: function(xhr) {
							let error = JSON.parse(xhr.responseText);
							if ( typeof error == "object" ) {
								var message = error.metadata.message;
							} else {
								var message = "Tidak dapat menghubungi server";
							}
							$.messager.alert('Error', message, 'error');
							console.log(error);
						},
						complete: function() {
							$('#table-klp-jabatan-fungsional-umum-2').datagrid('loaded');
						}
					})
				}
			});
		}
	}

	function searchKlpJabatanFungsionalUmum2(obj) {
		$('#window-search-klp-jabatan-fungsional-umum-2').dialog();
	}

	function doSearchKlpJabatanFungsionalUmum2(obj) {
		var kode = $(obj).find('[name="kode"]').val();
		var kelompok = $(obj).find('[name="kelompok"]').val();
		var id_klp_1 = $(obj).find('[name="id_klp_1"]').val();

		$('#table-klp-jabatan-fungsional-umum-2').datagrid('load', {
			kode: kode,
			kelompok: kelompok,
			id_klp_1: id_klp_1
		});

		$('#window-search-klp-jabatan-fungsional-umum-2').dialog('close');
	}

	function doRefreshKlpJabatanFungsionalUmum2(obj) {
		$('#table-klp-jabatan-fungsional-umum-2').datagrid('load', {});
		$('#window-search-klp-jabatan-fungsional-umum-2').dialog('close');
		$(obj).form('clear');
	}

	$(document).ready(function() {
		$('#table-klp-jabatan-fungsional-umum-2').datagrid({
			title: '<i class="fa fa-fw fa-list"></i> Jabatan',
			url: '<?= site_url('api/jabatan_fungsional_umum/manage_kelompok_jabatan_2/datagrid') ?>',
			method: 'get',
			pagination: true,
			fit: true,
			rownumbers: true,
			fitColumns: true,
			singleSelect: true,
			toolbar: '#toolbar-table-klp-jabatan-fungsional-umum-2',
			pageSize: 20,
			striped: true,
			emptyMsg: 'Tidak ada data ditemukan',
			multiSort: true,
			columns: [[
				{
					title: 'Kode',
					field: 'kode',
					width: '10%',
					align: 'center',
					sortable: true
				},
				{
					title: 'Kelompok',
					field: 'kelompok',
					width: '45%',
					sortable: true
				},
				{
					title: 'Kelompok Jabatan 1',
					field: 'kelompok1',
					width: '45%',
					sortable: true
				}
			]]
		});
	});
</script>