<div id="window-search-klp-jabatan-fungsional-umum-2"
	title="Pencarian Kelompok Jabatan",
	data-options="width:500,iconCls:'icon-search',inline:true"
	style="display:none">
	<form id="form-search-klp-jabatan-fungsional-umum-2" method="post" class="form-horizontal" role="form" onsubmit="doSearchKlpJabatanFungsionalUmum2(this); return false" onreset="doRefreshKlpJabatanFungsionalUmum2(this)">
		<div class="panel-content">
			<div class="form-group">
				<label for="kode" class="col-xs-4 control-label">Kode</label>
				<div class="col-xs-8">
					<input type="text" name="kode" class="form-control input-sm" />
				</div>
			</div>
			<div class="form-group">
				<label for="kelompok" class="col-xs-4 control-label">Kelompok</label>
				<div class="col-xs-8">
					<input type="text" name="kelompok" class="form-control input-sm" />
				</div>
			</div>
			<div class="form-group">
				<label for="id_klp_1" class="col-xs-4 control-label">Kelompok Jabatan 1</label>
				<div class="col-xs-8">
					<?= dinamyc_dropdown(array(
						'name' => 'id_klp_1',
						'table' => 'v_klp_jabatan_fungsional_umum_1',
						'order_by' => 'kelompok ASC',
						'key' => 'id',
						'label' => ['kode', 'kelompok'],
						'default' => '',
						'empty_first' => TRUE,
						'empty_first_label' => '- Pilih Kelompok Jabatan 1 -',
						'attr' => 'class="form-control input-sm easyui-combobox" style="width:100%"'
					)) ?>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-xs-8 col-xs-offset-4">
					<button type="submit" class="btn btn-sm btn-default btn-round"><i class="fa fa-fw fa-search"></i> Cari</button>	
					<button type="reset" class="btn btn-sm btn-default btn-round"><i class="fa fa-fw fa-refresh"></i> Segarkan</button>	
				</div>
			</div>
		</div>
	</form>
</div>