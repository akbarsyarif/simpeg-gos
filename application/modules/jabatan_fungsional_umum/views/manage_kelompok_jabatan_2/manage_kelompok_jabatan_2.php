<div id="toolbar-table-klp-jabatan-fungsional-umum-2">
	<table cellpadding="0" cellspacing="0" class="tb-toolbar">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td><a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="addKlpJabatanFungsionalUmum2(this)"><i class="fa fa-fw fa-plus"></i> Tambah</a></td>
						<td><a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="editKlpJabatanFungsionalUmum2(this)"><i class="fa fa-fw fa-edit"></i> Edit</a></td>
						<td><a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="removeKlpJabatanFungsionalUmum2(this)"><i class="fa fa-fw fa-trash"></i> Hapus</a></td>
					</tr>
				</table>
			</td>
			<td align="right">
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td><a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="searchKlpJabatanFungsionalUmum2(this)"><i class="fa fa-fw fa-search"></i> Pencarian</a></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
<table id="table-klp-jabatan-fungsional-umum-2"></table>

<?php require 'includes/window_search_klp_jabatan_fungsional_umum_2.php' ?>
<?php require 'includes/window_form_klp_jabatan_fungsional_umum_2.php' ?>

<!-- include javascript -->
<?php require 'includes/app.js.php' ?>
