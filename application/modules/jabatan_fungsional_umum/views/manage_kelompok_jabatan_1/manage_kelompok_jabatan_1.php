<div id="toolbar-table-klp-jabatan-fungsional-umum-1">
	<table cellpadding="0" cellspacing="0" class="tb-toolbar">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td><a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="addKlpJabatanFungsionalUmum1(this)"><i class="fa fa-fw fa-plus"></i> Tambah</a></td>
						<td><a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="editKlpJabatanFungsionalUmum1(this)"><i class="fa fa-fw fa-edit"></i> Edit</a></td>
						<td><a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="removeKlpJabatanFungsionalUmum1(this)"><i class="fa fa-fw fa-trash"></i> Hapus</a></td>
					</tr>
				</table>
			</td>
			<td align="right">
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td><a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="searchKlpJabatanFungsionalUmum1(this)"><i class="fa fa-fw fa-search"></i> Pencarian</a></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
<table id="table-klp-jabatan-fungsional-umum-1"></table>

<?php require 'includes/window_search_klp_jabatan_fungsional_umum_1.php' ?>
<?php require 'includes/window_form_klp_jabatan_fungsional_umum_1.php' ?>

<!-- include javascript -->
<?php require 'includes/app.js.php' ?>
