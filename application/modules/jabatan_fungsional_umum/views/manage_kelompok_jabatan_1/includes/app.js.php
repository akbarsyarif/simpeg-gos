<script type="text/javascript">
	function addKlpJabatanFungsionalUmum1(obj) {
		$('#window-form-klp-jabatan-fungsional-umum-1').dialog({
			iconCls: "icon-add",
			title: "Tambah Jabatan",
			onBeforeOpen: function() {
				// clear form fields
				$('#form-klp-jabatan-fungsional-umum-1').form('reset');
			}
		});
	}

	function editKlpJabatanFungsionalUmum1(obj) {
		var data = $('#table-klp-jabatan-fungsional-umum-1').datagrid('getSelected');
		
		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin diedit', 'warning');
		} else {
			$('#window-form-klp-jabatan-fungsional-umum-1').dialog({
				iconCls: "icon-edit",
				title: "Edit Jabatan",
				onBeforeOpen: function() {
					// set fields value
					$('#form-klp-jabatan-fungsional-umum-1').find('[name="id"]').val(data.id);
					$('#form-klp-jabatan-fungsional-umum-1').find('[name="kode"]').val(data.kode);
					$('#form-klp-jabatan-fungsional-umum-1').find('[name="kelompok"]').val(data.kelompok);
				}
			});
		}
	}

	function doSubmitFormKlpJabatanFungsionalUmum1(obj) {
		let data = $(obj).serialize();
		let id = $(obj).find('[name="id"]').val();
		if ( typeof id !== "undefined" && id !== "" ) {
			var method = "PUT";
		} else {
			var method = "POST";
		}

		$.ajax({
			url: '<?= site_url('api/jabatan_fungsional_umum/manage_kelompok_jabatan_1') ?>',
			method: method,
			dataType: 'json',
			data: data,
			beforeSend: function(xhr) {
				isValid = $(obj).form('validate');
				if ( isValid ) {
					$(obj).find('[type="submit"]').button('loading');
				}
				return isValid;
			},
			success: function(xhr) {
				if ( typeof xhr === "object" ) {
					if ( xhr.metadata.code != 200 ) {
						$.messager.alert('Error', xhr.metadata.message, 'error');
					} else {
						if ( method === "PUT" ) {
							$('#window-form-klp-jabatan-fungsional-umum-1').dialog('close');
						}
						$('#table-klp-jabatan-fungsional-umum-1').datagrid('reload');
						$(obj).form('reset');
					}
				} else {
					$.messager.alert('Error', 'Server error', 'error');
					console.log(xhr);
				}
			},
			error: function(xhr) {
				let error = JSON.parse(xhr.responseText);
				if ( typeof error == "object" ) {
					var message = error.metadata.message;
				} else {
					var message = "Tidak dapat menghubungi server";
				}
				$.messager.alert('Error', message, 'error');
				console.log(error);
			},
			complete: function() {
				$(obj).find('[type="submit"]').button('reset');
			}
		});
	}

	function doResetFormKlpJabatanFungsionalUmum1(obj) {
		$('#window-form-klp-jabatan-fungsional-umum-1').dialog('close');
		$(obj).form('clear');
	}

	function removeKlpJabatanFungsionalUmum1(obj) {
		var data = $('#table-klp-jabatan-fungsional-umum-1').datagrid('getSelected');

		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin dihapus', 'warning');
		} else {
			$.messager.confirm('Hapus Data', 'Anda yakin ingin menghapus data kelompok jabatan <b>'+ data.kelompok +'</b>?', function(r) {
				if ( r ) {
					$('#table-klp-jabatan-fungsional-umum-1').datagrid('loading');

					$.ajax({
						url: '<?= site_url('api/jabatan_fungsional_umum/manage_kelompok_jabatan_1') ?>',
						method: 'delete',
						data: {
							id: data.id
						},
						dataType: 'json',
						success: function(xhr) {
							if ( typeof xhr === "object" ) {
								if ( xhr.metadata.code != 200 ) {
									$.messager.alert('Error', xhr.metadata.message, 'error');
								} else {
									$('#table-klp-jabatan-fungsional-umum-1').datagrid('reload');
								}
							} else {
								$.messager.alert('Error', 'Server error', 'error');
								console.log(xhr);
							}
						},
						error: function(xhr) {
							let error = JSON.parse(xhr.responseText);
							if ( typeof error == "object" ) {
								var message = error.metadata.message;
							} else {
								var message = "Tidak dapat menghubungi server";
							}
							$.messager.alert('Error', message, 'error');
							console.log(error);
						},
						complete: function() {
							$('#table-klp-jabatan-fungsional-umum-1').datagrid('loaded');
						}
					})
				}
			});
		}
	}

	function searchKlpJabatanFungsionalUmum1(obj) {
		$('#window-search-klp-jabatan-fungsional-umum-1').dialog();
	}

	function doSearchKlpJabatanFungsionalUmum1(obj) {
		var kode = $(obj).find('[name="kode"]').val();
		var kelompok = $(obj).find('[name="kelompok"]').val();

		$('#table-klp-jabatan-fungsional-umum-1').datagrid('load', {
			kode: kode,
			kelompok: kelompok
		});

		$('#window-search-klp-jabatan-fungsional-umum-1').dialog('close');
	}

	function doRefreshKlpJabatanFungsionalUmum1(obj) {
		$('#table-klp-jabatan-fungsional-umum-1').datagrid('load', {});
		$('#window-search-klp-jabatan-fungsional-umum-1').dialog('close');
		$(obj).form('clear');
	}

	$(document).ready(function() {
		$('#table-klp-jabatan-fungsional-umum-1').datagrid({
			title: '<i class="fa fa-fw fa-list"></i> Jabatan',
			url: '<?= site_url('api/jabatan_fungsional_umum/manage_kelompok_jabatan_1/datagrid') ?>',
			method: 'get',
			pagination: true,
			fit: true,
			rownumbers: true,
			fitColumns: true,
			singleSelect: true,
			toolbar: '#toolbar-table-klp-jabatan-fungsional-umum-1',
			pageSize: 20,
			striped: true,
			emptyMsg: 'Tidak ada data ditemukan',
			multiSort: true,
			columns: [[
				{
					title: 'Kode',
					field: 'kode',
					width: '10%',
					align: 'center',
					sortable: true
				},
				{
					title: 'Kelompok',
					field: 'kelompok',
					width: '90%',
					sortable: true
				}
			]]
		});
	});
</script>