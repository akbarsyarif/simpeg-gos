<div id="window-form-klp-jabatan-fungsional-umum-1"
	style="display:none"
	data-options="width:500,inline:true">
	<form id="form-klp-jabatan-fungsional-umum-1" class="form-horizontal" role="form" onsubmit="doSubmitFormKlpJabatanFungsionalUmum1(this); return false" onreset="doResetFormKlpJabatanFungsionalUmum1(this)">
		<input type="hidden" name="id" />
		<div class="panel-content">
			<div class="form-group">
				<label for="kode" class="col-xs-3 control-label">Kode</label>
				<div class="col-xs-9">
					<input type="text" name="kode" class="form-control input-sm easyui-validatebox" data-options="required:true" />
				</div>
			</div>
			<div class="form-group">
				<label for="kelompok" class="col-xs-3 control-label">Kelompok</label>
				<div class="col-xs-9">
					<input type="text" name="kelompok" class="form-control input-sm easyui-validatebox" data-options="required:true" />
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-xs-9 col-xs-offset-3">
					<button type="submit" class="btn btn-sm btn-success btn-round" data-loading-text="<i class='fa fa-fw fa-circle-o-notch fa-spin'></i> loading..."><i class="fa fa-fw fa-check-circle"></i> Simpan</button> 
					<button type="reset" class="btn btn-sm btn-default btn-round"><i class="fa fa-fw fa-times-circle"></i> Batal</button> 
				</div>
			</div>
		</div>
	</form>
</div>