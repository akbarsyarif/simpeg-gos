<div id="window-search-klp-jabatan-fungsional-umum-1"
	title="Pencarian Kelompok Jabatan",
	data-options="width:500,iconCls:'icon-search',inline:true"
	style="display:none">
	<form id="form-search-klp-jabatan-fungsional-umum-1" method="post" class="form-horizontal" role="form" onsubmit="doSearchKlpJabatanFungsionalUmum1(this); return false" onreset="doRefreshKlpJabatanFungsionalUmum1(this)">
		<div class="panel-content">
			<div class="form-group">
				<label class="control-label col-xs-3">Kode</label>
				<div class="col-xs-9">
					<input name="kode" type="text" class="form-control input-sm" />
				</div>
			</div>
			<div class="form-group">
				<label for="kelompok" class="col-xs-3 control-label">Kelompok</label>
				<div class="col-xs-9">
					<input name="kelompok" type="text" class="form-control input-sm" />
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-xs-9 col-xs-offset-3">
					<button type="submit" class="btn btn-sm btn-default btn-round"><i class="fa fa-fw fa-search"></i> Cari</button>	
					<button type="reset" class="btn btn-sm btn-default btn-round"><i class="fa fa-fw fa-refresh"></i> Segarkan</button>	
				</div>
			</div>
		</div>
	</form>
</div>