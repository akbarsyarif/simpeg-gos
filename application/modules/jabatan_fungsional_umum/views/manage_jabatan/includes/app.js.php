<script type="text/javascript">
	function addJabatanFungsionalUmum(obj) {
		$('#window-form-jabatan-fungsional-umum').dialog({
			iconCls: "icon-add",
			title: "Tambah Jabatan",
			onBeforeOpen: function() {
				// clear form fields
				$('#form-jabatan-fungsional-umum').form('reset');
			}
		});
	}

	function editJabatanFungsionalUmum(obj) {
		var data = $('#table-jabatan-fungsional-umum').datagrid('getSelected');
		
		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin diedit', 'warning');
		} else {
			$('#window-form-jabatan-fungsional-umum').dialog({
				iconCls: "icon-edit",
				title: "Edit Jabatan",
				onBeforeOpen: function() {
					// set fields value
					$('#form-jabatan-fungsional-umum').find('[name="id"]').val(data.id);
					$('#form-jabatan-fungsional-umum').find('[name="id_klp_1"]').val(data.id_klp_1).trigger('change');

					// kode jabatan 2
					let dropdown = ajaxDropdown({
						target: '#manage-jabatan-fungsional-umum-klp-2',
						url: '<?= site_url("api/jabatan_fungsional_umum/manage_kelompok_jabatan_2") ?>',
						data: { id_klp_1: data.id_klp_1 },
						firstLabel: '- Pilih Kelompok Usulan -',
						selected: data.id_klp_2,
						processResult: {
							value: 'id',
							label: ['kode', 'kelompok']
						},
						complete: function() {
							$('#manage-jabatan-fungsional-umum-klp-2').trigger('change');
						}
					});

					$('#form-jabatan-fungsional-umum').find('[name="kode"]').val(data.kode);
					$('#form-jabatan-fungsional-umum').find('[name="jabatan"]').val(data.jabatan);
				}
			});
		}
	}

	function doSubmitFormJabatanFungsionalUmum(obj) {
		let data = $(obj).serialize();
		let id = $(obj).find('[name="id"]').val();
		if ( typeof id !== "undefined" && id !== "" ) {
			var method = "PUT";
		} else {
			var method = "POST";
		}

		$.ajax({
			url: '<?= site_url('api/jabatan_fungsional_umum/manage_jabatan') ?>',
			method: method,
			dataType: 'json',
			data: data,
			beforeSend: function(xhr) {
				isValid = $(obj).form('validate');
				if ( isValid ) {
					$(obj).find('[type="submit"]').button('loading');
				}
				return isValid;
			},
			success: function(xhr) {
				if ( typeof xhr === "object" ) {
					if ( xhr.metadata.code != 200 ) {
						$.messager.alert('Error', xhr.metadata.message, 'error');
					} else {
						if ( method === "PUT" ) {
							$('#window-form-jabatan-fungsional-umum').dialog('close');
						}
						$('#table-jabatan-fungsional-umum').datagrid('reload');
						$(obj).form('reset');
					}
				} else {
					$.messager.alert('Error', 'Server error', 'error');
					console.log(xhr);
				}
			},
			error: function(xhr) {
				let error = JSON.parse(xhr.responseText);
				if ( typeof error == "object" ) {
					var message = error.metadata.message;
				} else {
					var message = "Tidak dapat menghubungi server";
				}
				$.messager.alert('Error', message, 'error');
				console.log(error);
			},
			complete: function() {
				$(obj).find('[type="submit"]').button('reset');
			}
		});
	}

	function doResetFormJabatanFungsionalUmum(obj) {
		$('#window-form-jabatan-fungsional-umum').dialog('close');
		$(obj).form('clear');
	}

	function removeJabatanFungsionalUmum(obj) {
		var data = $('#table-jabatan-fungsional-umum').datagrid('getSelected');

		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin dihapus', 'warning');
		} else {
			$.messager.confirm('Hapus Data', 'Anda yakin ingin menghapus data jabatan <b>'+ data.kode + ' - ' + data.jabatan +'</b>?', function(r) {
				if ( r ) {
					$('#table-jabatan-fungsional-umum').datagrid('loading');

					$.ajax({
						url: '<?= site_url('api/jabatan_fungsional_umum/manage_jabatan') ?>',
						method: 'delete',
						data: {
							id: data.id
						},
						dataType: 'json',
						success: function(xhr) {
							if ( typeof xhr === "object" ) {
								if ( xhr.metadata.code != 200 ) {
									$.messager.alert('Error', xhr.metadata.message, 'error');
								} else {
									$('#table-jabatan-fungsional-umum').datagrid('reload');
								}
							} else {
								$.messager.alert('Error', 'Server error', 'error');
								console.log(xhr);
							}
						},
						error: function(xhr) {
							let error = JSON.parse(xhr.responseText);
							if ( typeof error == "object" ) {
								var message = error.metadata.message;
							} else {
								var message = "Tidak dapat menghubungi server";
							}
							$.messager.alert('Error', message, 'error');
							console.log(error);
						},
						complete: function() {
							$('#table-jabatan-fungsional-umum').datagrid('loaded');
						}
					})
				}
			});
		}
	}

	function searchJabatanFungsionalUmum(obj) {
		$('#window-search-jabatan-fungsional-umum').dialog();
	}

	function doSearchJabatanFungsionalUmum(obj) {
		var jabatan = $(obj).find('[name="jabatan"]').val();
		var id_eselon = $(obj).find('[name="id_eselon"]').val();
		var id_skpd = $(obj).find('[name="id_skpd"]').val();

		$('#table-jabatan-fungsional-umum').datagrid('load', {
			jabatan: jabatan,
			id_eselon: id_eselon,
			id_skpd: id_skpd,
		});

		$('#window-search-jabatan-fungsional-umum').dialog('close');
	}

	function doRefreshJabatanFungsionalUmum(obj) {
		$('#table-jabatan-fungsional-umum').datagrid('load', {});
		$('#window-search-jabatan-fungsional-umum').dialog('close');
		$(obj).form('clear');
	}

	function loadKlpJabatanFungsionalUmum2() {
		let id_klp_1 = $('#manage-jabatan-fungsional-umum-klp-1').combobox('getValue');
		if ( typeof id_klp_1 == "undefined" || id_klp_1 == "" ) {
			$('#manage-jabatan-fungsional-umum-klp-2').combobox('loadData', []);
			$('#manage-jabatan-fungsional-umum-klp-2').combobox('setValue', '');
		} else {
			$('#manage-jabatan-fungsional-umum-klp-2').combobox('reload', {
				id_klp_1: id_klp_1
			});
		}
	}

	$(document).ready(function() {
		$('#table-jabatan-fungsional-umum').datagrid({
			title: '<i class="fa fa-fw fa-list"></i> Jabatan',
			url: '<?= site_url('api/jabatan_fungsional_umum/manage_jabatan/datagrid') ?>',
			method: 'get',
			pagination: true,
			rownumbers: true,
			fitColumns: true,
			singleSelect: true,
			toolbar: '#toolbar-table-jabatan-fungsional-umum',
			pageSize: 20,
			fit: true,
			emptyMsg: 'Tidak ada data ditemukan',
			multiSort: true,
			columns: [[
				{
					title: 'Kelompok Jabatan 1',
					field: 'kelompok1',
					width: 150,
				},
				{
					title: 'Kelompok Jabatan 2',
					field: 'kelompok2',
					width: 150,
				},
				{
					title: 'Kode',
					field: 'kode',
					width: 60,
					align: 'center'
				},
				{
					title: 'Jabatan',
					field: 'jabatan',
					width: 150,
				}
			]],
			onLoadSuccess: function(data) {
				$(this).datagrid("mergeEqualCells", ["kelompok1", "kelompok2"]);
			}
		});

		$('#main-tabs').on('change', '#manage-jabatan-fungsional-umum-klp-1', function() {
			let id_klp_1 = $(this).val();
			let dropdown = ajaxDropdown({
				target: '#manage-jabatan-fungsional-umum-klp-2',
				url: '<?= site_url("api/jabatan_fungsional_umum/manage_kelompok_jabatan_2") ?>',
				data: { id_klp_1: id_klp_1 },
				firstLabel: '- Pilih Kelompok Usulan -',
				processResult: {
					value: 'id',
					label: ['kode', 'kelompok']
				},
				complete: function() {
					$('#manage-jabatan-fungsional-umum-klp-2').trigger('change');
				}
			});
		});
	});
</script>