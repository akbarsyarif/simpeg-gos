<div id="window-search-jabatan-fungsional-umum"
	title="Pencarian Jabatan",
	data-options="width:500,iconCls:'icon-search',inline:true"
	style="display:none">
	<form id="form-search-jabatan-fungsional-umum" method="post" class="form-horizontal" role="form" onsubmit="doSearchJabatanFungsionalUmum(this); return false" onreset="doRefreshJabatanFungsionalUmum(this)">
		<div class="panel-content">
			<div class="form-group">
				<label for="id_klp_1" class="col-xs-3 control-label">Kelompok 1</label>
				<div class="col-xs-9">
					<?= dinamyc_dropdown(array(
						'name' => 'id_klp_1',
						'table' => 'v_klp_jabatan_fungsional_umum_1',
						'key' => 'id',
						'label' => array('kode', 'kelompok'),
						'default' => '',
						'empty_first' => TRUE,
						'empty_first_label' => '- Pilih Kelompok Usulan -',
						'attr' => 'id="search-jabatan-fungsional-umum-klp-1" class="form-control input-sm" style="width:100%"'
					)) ?>
				</div>
			</div>
			<div class="form-group">
				<label for="id_klp_2" class="col-xs-3 control-label">Kelompok 2</label>
				<div class="col-xs-9">
					<?= form_dropdown(array(
						'name' => 'id_klp_2',
						'options' => array(
							'' => '- Pilih Kelompok Usulan -'
						),
						'selected' => '',
						'class' => 'form-control input-sm',
						'id' => 'search-jabatan-fungsional-umum-klp-2',
						'style' => 'width:100%'
					)) ?>
				</div>
			</div>
			<div class="form-group">
				<label for="kode" class="col-xs-3 control-label">Kode</label>
				<div class="col-xs-9">
					<input type="text" name="kode" class="form-control input-sm" />
				</div>
			</div>
			<div class="form-group">
				<label for="jabatan" class="col-xs-3 control-label">Jabatan</label>
				<div class="col-xs-9">
					<input type="text" name="jabatan" class="form-control input-sm" />
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-xs-9 col-xs-offset-3">
					<button type="submit" class="btn btn-sm btn-default btn-round"><i class="fa fa-fw fa-search"></i> Cari</button>	
					<button type="reset" class="btn btn-sm btn-default btn-round"><i class="fa fa-fw fa-refresh"></i> Segarkan</button>	
				</div>
			</div>
		</div>
	</form>
</div>