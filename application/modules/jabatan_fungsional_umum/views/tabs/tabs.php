<div id="tab-jabatan-fungsional-umum" class="easyui-tabs" fit="true" data-options="border:false">
	<div title="Kelompok Jabatan 1" style="padding:5px" data-options="
		closeable:true,
		href:'<?= site_url('jabatan_fungsional_umum/manage_kelompok_jabatan_1') ?>'">
	</div>
	<div title="Kelompok Jabatan 2" style="padding:5px" data-options="
		closeable:true,
		href:'<?= site_url('jabatan_fungsional_umum/manage_kelompok_jabatan_2') ?>'">
	</div>
	<div title="Jabatan" style="padding:5px" data-options="
		closeable:true,
		href:'<?= site_url('jabatan_fungsional_umum/manage_jabatan') ?>'">>
	</div>
</div>

<!-- include javascript -->
<?php require 'includes/app.js.php' ?>