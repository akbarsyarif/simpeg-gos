<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('profile/profile', $this->data);
	}

}

/* End of file Profile.php */
/* Location: ./application/modules/users/controllers/Profile.php */