<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

	public $data = [];

	public function __construct()
	{
		parent::__construct();

		$this->template->set_theme($this->config->item('admin_theme'));
		$this->template->set_layout('login');

		$this->data['app_title'] = $this->config->item('app_title');
		$this->data['app_description'] = $this->config->item('app_description');
	}

	public function login()
	{
		if ( $this->ion_auth->logged_in() ) {
			redirect('welcome');
		}
		else {
			$this->template->build('auth/login', $this->data);
		}
	}

	public function logout()
	{
		$this->ion_auth->logout();
		redirect('login');
	}

}

/* End of file Auth.php */
/* Location: ./application/modules/users/controllers/Auth.php */