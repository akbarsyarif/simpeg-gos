<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_user extends SP_Rest_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->config->load('ion_auth');
		$this->load->library('content_dir');
		$this->load->model('m_user');
		$this->load->model('ion_auth_model');
	}

	/**
	 * response for jeasyui datagrid
	 */
	public function datagrid_get()
	{
		$query = [];

		// search params
		if ( isset($_GET['nama_lengkap']) && !empty($_GET['nama_lengkap']) ) $query['where']['name LIKE'] = '%' . $this->get('nama_lengkap', TRUE) . '%';
		if ( isset($_GET['email']) && !empty($_GET['email']) ) $query['where']['email LIKE'] = '%' . $this->get('email', TRUE) . '%';
		if ( isset($_GET['group_id']) && !empty($_GET['group_id']) ) $query['where']['group_id'] = $this->get('group_id', TRUE);
		// created_on range
		$created_on_begin = ( isset($_GET['created_on_begin']) && !empty($_GET['created_on_begin']) ) ? $this->get('created_on_begin', TRUE) : FALSE;
		$created_on_end = ( isset($_GET['created_on_end']) && !empty($_GET['created_on_end']) ) ? $this->get('created_on_end', TRUE) : date('Y-m-d');
		if ( $created_on_begin !== FALSE ) $query['where_no_escape']['created_on BETWEEN'] = "'{$created_on_begin}' AND '{$created_on_end}'";
		// last_login range
		$last_login_begin = ( isset($_GET['last_login_begin']) && !empty($_GET['last_login_begin']) ) ? $this->get('last_login_begin', TRUE) : FALSE;
		$last_login_end = ( isset($_GET['last_login_end']) && !empty($_GET['last_login_end']) ) ? $this->get('last_login_end', TRUE) : date('Y-m-d');
		if ( $last_login_begin !== FALSE ) $query['where_no_escape']['last_login BETWEEN'] = "'{$last_login_begin}' AND '{$last_login_end}'";

		$count = $this->m_user->count($query);

		// sorting
		$sort = $this->get('sort', TRUE);
		$order = $this->get('order', TRUE);
		$split_sort = explode(',', $sort);
		$split_order = explode(',', $order);

		if ( isset($_GET['sort']) && isset($_GET['order']) ) {
			if ( is_array($split_sort) && count($split_sort) > 0 ) {
				foreach ($split_sort as $index => $field) {
					$orders[$field] = $split_order[$index];
				}
				$query['order_by'] = $orders;
			} else {
				$query['order_by'] = "{$sort} {$order}";
			}
		}

		// set limit and offset
		$offset = isset($_GET['page']) ? $this->get('page', TRUE) : 1;
		$limit  = isset($_GET['rows']) ? $this->get('rows', TRUE) : 10;
		$offset = ($offset - 1) * $limit;
		$query['limit_offset'] = [$limit, $offset];

		$data = $this->m_user->get($query);

		$this->response([
			'total' => $count,
			'rows' => $data['data']
		]);
	}

	/**
	 * get user data
	 */
	public function index_get()
	{
		$uid = $this->get('id', TRUE);

		if ( $uid != NULL ) {
			$result = $this->m_user->row([
				'where' => [
					'id' => $uid
				]
			]);

			if ($result == FALSE) {
				$this->response([
					'metadata' => [
						'code' => "400",
						'message' => "User tidak ditemukan"
					]
				], 400);
			} else {
				$this->response([
					'metadata' => [
						'code' => "200",
						'message' => "OK"
					],
					'response' => $result
				]);
			}
		} else {
			$query = [];

			$count = $this->m_user->count($query);

			// set limit and offset
			$offset = isset($_GET['page']) ? $this->get('page', TRUE) : 1;
			$limit  = isset($_GET['rows']) ? $this->get('rows', TRUE) : 10;
			$offset = ($offset - 1) * $limit;
			$query['limit_offset'] = [$limit, $offset];

			// sorting
			$sort = $this->get('sort', TRUE);
			$order = $this->get('order', TRUE);
			if ( isset($_GET['sort']) && isset($_GET['order']) ) $query['order_by'] = "{$sort} {$order}";

			$result = $this->m_user->get($query);

			if ($result == FALSE) {
				$this->response([
					'metadata' => [
						'code' => "400",
						'message' => "User tidak ditemukan"
					]
				], 400);
			} else {
				$this->response([
					'metadata' => [
						'code' => "200",
						'message' => "OK"
					],
					'response' => [
						'total' => $count,
						'rows' => $result['data']
					]
				]);
			}
		}
	}

	/**
	 * register new user
	 */
	public function index_post()
	{
		$rules = [
			[
				'field' => 'name',
				'label' => 'Nama Lengkap',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'email',
				'label' => 'Email',
				'rules' => 'required|trim|valid_email|is_unique[users.email]|xss_clean'
			],
			[
				'field' => 'username',
				'label' => 'Username',
				'rules' => 'required|trim|is_unique[users.username]|xss_clean'
			],
			[
				'field' => 'password',
				'label' => 'Kata Sandi',
				'rules' => 'required|trim|min_length['. $this->config->item('min_password_length', 'ion_auth') .']|max_length['. $this->config->item('max_password_length', 'ion_auth') .']|xss_clean'
			],
			[
				'field' => 'repeat_password',
				'label' => 'Ulangi Kata Sandi',
				'rules' => 'required|trim|matches[password]|xss_clean'
			],
			[
				'field' => 'group_id',
				'label' => 'Grup',
				'ruels' => 'required|trim|xss_clean'
			]
		];

		$group_id = $this->input->post('group_id', TRUE);

		if ( $group_id == 2 ) {
			$rules[] = [
				'field' => 'nip',
				'label' => 'Nomor Induk Pegawai',
				'rules' => 'required|trim|is_unique[users.nip]|xss_clean'
			];
		}

		$this->form_validation->set_rules($rules);

		if ( $this->form_validation->run() == FALSE ) {
			$this->response([
				'metadata' => [
					'code' => "400",
					'message' => strip_tags(validation_errors())
				]
			], 400);
		} else {
			$email    = $this->input->post('email', TRUE);
			$username = $this->input->post('username', TRUE);
			$password = $this->input->post('password', TRUE);
			
			// groups
			$groups = [$group_id];

			// additional data
			$name = $this->input->post('name', TRUE);
			$nip = ($group_id == 2) ? $this->input->post('nip', TRUE) : NULL;
			$active = $this->input->post('active', TRUE);

			$additional_data = [
				'active' => $active,
				'name' => $name,
				'nip' => $nip
			];

			if ( $this->ion_auth->register($username, $password, $email, $additional_data, $groups) ) {
				$this->content_dir->createUserDir($username);

				$this->response([
					'metadata' => [
						'code' => "200",
						'message' => $this->ion_auth->messages()
					]
				]);
			} else {
				$this->response([
					'metadata' => [
						'code' => "500",
						'message' => $this->ion_auth->errors()
					]
				], 500);
			}
		}
	}

	/**
	 * update user information
	 */
	public function index_put()
	{
		$this->form_validation->set_data($this->put());

		$id = $this->put('id', TRUE);

		$rules = [
			[
				'field' => 'name',
				'label' => 'Nama Lengkap',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'email',
				'label' => 'Email',
				'rules' => 'required|trim|valid_email|xss_clean|conflict_user_email['.$id.']'
			],
			[
				'field' => 'group_id',
				'label' => 'Grup',
				'ruels' => 'required|trim|xss_clean'
			]
		];

		$group_id = $this->put('group_id', TRUE);

		if ( $group_id == 2 ) {
			$rules[] = [
				'field' => 'nip',
				'label' => 'Nomor Induk Pegawai',
				'rules' => 'required|trim|conflict_user_nip['.$id.']|xss_clean'
			];
		}

		$this->form_validation->set_rules($rules);

		if ( $this->form_validation->run() == FALSE ) {
			$this->response([
				'metadata' => [
					'code' => "400",
					'message' => strip_tags(validation_errors())
				]
			], 400);
		} else {
			// groups
			$groups = [$group_id];

			// user data
			$id = $this->put('id', TRUE);
			$name = $this->put('name', TRUE);
			$email = $this->put('email', TRUE);
			$nip = ($group_id == 2) ? $this->put('nip', TRUE) : NULL;

			$data = [
				'name' => $name,
				'email' => $email,
				'nip' => $nip
			];

			if ( $this->ion_auth->update($id, $data) ) {
				$this->response([
					'metadata' => [
						'code' => "200",
						'message' => $this->ion_auth->messages()
					]
				]);
			} else {
				$this->response([
					'metadata' => [
						'code' => "500",
						'message' => $this->ion_auth->errors()
					]
				], 500);
			}
		}
	}

	/**
	 * remove user
	 */
	public function index_delete()
	{
		$id = $this->delete('id');

		$result = $this->m_user->where('id', $id)->delete();

		if ( $result == FALSE ) {
			$this->response([
				'metadata' => [
					'code' => "500",
					'message' => "Server error, gagal menghapus data."
				]
			], 500);
		} else {
			$this->response([
				'metadata' => [
					'code' => "200",
					'message' => "OK"
				]
			]);
		}
	}

	/**
	 * login as specific user
	 * without password
	 */
	public function loginAs_get()
	{
		$identify = $this->get('identify');
		if ( $this->ion_auth_model->impersonate($identify) ) {
			$this->response([
				'metadata' => [
					'code' => "200",
					'message' => "OK"
				]
			]);
		} else {
			$this->response([
				'metadata' => [
					'code' => "400",
					'message' => "User tidak ditemukan"
				]
			], 400);
		}
	}

	public function resetPassword_put()
	{
		$id = $this->put('id');
		if ( $this->ion_auth->user($id) !== FALSE ) {
			$new_password = $this->m_user->generateNewPassword();
			$data = ['password' => $new_password];
			if ( $this->ion_auth->update($id, $data) ) {
				$this->response([
					'metadata' => [
						'code' => "200",
						'message' => "OK"
					],
					'response' => $new_password
				]);
			} else {
				$this->response([
					'metadata' => [
						'code' => "400",
						'message' => strip_tags($this->ion_auth->errors())
					]
				], 400);
			}
		} else {
			$this->response([
				'metadata' => [
					'code' => "400",
					'message' => strip_tags($this->ion_auth->errors())
				]
			], 400);
		}
	}

}

/* End of file Manage_user.php */
/* Location: ./application/modules/users/controllers/rest/Manage_user.php */