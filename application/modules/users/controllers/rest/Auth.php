<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends SP_Rest_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('ion_auth_model');
	}

	/**
	 * authenticate user
	 */
	public function auth_post()
	{
		$rules = array(
			array(
				'field' => 'username',
				'label' => 'Username',
				'rules' => 'required|xss_clean'
			),
			array(
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'required|xss_clean'
			)
		);
		$this->form_validation->set_rules($rules);

		if ( $this->form_validation->run($rules) == FALSE ) {
			$this->response([
				'metadata' => [
					'code' => "400",
					'message' => strip_tags(validation_errors())
				]
			], 400);
		}
		else {
			$username = $this->input->post('username', TRUE);
			$password = $this->input->post('password', TRUE);
			$remember = $this->input->post('remember', TRUE);

			if ( $this->ion_auth->login($username, $password, $remember) == FALSE ) {
				$this->response([
					'metadata' => [
						'code' => "400",
						'message' => strip_tags($this->ion_auth->errors())
					]
				], 401);
			}
			else {
				$this->response([
					'metadata' => [
						'code' => "200",
						'message' => strip_tags($this->ion_auth->messages())
					]
				]);
			}
		}
	}

}

/* End of file Auth.php */
/* Location: ./application/modules/users/controllers/rest/Auth.php */