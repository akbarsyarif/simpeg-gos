<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_user extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('manage_user/manage_user', $this->data);
	}

}

/* End of file Manage_user.php */
/* Location: ./application/modules/users/controllers/Manage_user.php */