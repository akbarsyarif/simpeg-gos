<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_User extends MY_Model {

	public $table = 'users';
	public $view = 'v_user';
	public $pk = 'id';

	public function __construct()
	{
		parent::__construct();
	}

	public function generateNewPassword()
	{
		return random_string('alnum', $this->config->item('min_password_length', 'ion_auth'));
	}

}

/* End of file M_User.php */
/* Location: ./application/models/M_User.php */