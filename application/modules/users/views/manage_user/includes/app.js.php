<script type="text/javascript">
	function addUser(obj) {
		$('#window-form-user').dialog({
			iconCls: "icon-add",
			title: "Tambah Pengguna",
			onBeforeOpen: function() {
				// clear form fields
				$('#form-user').form('reset');

				// enabled validation for username, password and repeat_password
				$('#form-user').find('[name="username"]').removeAttr('disabled', '').validatebox('enableValidation');
				$('#form-user').find('[name="password"]').removeAttr('disabled', '').validatebox('enableValidation');
				$('#form-user').find('[name="repeat_password"]').removeAttr('disabled', '').validatebox('enableValidation');
			}
		});
	}

	function editUser(obj) {
		var data = $('#table-users').datagrid('getSelected');
		
		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin diedit', 'warning');
		} else {
			$('#window-form-user').dialog({
				iconCls: "icon-edit",
				title: "Edit Pengguna",
				onBeforeOpen: function() {
					// set fields value
					$('#form-user').find('[name="id"]').val(data.id);
					$('#form-user').find('[name="name"]').val(data.name);
					$('#form-user').find('[name="email"]').val(data.email);
					$('#form-user').find('[name="username"]').val(null);
					$('#form-user').find('[name="password"]').val(null);
					$('#form-user').find('[name="repeat_password"]').val(null);
					$('#form-user').find('[name="group_id"]').val(data.group_id);
					$('#form-user').find('[name="nip"]').val(data.nip);

					// disabled validation for username, password and repeat_password
					$('#form-user').find('[name="username"]').attr('disabled', '').validatebox('disableValidation');
					$('#form-user').find('[name="password"]').attr('disabled', '').validatebox('disableValidation');
					$('#form-user').find('[name="repeat_password"]').attr('disabled', '').validatebox('disableValidation');
				}
			});
		}
	}

	function doSubmitUserForm(obj) {
		let data = $(obj).serialize();
		let id = $(obj).find('[name="id"]').val();
		if ( typeof id !== "undefined" && id !== "" ) {
			var method = "PUT";
		} else {
			var method = "POST";
		}

		$.ajax({
			url: '<?= site_url('api/users/manage_user') ?>',
			method: method,
			dataType: 'json',
			data: data,
			beforeSend: function(xhr) {
				isValid = $(obj).form('validate');
				if ( isValid ) {
					$(obj).find('[type="submit"]').button('loading');
				}
				return isValid;
			},
			success: function(xhr) {
				if ( typeof xhr === "object" ) {
					if ( xhr.metadata.code != 200 ) {
						$.messager.alert('Error', xhr.metadata.message, 'error');
					} else {
						if ( method === "PUT" ) {
							$('#window-form-user').dialog('close');
						}
						$('#table-users').datagrid('reload');
						$(obj).form('reset');
					}
				} else {
					$.messager.alert('Error', 'Server error', 'error');
					console.log(xhr);
				}
			},
			error: function(xhr) {
				let error = JSON.parse(xhr.responseText);
				if ( typeof error == "object" ) {
					var message = error.metadata.message;
				} else {
					var message = "Tidak dapat menghubungi server";
				}
				$.messager.alert('Error', message, 'error');
				console.log(error);
			},
			complete: function() {
				$(obj).find('[type="submit"]').button('reset');
			}
		});
	}

	function doResetUserForm(obj) {
		$('#window-form-user').dialog('close');
		$('#form-user').form('clear');
	}

	function removeUser(obj) {
		var data = $('#table-users').datagrid('getSelected');

		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin dihapus', 'warning');
		} else {
			$.messager.confirm('Hapus Data', 'Anda yakin ingin menghapus data pengguna <b>'+ data.name +'</b>?', function(r) {
				if ( r ) {
					$('#table-users').datagrid('loading');

					$.ajax({
						url: '<?= site_url('api/users/manage_user') ?>',
						method: 'delete',
						data: {
							id: data.id
						},
						dataType: 'json',
						success: function(xhr) {
							if ( typeof xhr === "object" ) {
								if ( xhr.metadata.code != 200 ) {
									$.messager.alert('Error', xhr.metadata.message, 'error');
								} else {
									$('#table-users').datagrid('reload');
								}
							} else {
								$.messager.alert('Error', 'Server error', 'error');
								console.log(xhr);
							}
						},
						error: function(xhr) {
							let error = JSON.parse(xhr.responseText);
							if ( typeof error == "object" ) {
								var message = error.metadata.message;
							} else {
								var message = "Tidak dapat menghubungi server";
							}
							$.messager.alert('Error', message, 'error');
							console.log(error);
						},
						complete: function() {
							$('#table-users').datagrid('loaded');
						}
					})
				}
			});
		}
	}

	function doLoginAs(obj) {
		let data = $('#table-users').datagrid('getSelected');

		if ( data == null ) {
			$.messager.alert('Warning', 'Anda belum memilih pengguna', 'warning');
		} else {
			$.messager.confirm('Login Sebagai', 'Anda yakin akan keluar dan login sebagai <b>'+data.name+'</b>?', function(r) {
				if ( r ) {
					$.ajax({
						url: '<?= site_url('api/users/manage_user/loginas') ?>',
						method: 'get',
						data: {
							identify: data.username
						},
						dataType: 'json',
						success: function(xhr) {
							if ( typeof xhr === "object" ) {
								if ( xhr.metadata.code != 200 ) {
									$.messager.alert('Error', xhr.metadata.message, 'error');
								} else {
									window.location.replace('<?= site_url('web') ?>');
								}
							} else {
								$.messager.alert('Error', 'Server error', 'error');
								console.log(xhr);
							}
						},
						error: function(xhr) {
							let error = JSON.parse(xhr.responseText);
							if ( typeof error == "object" ) {
								var message = error.metadata.message;
							} else {
								var message = "Tidak dapat menghubungi server";
							}
							$.messager.alert('Error', message, 'error');
							console.log(error);
						}
					});
				}
			});
		}
	}

	function doResetPassword(obj) {
		let data = $('#table-users').datagrid('getSelected');

		if ( data == null ) {
			$.messager.alert('Warning', 'Anda belum memilih pengguna', 'warning');
		} else {
			$.messager.confirm('Reset Password', 'Anda yakin ingin membuat password baru untuk <b>'+data.name+'</b>?', function(r) {
				if ( r ) {
					$.ajax({
						url: '<?= site_url('api/users/manage_user/resetpassword') ?>',
						method: 'PUT',
						data: {
							id: data.id
						},
						dataType: 'json',
						success: function(xhr) {
							if ( typeof xhr === "object" ) {
								if ( xhr.metadata.code != 200 ) {
									$.messager.alert('Error', xhr.metadata.message, 'error');
								} else {
									let new_password = xhr.response;
									$.messager.alert('Reset Password', '<div class="text-center">Password baru anda adalah <h4 class="text-danger">'+ new_password +'</h4></div>');
								}
							} else {
								$.messager.alert('Error', 'Server error', 'error');
								console.log(xhr);
							}
						},
						error: function(xhr) {
							let error = JSON.parse(xhr.responseText);
							if ( typeof error == "object" ) {
								var message = error.metadata.message;
							} else {
								var message = "Tidak dapat menghubungi server";
							}
							$.messager.alert('Error', message, 'error');
							console.log(error);
						}
					});
				}
			});
		}
	}

	function searchUser(obj) {
		$('#window-search-user').dialog();
	}

	function doSearchUser(obj) {
		var nama_lengkap = $(obj).find('[name="nama_lengkap"]').val();
		var email = $(obj).find('[name="email"]').val();
		var group_id = $(obj).find('[name="group_id"]').val();
		var created_on_begin = $(obj).find('[name="created_on_begin"]').val();
		var created_on_end = $(obj).find('[name="created_on_end"]').val();
		var last_login_begin = $(obj).find('[name="last_login_begin"]').val();
		var last_login_end = $(obj).find('[name="last_login_end"]').val();

		$('#table-users').datagrid('load', {
			nama_lengkap: nama_lengkap,
			email: email,
			group_id: group_id,
			created_on_begin: created_on_begin,
			created_on_end: created_on_end,
			last_login_begin: last_login_begin,
			last_login_end: last_login_end
		});

		$('#window-search-user').dialog('close');
	}

	function doRefreshUser(obj) {
		$('#table-users').datagrid('load', {});
		$('#window-search-user').dialog('close');
		$('#form-search-user').form('clear');
	}

	function searchPegawai(obj) {
		$('#window-search-pegawai').dialog({
			onOpen: function() {
				$('#table-search-pegawai').datagrid({
					url: '<?= site_url('api/pegawai/manage_pegawai/datagrid') ?>',
					method: 'get',
					pagination: true,
					fit: true,
					singleSelect: true,
					toolbar: '#table-search-pegawai-tb',
					fitColumns: true,
					rownumbers: true,
					columns:[[
						{
							title: 'Nip Baru',
							field: 'nip_baru',
							width: 300,
							sortable: true
						},
						{
							title: 'Nama Pegawai',
							field: 'nama',
							width: 300,
							sortable: true
						},
						{
							title: 'L/P',
							field: 'jk',
							width: 70,
							sortable: true
						}
					]],
					onClickRow: function(index,row) {
						$('#form-user').find('[name="nip"]').val(row.nip_baru);
						$('#window-search-pegawai').window('close');
					}
				});
			}
		});
	}

	function doSearchPegawai(e, obj) {
		if ( e.keyCode == 13 ) {
			var nip_baru = $(obj).val();

			$('#table-search-pegawai').datagrid('load', {
				nip_baru: nip_baru
			});
		}
	}

	$(document).ready(function() {
		$('#table-users').datagrid({
			title: '<i class="fa fa-fw fa-users"></i> Data Pengguna',
			url: '<?= site_url('api/users/manage_user/datagrid') ?>',
			method: 'get',
			pagination: true,
			fit: true,
			rownumbers: true,
			fitColumns: true,
			singleSelect: true,
			toolbar: '#toolbar-table-users',
			pageSize: 20,
			striped: true,
			emptyMsg: 'Tidak ada data ditemukan',
			multiSort: true,
			columns: [[
				{
					title: 'Nama Lengkap',
					field: 'name',
					width: 130,
					sortable: true
				},
				{
					title: 'Email',
					field: 'email',
					width: 140,
					sortable: true
				},
				{
					title: 'Level',
					field: 'group_description',
					width: 130,
					sortable: true
				},
				{
					title: 'Tgl. Daftar',
					field: 'created_on',
					width: 130,
					sortable: true
				},
				{
					title: 'Login Terakhir',
					field: 'last_login',
					width: 130,
					sortable: true
				},
				{
					title: 'IP Adress',
					field: 'ip_address',
					width: 80,
					sortable: true
				},
				{
					title: 'Aktif',
					field: 'active',
					align: 'center',
					width: 30,
					formatter: function(value,row,index) {
						if ( value == true ) {
							var html = '<i class="fa fa-fw fa-check-circle text-success"></i>';
						} else {
							var html = '<i class="fa fa-fw fa-times-circle text-danger"></i>';
						}
						return html;
					}
				}
			]]
		});

		// validate nip
		$('#form-user').find('[name="group_id"]').on('change', function() {
			var group_id = $(this).val();
			if (group_id == 2) {
				$('#form-user').find('[name="nip"]').validatebox({
					required: true,
					validateOnCreate: false
				});
			} else {
				$('#form-user').find('[name="nip"]').validatebox({
					required: false,
					validateOnCreate: false
				});
			}
		});
	});
</script>