<div id="window-search-user"
	title="Pencarian Pengguna",
	data-options="width:500,iconCls:'icon-search',inline:true"
	style="display:none">
	<form id="form-search-user" method="post" class="form-horizontal" role="form" onsubmit="doSearchUser(this); return false" onreset="doRefreshUser(this)">
		<div class="panel-content">
			<div class="form-group">
				<label class="control-label col-xs-4">Nama Lengkap</label>
				<div class="col-xs-8">
					<input name="nama_lengkap" type="text" class="form-control input-sm" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-4">Email</label>
				<div class="col-xs-8">
					<input name="email" type="text" class="form-control input-sm" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-4">Level</label>
				<div class="col-xs-8">
					<?= dinamyc_dropdown(array(
						'name' => 'group_id',
						'table' => 'groups',
						'key' => 'id',
						'label' => 'description',
						'default' => '',
						'empty_first' => TRUE,
						'empty_first_label' => '- Semua -',
						'attr' => 'class="form-control input-sm"'
					)) ?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-4">Tgl. Daftar</label>
				<div class="col-xs-8">
					<div class="row row-sm">
						<div class="col-xs-6">
							<input name="created_on_begin" type="text" class="form-control input-sm easyui-datebox" style="width:100%" />
						</div>
						<div class="col-xs-6">
							<input name="created_on_end" type="text" class="form-control input-sm easyui-datebox" style="width:100%" />
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-4">Login Terakhir</label>
				<div class="col-xs-8">
					<div class="row row-sm">
						<div class="col-xs-6">
							<input name="last_login_begin" type="text" class="form-control input-sm easyui-datebox" style="width:100%" />
						</div>
						<div class="col-xs-6">
							<input name="last_login_end" type="text" class="form-control input-sm easyui-datebox" style="width:100%" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-xs-8 col-xs-offset-4">
					<button type="submit" class="btn btn-sm btn-default btn-round"><i class="fa fa-fw fa-search"></i> Cari</button>	
					<button type="reset" class="btn btn-sm btn-default btn-round"><i class="fa fa-fw fa-refresh"></i> Segarkan</button>	
				</div>
			</div>
		</div>
	</form>
</div>