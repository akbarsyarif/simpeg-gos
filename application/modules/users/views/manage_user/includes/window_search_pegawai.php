<div id="window-search-pegawai"
	title="Data Pegawai",
	data-options="width:500,height:400,iconCls:'icon-search',inline:true"
	style="padding:5px;display:none">
	<table id="table-search-pegawai"></table>
	<div id="table-search-pegawai-tb" style="padding:5px">
		<input name="nip_baru" class="form-control" placeholder="Masukkan NIK lalu tekan Enter" onkeypress="doSearchPegawai(event, this)" />
	</div>
</div>