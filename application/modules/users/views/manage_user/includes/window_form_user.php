<div id="window-form-user"
	style="display:none"
	data-options="width:500,inline:true">
	<form id="form-user" class="form-horizontal" role="form" onsubmit="doSubmitUserForm(this); return false" onreset="doResetUserForm(this)">
		<input type="hidden" name="id" />
		<div class="panel-content">
			<div class="form-group">
				<label for="name" class="col-xs-4 control-label">Nama Lengkap</label>
				<div class="col-xs-8">
					<input class="form-control input-sm easyui-validatebox" data-options="required:true" type="text" name="name" />
				</div>
			</div>
			<div class="form-group">
				<label for="email" class="col-xs-4 control-label">Email</label>
				<div class="col-xs-8">
					<input class="form-control input-sm easyui-validatebox" data-options="required:true,validType:'email'" type="text" name="email" />
				</div>
			</div>
			<div class="form-group">
				<label for="username" class="col-xs-4 control-label">Username</label>
				<div class="col-xs-8">
					<input class="form-control input-sm easyui-validatebox" data-options="required:true" type="text" name="username" />
				</div>
			</div>
			<div class="form-group">
				<label for="password" class="col-xs-4 control-label">Kata Sandi</label>
				<div class="col-xs-8">
					<input id="password" class="form-control input-sm easyui-validatebox" data-options="required:true" type="password" name="password" />
				</div>
			</div>
			<div class="form-group">
				<label for="repeat_password" class="col-xs-4 control-label">Ulangi Kata Sandi</label>
				<div class="col-xs-8">
					<input class="form-control input-sm easyui-validatebox" validType="equals['#password']" data-options="required:true" type="password" name="repeat_password" />
				</div>
			</div>
			<div class="form-group">
				<label for="group_id" class="col-xs-4 control-label">Level</label>
				<div class="col-xs-8">
					<?= dinamyc_dropdown(array(
						'name' => 'group_id',
						'table' => 'groups',
						'key' => 'id',
						'label' => 'description',
						'default' => '',
						'empty_first' => TRUE,
						'empty_first_label' => '- Pilih Grup -',
						'attr' => 'class="form-control input-sm easyui-validatebox" data-options="required:true"'
					)) ?>
				</div>
			</div>
			<div class="form-group">
				<label for="id_pegawai" class="col-xs-4 control-label">NIP (Jika Pegawai)</label>
				<div class="col-xs-8">
					<div class="input-group">
						<input class="form-control input-sm" type="text" name="nip" placeholder="Cari pegawai..." />
						<span class="input-group-btn">
							<button class="btn btn-default btn-sm" type="button" onClick="searchPegawai(this)"><i class="fa fa-fw fa-search"></i></button>
						</span>
					</div>
					<p class="help-block">
						Nip hanya diisi jika level user adalah pegawai
					</p>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-xs-8 col-xs-offset-4">
					<button type="submit" class="btn btn-sm btn-success btn-round" data-loading-text="<i class='fa fa-fw fa-circle-o-notch fa-spin'></i> loading..."><i class="fa fa-fw fa-check-circle"></i> Simpan</button> 
					<button type="reset" class="btn btn-sm btn-default btn-round"><i class="fa fa-fw fa-times-circle"></i> Batal</button> 
				</div>
			</div>
		</div>
	</form>
</div>