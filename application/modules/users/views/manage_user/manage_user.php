<div id="toolbar-table-users">
	<table cellpadding="0" cellspacing="0" class="tb-toolbar">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td><a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="addUser(this)"><i class="fa fa-fw fa-plus"></i> Tambah</a></td>
						<td><a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="editUser(this)"><i class="fa fa-fw fa-edit"></i> Edit</a></td>
						<td><a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="removeUser(this)"><i class="fa fa-fw fa-trash"></i> Hapus</a></td>
						<td><div class="datagrid-btn-separator"></div></td>
						<td><a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="doLoginAs(this)"><i class="fa fa-fw fa-key"></i> Login Sebagai</a></td>
						<td><div class="datagrid-btn-separator"></div></td>
						<td><a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="doResetPassword(this)"><i class="fa fa-fw fa-refresh"></i> Reset Password</a></td>
					</tr>
				</table>
			</td>
			<td align="right">
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td><a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="searchUser(this)"><i class="fa fa-fw fa-search"></i> Pencarian</a></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
<table id="table-users"></table>

<?php require 'includes/window_search_user.php' ?>
<?php require 'includes/window_search_pegawai.php' ?>
<?php require 'includes/window_form_user.php' ?>

<!-- include javascript -->
<?php require 'includes/app.js.php' ?>