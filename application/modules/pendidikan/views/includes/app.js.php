<script type="text/javascript">
	function addPendidikan(obj) {
		$('#window-form-pendidikan').dialog({
			iconCls: "icon-add",
			title: "Tambah Pendidikan",
			onBeforeOpen: function() {
				// clear form fields
				$('#form-pendidikan').form('reset');

				// enabled validation for username, password and repeat_password
				$('#form-pendidikan').find('[name="pendidikan"]').removeAttr('disabled', '').validatebox('enableValidation');
			}
		});
	}

	function editPendidikan(obj) {
		var data = $('#table-pendidikan').datagrid('getSelected');
		
		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin diedit', 'warning');
		} else {
			$('#window-form-pendidikan').dialog({
				iconCls: "icon-edit",
				title: "Edit Jurusan",
				onBeforeOpen: function() {
					// set fields value
					$('#form-pendidikan').find('[name="id"]').val(data.id);
					$('#form-pendidikan').find('[name="pendidikan"]').val(data.pendidikan);

					// disabled validation for username, password and repeat_password
					$('#form-pendidikan').find('[name="pendidikan"]').removeAttr('disabled', '').validatebox('enableValidation');
				}
			});
		}
	}

	function doSubmitPendidikanForm(obj) {
		let data = $(obj).serialize();
		let id = $(obj).find('[name="id"]').val();
		if ( typeof id !== "undefined" && id !== "" ) {
			var method = "PUT";
		} else {
			var method = "POST";
		}

		$.ajax({
			url: '<?= site_url('api/pendidikan/manage_pendidikan') ?>',
			method: method,
			dataType: 'json',
			data: data,
			beforeSend: function(xhr) {
				isValid = $(obj).form('validate');
				if ( isValid ) {
					$(obj).find('[type="submit"]').button('loading');
				}
				return isValid;
			},
			success: function(xhr) {
				if ( typeof xhr === "object" ) {
					if ( xhr.metadata.code != 200 ) {
						$.messager.alert('Error', xhr.metadata.message, 'error');
					} else {
						if ( method === "PUT" ) {
							$('#window-form-pendidikan').dialog('close');
						}
						$('#table-pendidikan').datagrid('reload');
						$(obj).form('reset');
					}
				} else {
					$.messager.alert('Error', 'Server error', 'error');
					console.log(xhr);
				}
			},
			error: function(xhr) {
				let error = JSON.parse(xhr.responseText);
				if ( typeof error == "object" ) {
					var message = error.metadata.message;
				} else {
					var message = "Tidak dapat menghubungi server";
				}
				$.messager.alert('Error', message, 'error');
				console.log(error);
			},
			complete: function() {
				$(obj).find('[type="submit"]').button('reset');
			}
		});
	}

	function doResetPendidikanForm(obj) {
		$('#window-form-pendidikan').dialog('close');
		$('#form-pendidikan').form('clear');
	}

	function removePendidikan(obj) {
		var data = $('#table-pendidikan').datagrid('getSelected');

		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin dihapus', 'warning');
		} else {
			$.messager.confirm('Hapus Data', 'Anda yakin ingin menghapus data pengguna <b>'+ data.pendidikan +'</b>?', function(r) {
				if ( r ) {
					$('#table-pendidikan').datagrid('loading');

					$.ajax({
						url: '<?= site_url('api/pendidikan/manage_pendidikan') ?>',
						method: 'delete',
						data: {
							id: data.id
						},
						dataType: 'json',
						success: function(xhr) {
							if ( typeof xhr === "object" ) {
								if ( xhr.metadata.code != 200 ) {
									$.messager.alert('Error', xhr.metadata.message, 'error');
								} else {
									$('#table-pendidikan').datagrid('reload');
								}
							} else {
								$.messager.alert('Error', 'Server error', 'error');
								console.log(xhr);
							}
						},
						error: function(xhr) {
							let error = JSON.parse(xhr.responseText);
							if ( typeof error == "object" ) {
								var message = error.metadata.message;
							} else {
								var message = "Tidak dapat menghubungi server";
							}
							$.messager.alert('Error', message, 'error');
							console.log(error);
						},
						complete: function() {
							$('#table-pendidikan').datagrid('loaded');
						}
					})
				}
			});
		}
	}

	function searchPendidikan(obj) {
		$('#window-search-pendidikan').dialog();
	}

	function doSearchPendidikan(obj) {
		var j = $(obj).find('[name="pendidikan"]').val();

		$('#table-pendidikan').datagrid('load', {
			pendidikan: j,
		});

		$('#window-search-pendidikan').dialog('close');
	}

	function doRefreshJurusan(obj) {
		$('#table-pendidikan').datagrid('load', {});
		$('#window-search-pendidikan').dialog('close');
		$('#form-search-pendidikan').form('clear');
	}

	$(document).ready(function() {
		$('#table-pendidikan').datagrid({
			title: '<i class="fa fa-fw fa-mortar-board"></i> Data Jurusan',
			url: '<?= site_url('api/pendidikan/manage_pendidikan/datagrid') ?>',
			method: 'get',
			pagination: true,
			fit: true,
			rownumbers: true,
			fitColumns: true,
			singleSelect: true,
			toolbar: '#toolbar-table-pendidikan',
			pageSize: 20,
			striped: true,
			emptyMsg: 'Tidak ada data ditemukan',
			multiSort: true,
			columns: [[
				{
					title: 'Pendidikan',
					field: 'pendidikan',
					width: 130,
					sortable: true
				}
			]]
		});
	});
</script>