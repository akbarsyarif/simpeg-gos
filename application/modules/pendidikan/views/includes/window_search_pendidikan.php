<div id="window-search-pendidikan"
	title="Pencarian Pendidikan",
	data-options="width:500,iconCls:'icon-search',modal:true"
	style="display:none">
	<form id="form-search-pendidikan" method="post" class="form-horizontal" role="form" onSubmit="doSearchPendidikan(this); return false" onReset="doRefreshPendidikan(this)">
		<div class="panel-content">
			<div class="form-group">
				<label for="name" class="col-xs-4 control-label">Pendidikan</label>
				<div class="col-xs-8">
					<input class="form-control input-sm easyui-validatebox" data-options="required:true" type="text" name="pendidikan" />
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-xs-9 col-xs-offset-3">
					<button type="submit" class="btn btn-sm btn-default btn-round"><i class="fa fa-fw fa-search"></i> Cari</button>
					<button type="reset" class="btn btn-sm btn-default btn-round"><i class="fa fa-fw fa-refresh"></i> Segarkan</button>
				</div>
			</div>
		</div>
	</form>
</div>
