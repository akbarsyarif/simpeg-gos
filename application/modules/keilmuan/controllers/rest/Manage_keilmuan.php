<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_keilmuan extends SP_Rest_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_keilmuan');
	}

	/**
	 * response for jeasyui datagrid
	 */
	public function datagrid_get()
	{
		$query=[];
		// search params
		if ( isset($_GET['bidang_keilmuan']) && !empty($_GET['bidang_keilmuan']) ) $query['where']['bidang_keilmuan LIKE'] = '%' . $this->input->get('bidang_keilmuan', TRUE) . '%';
		
		$count=$this->m_keilmuan->count($query);

		// set limit and offset
		$offset = isset($_GET['page']) ? $this->input->get('page', TRUE) : 1;
		$limit  = isset($_GET['rows']) ? $this->input->get('rows', TRUE) : 10;
		$offset = ($offset - 1) * $limit;
		$query['limit_offset'] = [$limit, $offset];

		// sorting
		$sort = $this->input->get('sort', TRUE);
		$order = $this->input->get('order', TRUE);
		$split_sort = explode(',', $sort);
		$split_order = explode(',', $order);

		if ( isset($_GET['sort']) && isset($_GET['order']) ) {
			if ( is_array($split_sort) && count($split_sort) > 0 ) {
				foreach ($split_sort as $index => $field) {
					$orders[$field] = $split_order[$index];
				}
				$query['order_by'] = $orders;
			} else {
				$query['order_by'] = "{$sort} {$order}";
			}
		}

		$result = $this->m_keilmuan->get($query);

		$this->response([
			'total' => $count,
			'rows' => $result['data']
		]);
	}

	/**
	* form post handler
	*/
	public function index_post()
	{
		$rules=
		[
			[
				'field'=>'bidang_keilmuan',
				'label'=>'bidang keilmuan',
				'rules'=>'required|xss_clean|min_length[5]|is_unique[bidang_keilmuan.bidang_keilmuan]'
			]
		];

		$this->form_validation->set_rules($rules);
		if($this->form_validation->run()== FALSE)
		{
			$this->response([
				'metadata'=>[
					'code'=>"400",
					'message'=>strip_tags(validation_errors())
				]
			]);
		}
		else
		{
			$data=
			[
				'bidang_keilmuan'=>$this->input->post('bidang_keilmuan'),
			];
			if($this->m_keilmuan->insert($data)>0)
			{
				$this->response([
					'metadata'=>[
						'code'=>"200",
						'message'=>"OK"
					]
				]);
			}
			else
			{
				$this->reponse([
					'metadata'=>[
						'code'=>"400",
						'message'=>"Gagal menyimpan data"
					]
				]);
			}
		}
	}

	/**
	* update date handler
	*/
	public function index_put()
	{
		$this->form_validation->set_data($this->put());
		$id=$this->put('id',TRUE);

		$rules=
		[
			[
				'field'=>'bidang_keilmuan',
				'label'=>'bidang keilmuan',
				'rules'=>'required|min_length[5]|xss_clean'
			]
		];
		$this->form_validation->set_rules($rules);
		if($this->form_validation->run()==FALSE){
			$this->response([
				'metadata'=>[
					'code'=>"400",
					'message'=>strip_tags(validation_errors())
				]
			]);
		} else {
			$data=[
				'bidang_keilmuan'=>$this->put('bidang_keilmuan',TRUE)
			];
			if($this->m_keilmuan->where('id',$id)->update($data)){
				$this->response([
					'metadata'=>[
						'code'=>"200",
						'message'=>"OK"
					]
				]);
			}else {
				$this->response([
					'metadata'=>[
						'code'=>"400",
						'message'=>"Gagal memperbarui data"
					]
				]);
			}
		}
	}

	/**
	 * remove golongan
	 */
	public function index_delete()
	{
		$id = $this->delete('id');

		$result = $this->m_keilmuan->where('id', $id)->delete();

		if ( $result == FALSE ) {
			$this->response([
				'metadata' => [
					'code' => "500",
					'message' => "Server error, gagal menghapus data."
				]
			], 500);
		} else {
			$this->response([
				'metadata' => [
					'code' => "200",
					'message' => "OK"
				]
			]);
		}
	}

}

/* End of file Manage_golongan.php */
/* Location: ./application/modules/golongan/controllers/rest/Manage_golongan.php */
