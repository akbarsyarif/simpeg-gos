<script type="text/javascript">
	function addKeilmuan(obj) {
		$('#window-form-keilmuan').dialog({
			iconCls: "icon-add",
			title: "Tambah Bidang Keilmuan",
			onBeforeOpen: function() {
				// clear form fields
				$('#form-keilmuan').form('reset');

				// enabled validation for username, password and repeat_password
				$('#form-keilmuan').find('[name="bidang_keilmuan"]').removeAttr('disabled', '').validatebox('enableValidation');
			}
		});
	}

	function editKeilmuan(obj) {
		var data = $('#table-keilmuan').datagrid('getSelected');
		
		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin diedit', 'warning');
		} else {
			$('#window-form-keilmuan').dialog({
				iconCls: "icon-edit",
				title: "Edit Bidang Keilmuan",
				onBeforeOpen: function() {
					// set fields value
					$('#form-keilmuan').find('[name="id"]').val(data.id);
					$('#form-keilmuan').find('[name="bidang_keilmuan"]').val(data.bidang_keilmuan);

					// disabled validation for username, password and repeat_password
					$('#form-keilmuan').find('[name="bidang_keilmuan"]').removeAttr('disabled', '').validatebox('enableValidation');
				}
			});
		}
	}

	function doSubmitKeilmuanForm(obj) {
		let data = $(obj).serialize();
		let id = $(obj).find('[name="id"]').val();
		if ( typeof id !== "undefined" && id !== "" ) {
			var method = "PUT";
		} else {
			var method = "POST";
		}

		$.ajax({
			url: '<?= site_url('api/keilmuan/manage_keilmuan') ?>',
			method: method,
			dataType: 'json',
			data: data,
			beforeSend: function(xhr) {
				isValid = $(obj).form('validate');
				if ( isValid ) {
					$(obj).find('[type="submit"]').button('loading');
				}
				return isValid;
			},
			success: function(xhr) {
				if ( typeof xhr === "object" ) {
					if ( xhr.metadata.code != 200 ) {
						$.messager.alert('Error', xhr.metadata.message, 'error');
					} else {
						if ( method === "PUT" ) {
							$('#window-form-keilmuan').dialog('close');
						}
						$('#table-keilmuan').datagrid('reload');
						$(obj).form('reset');
					}
				} else {
					$.messager.alert('Error', 'Server error', 'error');
					console.log(xhr);
				}
			},
			error: function(xhr) {
				let error = JSON.parse(xhr.responseText);
				if ( typeof error == "object" ) {
					var message = error.metadata.message;
				} else {
					var message = "Tidak dapat menghubungi server";
				}
				$.messager.alert('Error', message, 'error');
				console.log(error);
			},
			complete: function() {
				$(obj).find('[type="submit"]').button('reset');
			}
		});
	}

	function doResetKeilmuanForm(obj) {
		$('#window-form-keilmuan').dialog('close');
		$('#form-keilmuan').form('clear');
	}

	function removeKeilmuan(obj) {
		var data = $('#table-keilmuan').datagrid('getSelected');

		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin dihapus', 'warning');
		} else {
			$.messager.confirm('Hapus Data', 'Anda yakin ingin menghapus data pengguna <b>'+ data.bidang_keilmuan +'</b>?', function(r) {
				if ( r ) {
					$('#table-keilmuan').datagrid('loading');

					$.ajax({
						url: '<?= site_url('api/keilmuan/manage_keilmuan') ?>',
						method: 'delete',
						data: {
							id: data.id
						},
						dataType: 'json',
						success: function(xhr) {
							if ( typeof xhr === "object" ) {
								if ( xhr.metadata.code != 200 ) {
									$.messager.alert('Error', xhr.metadata.message, 'error');
								} else {
									$('#table-keilmuan').datagrid('reload');
								}
							} else {
								$.messager.alert('Error', 'Server error', 'error');
								console.log(xhr);
							}
						},
						error: function(xhr) {
							let error = JSON.parse(xhr.responseText);
							if ( typeof error == "object" ) {
								var message = error.metadata.message;
							} else {
								var message = "Tidak dapat menghubungi server";
							}
							$.messager.alert('Error', message, 'error');
							console.log(error);
						},
						complete: function() {
							$('#table-keilmuan').datagrid('loaded');
						}
					})
				}
			});
		}
	}

	function searchKeilmuan(obj) {
		$('#window-search-keilmuan').dialog();
	}

	function doSearchKeilmuan(obj) {
		var a = $(obj).find('[name="bidang_keilmuan"]').val();

		$('#table-keilmuan').datagrid('load', {
			bidang_keilmuan: a,
		});

		$('#window-search-keilmuan').dialog('close');
	}

	function doRefreshKeilmuan(obj) {
		$('#table-keilmuan').datagrid('load', {});
		$('#window-search-keilmuan').dialog('close');
		$('#form-search-keilmuan').form('clear');
	}

	$(document).ready(function() {
		$('#table-keilmuan').datagrid({
			title: '<i class="fa fa-fw fa-book"></i> Data Bidang Keilmuan',
			url: '<?= site_url('api/keilmuan/manage_keilmuan/datagrid') ?>',
			method: 'get',
			pagination: true,
			fit: true,
			rownumbers: true,
			fitColumns: true,
			singleSelect: true,
			toolbar: '#toolbar-table-keilmuan',
			pageSize: 20,
			striped: true,
			emptyMsg: 'Tidak ada data ditemukan',
			multiSort: true,
			columns: [[
				{
					title: 'Bidang Keilmuan',
					field: 'bidang_keilmuan',
					width: 130,
					sortable: true
				},
			]]
		});
	});
</script>