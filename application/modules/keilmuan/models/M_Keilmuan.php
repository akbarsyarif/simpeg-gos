<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Keilmuan extends MY_Model {

	public $table = 'bidang_keilmuan';
	public $pk    = 'id';
	public $view  = 'v_bid_keilmuan';
	
	public function __construct()
	{
		parent::__construct();
	}
}

/* End of file M_User.php */
/* Location: ./application/models/M_User.php */
