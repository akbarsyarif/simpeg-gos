<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaturan_umum extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('pengaturan_umum', $this->data);
	}

}

/* End of file Pengaturan_umum.php */
/* Location: ./application/modules/settings/controllers/Pengaturan_umum.php */