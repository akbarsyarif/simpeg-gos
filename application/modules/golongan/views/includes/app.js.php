<script type="text/javascript">
	function addGolongan(obj) {
		$('#window-form-golongan').dialog({
			iconCls: "icon-add",
			title: "Tambah Golongan",
			onBeforeOpen: function() {
				// clear form fields
				$('#form-golongan').form('reset');

				// enabled validation for username, password and repeat_password
				$('#form-golongan').find('[name="golongan"]').removeAttr('disabled', '').validatebox('enableValidation');
				$('#form-golongan').find('[name="pangkat"]').removeAttr('disabled', '').validatebox('enableValidation');
			}
		});
	}

	function editGolongan(obj) {
		var data = $('#table-golongan').datagrid('getSelected');
		
		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin diedit', 'warning');
		} else {
			$('#window-form-golongan').dialog({
				iconCls: "icon-edit",
				title: "Edit Golongan",
				onBeforeOpen: function() {
					// set fields value
					$('#form-golongan').find('[name="id"]').val(data.id);
					$('#form-golongan').find('[name="pangkat"]').val(data.pangkat);
					$('#form-golongan').find('[name="golongan"]').val(data.golongan);

					// disabled validation for username, password and repeat_password
					$('#form-golongan').find('[name="golongan"]').removeAttr('disabled', '').validatebox('enableValidation');
					$('#form-golongan').find('[name="pangkat"]').removeAttr('disabled', '').validatebox('enableValidation');
				}
			});
		}
	}

	function doSubmitGolonganForm(obj) {
		let data = $(obj).serialize();
		let id = $(obj).find('[name="id"]').val();
		if ( typeof id !== "undefined" && id !== "" ) {
			var method = "PUT";
		} else {
			var method = "POST";
		}

		$.ajax({
			url: '<?= site_url('api/golongan/manage_golongan') ?>',
			method: method,
			dataType: 'json',
			data: data,
			beforeSend: function(xhr) {
				isValid = $(obj).form('validate');
				if ( isValid ) {
					$(obj).find('[type="submit"]').button('loading');
				}
				return isValid;
			},
			success: function(xhr) {
				if ( typeof xhr === "object" ) {
					if ( xhr.metadata.code != 200 ) {
						$.messager.alert('Error', xhr.metadata.message, 'error');
					} else {
						if ( method === "PUT" ) {
							$('#window-form-golongan').dialog('close');
						}
						$('#table-golongan').datagrid('reload');
						$(obj).form('reset');
					}
				} else {
					$.messager.alert('Error', 'Server error', 'error');
					console.log(xhr);
				}
			},
			error: function(xhr) {
				let error = JSON.parse(xhr.responseText);
				if ( typeof error == "object" ) {
					var message = error.metadata.message;
				} else {
					var message = "Tidak dapat menghubungi server";
				}
				$.messager.alert('Error', message, 'error');
				console.log(error);
			},
			complete: function() {
				$(obj).find('[type="submit"]').button('reset');
			}
		});
	}

	function doResetGolonganForm(obj) {
		$('#window-form-golongan').dialog('close');
		$('#form-golongan').form('clear');
	}

	function removeGolongan(obj) {
		var data = $('#table-golongan').datagrid('getSelected');

		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin dihapus', 'warning');
		} else {
			$.messager.confirm('Hapus Data', 'Anda yakin ingin menghapus data pengguna <b>'+ data.golongan +'</b>?', function(r) {
				if ( r ) {
					$('#table-golongan').datagrid('loading');

					$.ajax({
						url: '<?= site_url('api/golongan/manage_golongan') ?>',
						method: 'delete',
						data: {
							id: data.id
						},
						dataType: 'json',
						success: function(xhr) {
							if ( typeof xhr === "object" ) {
								if ( xhr.metadata.code != 200 ) {
									$.messager.alert('Error', xhr.metadata.message, 'error');
								} else {
									$('#table-golongan').datagrid('reload');
								}
							} else {
								$.messager.alert('Error', 'Server error', 'error');
								console.log(xhr);
							}
						},
						error: function(xhr) {
							let error = JSON.parse(xhr.responseText);
							if ( typeof error == "object" ) {
								var message = error.metadata.message;
							} else {
								var message = "Tidak dapat menghubungi server";
							}
							$.messager.alert('Error', message, 'error');
							console.log(error);
						},
						complete: function() {
							$('#table-golongan').datagrid('loaded');
						}
					})
				}
			});
		}
	}

	function searchGolongan(obj) {
		$('#window-search-golongan').dialog();
	}

	function doSearchGolongan(obj) {
		var g = $(obj).find('[name="golongan"]').val();
		var p = $(obj).find('[name="pangkat"]').val();

		$('#table-golongan').datagrid('load', {
			golongan: g,
			pangkat: p,
		});

		$('#window-search-golongan').dialog('close');
	}

	function doRefreshGolongan(obj) {
		$('#table-golongan').datagrid('load', {});
		$('#window-search-golongan').dialog('close');
		$('#form-search-eselon').form('clear');
	}

	$(document).ready(function() {
		$('#table-golongan').datagrid({
			title: '<i class="fa fa-fw fa-users"></i> Data Golongan',
			url: '<?= site_url('api/golongan/manage_golongan/datagrid') ?>',
			method: 'get',
			pagination: true,
			fit: true,
			rownumbers: true,
			fitColumns: true,
			singleSelect: true,
			toolbar: '#toolbar-table-golongan',
			pageSize: 20,
			striped: true,
			emptyMsg: 'Tidak ada data ditemukan',
			multiSort: true,
			columns: [[
				{
					title: 'Golongan',
					field: 'golongan',
					width: 130,
					sortable: true
				},
				{
					title: 'Pangkat',
					field: 'pangkat',
					width: 140,
					sortable: true
				}
			]]
		});
	});
</script>