<div id="window-form-golongan"
	style="display:none"
	data-options="width:500,inline:true">
	<form id="form-golongan" class="form-horizontal" role="form" onsubmit="doSubmitGolonganForm(this); return false" onreset="doResetGolonganForm(this)">
		<input type="hidden" name="id" />
		<div class="panel-content">
			<div class="form-group">
				<label for="name" class="col-xs-4 control-label">Golongan</label>
				<div class="col-xs-8">
					<input class="form-control input-sm easyui-validatebox" data-options="required:true" type="text" name="golongan" />
				</div>
			</div>
			<div class="form-group">
				<label for="name" class="col-xs-4 control-label">Pangkat</label>
				<div class="col-xs-8">
					<input class="form-control input-sm easyui-validatebox" data-options="required:true" type="text" name="pangkat" />
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-xs-8 col-xs-offset-4">
					<button type="submit" class="btn btn-sm btn-success btn-round" data-loading-text="<i class='fa fa-fw fa-circle-o-notch fa-spin'></i> loading..."><i class="fa fa-fw fa-check-circle"></i> Simpan</button> 
					<button type="reset" class="btn btn-sm btn-default btn-round"><i class="fa fa-fw fa-times-circle"></i> Batal</button> 
				</div>
			</div>
		</div>
	</form>
</div>