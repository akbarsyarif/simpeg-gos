<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_golongan extends SP_Rest_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_golongan');
	}

	/**
	 * response for jeasyui datagrid
	 */
	public function datagrid_get()
	{
		$query=[];

		// search params
		if ( isset($_GET['golongan']) && !empty($_GET['golongan']) ) $query['where']['golongan LIKE'] = '%' . $this->input->get('golongan', TRUE) . '%';
		if ( isset($_GET['pangkat']) && !empty($_GET['pangkat']) ) $query['where']['pangkat LIKE'] = '%' . $this->input->get('pangkat', TRUE) . '%';

		$count=$this->m_golongan->count($query);

				// set limit and offset
		$offset = isset($_GET['page']) ? $this->input->get('page', TRUE) : 1;
		$limit  = isset($_GET['rows']) ? $this->input->get('rows', TRUE) : 10;
		$offset = ($offset - 1) * $limit;
		$query['limit_offset'] = [$limit, $offset];

		// sorting
		$sort = $this->input->get('sort', TRUE);
		$order = $this->input->get('order', TRUE);
		$split_sort = explode(',', $sort);
		$split_order = explode(',', $order);

		if ( isset($_GET['sort']) && isset($_GET['order']) ) {
			if ( is_array($split_sort) && count($split_sort) > 0 ) {
				foreach ($split_sort as $index => $field) {
					$orders[$field] = $split_order[$index];
				}
				$query['order_by'] = $orders;
			} else {
				$query['order_by'] = "{$sort} {$order}";
			}
		}

		$result = $this->m_golongan->get($query);

		$this->response([
			'total' => $count,
			'rows' => $result['data']
		]);
	}

	/**
	* form post handler
	*/
	public function index_post()
	{
		$rules=
		[
			[
				'field'=>'golongan',
				'label'=>'golongan',
				'rules'=>'required|xss_clean|is_unique[golongan.golongan]'
			],
			[
				'field'=>'pangkat',
				'label'=>'pangkat',
				'rules'=>'required|min_length[5]|max_length[50]|is_unique[golongan.pangkat]|xss_clean'
			]
		];

		$this->form_validation->set_rules($rules);
		if($this->form_validation->run()== FALSE)
		{
			$this->response([
				'metadata'=>[
					'code'=>"400",
					'message'=>strip_tags(validation_errors())
				]
			]);
		}
		else
		{
			$data=
			[
				'golongan'=>$this->input->post('golongan'),
				'pangkat'=>$this->input->post('pangkat')
			];
			if($this->m_golongan->insert($data)>0)
			{
				$this->response([
					'metadata'=>[
						'code'=>"200",
						'message'=>"OK"
					]
				]);
			}
			else
			{
				$this->reponse([
					'metadata'=>[
						'code'=>"400",
						'message'=>"Gagal menyimpan data"
					]
				]);
			}
		}
	}

	/**
	* update date handler
	*/
	public function index_put()
	{
		$this->form_validation->set_data($this->put());
		$id=$this->put('id',TRUE);

		$rules=[
			[
				'field'=>'golongan',
				'label'=>'golongan',
				'rules'=>'required|xss_clean'
			],
			[
				'field'=>'pangkat',
				'label'=>'pangkat',
				'rules'=>'required|min_length[5]|max_length[50]|xss_clean'
			]
		];
		$this->form_validation->set_rules($rules);
		if($this->form_validation->run()==FALSE){
			$this->response([
				'metadata'=>[
					'code'=>"400",
					'message'=>strip_tags(validation_errors())
				]
			],400);
		} else {
			$data=[
				'golongan'=>$this->put('golongan',TRUE),
				'pangkat'=>$this->put('pangkat',TRUE)
			];
			if($this->m_golongan->where('id',$id)->update($data)){
				$this->response([
					'metadata'=>[
						'code'=>"200",
						'message'=>"OK"
					]
				]);
			}else {
				$this->response([
					'metadata'=>[
						'code'=>"400",
						'message'=>"Gagal memperbarui data"
					]
				]);
			}
		}
	}

	/**
	 * remove golongan
	 */
	public function index_delete()
	{
		$id = $this->delete('id');

		$result = $this->m_golongan->where('id', $id)->delete();

		if ( $result == FALSE ) {
			$this->response([
				'metadata' => [
					'code' => "500",
					'message' => "Server error, gagal menghapus data."
				]
			], 500);
		} else {
			$this->response([
				'metadata' => [
					'code' => "200",
					'message' => "OK"
				]
			]);
		}
	}

}

/* End of file Manage_golongan.php */
/* Location: ./application/modules/golongan/controllers/rest/Manage_golongan.php */
