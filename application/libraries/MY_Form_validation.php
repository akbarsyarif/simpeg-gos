<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation
{

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * check conflict user email
	 * @param  [type] $str        [description]
	 * @param  [type] $exclude_id [description]
	 * @return [type]             [description]
	 */
	public function conflict_user_email($str, $exclude_id=NULL)
	{
		if ( $exclude_id !== NULL ) $this->CI->db->where_not_in('id', $exclude_id);

		$result = $this->CI->db->select('id')->from('users')->where('email', $str)->get();
		
		if ( $result->num_rows() > 0 ) {
			$this->set_message('conflict_user_email', '{field} has been used by other user.');
			return FALSE;
		} else {
			return TRUE;
		}
	}

	/**
	 * check conflict user nip
	 * @param  [type] $str        [description]
	 * @param  [type] $exclude_id [description]
	 * @return [type]             [description]
	 */
	public function conflict_user_nip($str, $exclude_id=NULL)
	{
		if ( $exclude_id !== NULL ) $this->CI->db->where_not_in('id', $exclude_id);

		$result = $this->CI->db->select('id')->from('users')->where('nip', $str)->get();
		
		if ( $result->num_rows() > 0 ) {
			$this->set_message('conflict_user_nip', '{field} has been used by other user.');
			return FALSE;
		} else {
			return TRUE;
		}
	}

	public function conflict_kode_klp_jabatan_fungsional_umum_1($str, $exclude_id=NULL)
	{
		if ( $exclude_id !== NULL ) $this->CI->db->where_not_in('id', $exclude_id);

		$result = $this->CI->db->select('id')->from('klp_jabatan_fungsional_umum_1')->where('kode', $str)->get();
		
		if ( $result->num_rows() > 0 ) {
			$this->set_message('conflict_kode_klp_jabatan_fungsional_umum_1', '{field} has been used.');
			return FALSE;
		} else {
			return TRUE;
		}
	}

	public function conflict_kode_klp_jabatan_fungsional_umum_2($str, $exclude_id=NULL)
	{
		if ( $exclude_id !== NULL ) $this->CI->db->where_not_in('id', $exclude_id);

		$result = $this->CI->db->select('id')->from('klp_jabatan_fungsional_umum_2')->where('kode', $str)->get();
		
		if ( $result->num_rows() > 0 ) {
			$this->set_message('conflict_kode_klp_jabatan_fungsional_umum_2', '{field} has been used.');
			return FALSE;
		} else {
			return TRUE;
		}
	}

	public function conflict_kode_jabatan_fungsional_umum($str, $exclude_id=NULL)
	{
		if ( $exclude_id !== NULL ) $this->CI->db->where_not_in('id', $exclude_id);
		
		$result = $this->CI->db->select('id')->from('jabatan_fungsional_umum')->where('kode', $str)->get();
		
		if ( $result->num_rows() > 0 ) {
			$this->set_message('conflict_kode_jabatan_fungsional_umum', '{field} has been used.');
			return FALSE;
		} else {
			return TRUE;
		}
	}

}

/* End of file MY_Form_validation.php */
/* Location: ./application/libraries/MY_Form_validation.php */
