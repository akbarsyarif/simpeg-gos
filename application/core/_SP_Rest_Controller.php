<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class SP_Rest_Controller extends REST_Controller {

	public $data = [];

	public function __construct()
	{
		parent::__construct();

		$this->data['app_title'] = $this->config->item('app_title');
		$this->data['app_description'] = $this->config->item('app_description');
		$this->data['app_version'] = $this->config->item('app_version');
	}

}

/* End of file _SP_Rest_Controller.php */
/* Location: ./application/core/_SP_Rest_Controller.php */