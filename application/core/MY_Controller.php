<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require __DIR__ .  DIRECTORY_SEPARATOR . '_MY_Controller.php';
require __DIR__ .  DIRECTORY_SEPARATOR . '_SP_Admin_Controller.php';
require __DIR__ .  DIRECTORY_SEPARATOR . '_SP_Client_Controller.php';
require __DIR__ .  DIRECTORY_SEPARATOR . '_SP_Rest_Controller.php';
require __DIR__ .  DIRECTORY_SEPARATOR . '_SP_Cli_Controller.php';

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */