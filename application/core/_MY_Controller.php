<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends MX_Controller {

	public $data = [];

	public function __construct()
	{
		parent::__construct();
		
		$this->data['app_title'] = $this->config->item('app_title');
		$this->data['app_description'] = $this->config->item('app_description');
		$this->data['app_version'] = $this->config->item('app_version');
	}

}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */