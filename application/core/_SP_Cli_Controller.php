<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SP_Cli_Controller extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		
		// can only be called from the command line
		if (!$this->input->is_cli_request()) {
			exit('Direct access is not allowed');
		}

		// can only be run in the development environment
		if (ENVIRONMENT !== 'development') {
			exit('Wowsers! You don\'t want to do that!');
		}
	}

}

/* End of file _SP_Cli_Controller.php */
/* Location: ./application/core/_SP_Cli_Controller.php */