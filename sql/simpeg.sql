-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.22-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for simpeg
CREATE DATABASE IF NOT EXISTS `simpeg` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `simpeg`;

-- Dumping structure for table simpeg.bidang_keilmuan
CREATE TABLE IF NOT EXISTS `bidang_keilmuan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bidang_keilmuan` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Master bidang keilmuan';

-- Dumping data for table simpeg.bidang_keilmuan: ~0 rows (approximately)
/*!40000 ALTER TABLE `bidang_keilmuan` DISABLE KEYS */;
INSERT INTO `bidang_keilmuan` (`id`, `bidang_keilmuan`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Komputer', '2017-07-12 11:33:02', NULL, NULL);
/*!40000 ALTER TABLE `bidang_keilmuan` ENABLE KEYS */;

-- Dumping structure for table simpeg.eselon
CREATE TABLE IF NOT EXISTS `eselon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eselon` varchar(100) DEFAULT NULL,
  `gol_min` int(11) DEFAULT NULL COMMENT 'Golongan minimal',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `gol_min` (`gol_min`),
  CONSTRAINT `FK_eselon_golongan` FOREIGN KEY (`gol_min`) REFERENCES `golongan` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Master eselon';

-- Dumping data for table simpeg.eselon: ~1 rows (approximately)
/*!40000 ALTER TABLE `eselon` DISABLE KEYS */;
INSERT INTO `eselon` (`id`, `eselon`, `gol_min`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'IVb', 1, '2017-07-12 11:31:31', NULL, NULL);
/*!40000 ALTER TABLE `eselon` ENABLE KEYS */;

-- Dumping structure for table simpeg.golongan
CREATE TABLE IF NOT EXISTS `golongan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `golongan` varchar(100) DEFAULT NULL,
  `pangkat` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pangkat` (`pangkat`),
  KEY `golongan` (`golongan`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Master golongan';

-- Dumping data for table simpeg.golongan: ~4 rows (approximately)
/*!40000 ALTER TABLE `golongan` DISABLE KEYS */;
INSERT INTO `golongan` (`id`, `golongan`, `pangkat`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'III/b', 'Penata Muda Tk.I', '2017-07-12 11:30:40', NULL, NULL),
	(2, 'III/a', 'Penata Muda', '2017-07-18 16:14:55', '2017-07-21 06:29:17', NULL),
	(3, 'III/c', 'Penata', '2017-07-18 16:15:22', NULL, NULL),
	(4, 'III/d', 'Penata Tk.I', '2017-07-18 16:15:35', NULL, NULL);
/*!40000 ALTER TABLE `golongan` ENABLE KEYS */;

-- Dumping structure for table simpeg.groups
CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table simpeg.groups: ~2 rows (approximately)
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` (`id`, `name`, `description`) VALUES
	(1, 'admin', 'Administrator'),
	(2, 'pegawai', 'Pegawai'),
	(3, 'skpd', 'Bidang Kepegawaian SKPD');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;

-- Dumping structure for table simpeg.jabatan_fungsional_tertentu
CREATE TABLE IF NOT EXISTS `jabatan_fungsional_tertentu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jabatan` varchar(100) DEFAULT NULL,
  `tingkat` enum('Terampil','Ahli') DEFAULT NULL,
  `id_klp` int(11) DEFAULT NULL,
  `id_golongan` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_klp` (`id_klp`),
  KEY `id_eselon` (`id_golongan`),
  CONSTRAINT `FK_jabatan_fungsional_tertentu_golongan` FOREIGN KEY (`id_golongan`) REFERENCES `golongan` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_jabatan_fungsional_tertentu_klp_jabatan_fungsional` FOREIGN KEY (`id_klp`) REFERENCES `klp_jabatan_fungsional_tertentu` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Jabatan fungsional tertentu';

-- Dumping data for table simpeg.jabatan_fungsional_tertentu: ~6 rows (approximately)
/*!40000 ALTER TABLE `jabatan_fungsional_tertentu` DISABLE KEYS */;
INSERT INTO `jabatan_fungsional_tertentu` (`id`, `jabatan`, `tingkat`, `id_klp`, `id_golongan`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Pertama', 'Ahli', 1, 2, '2017-07-26 16:45:22', '2017-07-27 21:29:07', NULL),
	(2, 'Pertama', 'Ahli', 1, 1, '2017-07-26 16:45:27', NULL, NULL),
	(3, 'Muda', 'Ahli', 1, 3, '2017-07-26 16:46:13', NULL, NULL),
	(4, 'Muda', 'Ahli', 1, 4, '2017-07-26 16:46:39', NULL, NULL),
	(6, 'Pertama', 'Terampil', 2, 2, '2017-07-27 06:00:39', '2017-07-27 06:57:45', NULL),
	(8, 'Pertama', 'Terampil', 5, 4, '2017-07-27 06:57:35', '2017-07-27 07:17:05', NULL);
/*!40000 ALTER TABLE `jabatan_fungsional_tertentu` ENABLE KEYS */;

-- Dumping structure for table simpeg.jabatan_fungsional_umum
CREATE TABLE IF NOT EXISTS `jabatan_fungsional_umum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(50) DEFAULT NULL,
  `jabatan` varchar(100) DEFAULT NULL,
  `id_klp_2` int(11) DEFAULT NULL COMMENT 'Kelompok jabatan fungsional (level 2)',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_klp_2` (`id_klp_2`),
  CONSTRAINT `FK_jabatan_fungsional_umum_klp_jabatan_fungsional_umum_2` FOREIGN KEY (`id_klp_2`) REFERENCES `klp_jabatan_fungsional_umum_2` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 COMMENT='Jabatan fungsional umum';

-- Dumping data for table simpeg.jabatan_fungsional_umum: ~60 rows (approximately)
/*!40000 ALTER TABLE `jabatan_fungsional_umum` DISABLE KEYS */;
INSERT INTO `jabatan_fungsional_umum` (`id`, `kode`, `jabatan`, `id_klp_2`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'K2-001', 'Accusantium doloremque a temporibus.', 12, '2017-08-02 13:05:43', NULL, NULL),
	(2, 'K2-002', 'Corporis harum cumque delectus impedit ea.', 5, '2017-08-02 13:05:43', NULL, NULL),
	(3, 'K2-003', 'Soluta adipisci in ullam neque odio.', 5, '2017-08-02 13:05:43', NULL, NULL),
	(4, 'K2-004', 'Sunt autem eius quisquam.', 6, '2017-08-02 13:05:43', NULL, NULL),
	(5, 'K2-005', 'Reprehenderit eum quod animi distinctio.', 2, '2017-08-02 13:05:43', NULL, NULL),
	(6, 'K2-006', 'Sunt iusto non porro culpa est.', 5, '2017-08-02 13:05:43', NULL, NULL),
	(7, 'K2-007', 'Rerum ea commodi eius autem repellendus nesciunt.', 11, '2017-08-02 13:05:43', NULL, NULL),
	(8, 'K2-008', 'Dolorem quae ipsum veritatis voluptas numquam est.', 3, '2017-08-02 13:05:43', NULL, NULL),
	(9, 'K2-009', 'At atque assumenda quod perspiciatis.', 8, '2017-08-02 13:05:43', NULL, NULL),
	(10, 'K2-010', 'Vitae esse sequi incidunt.', 17, '2017-08-02 13:05:44', NULL, NULL),
	(11, 'K2-011', 'Enim nesciunt doloremque et vel neque id.', 16, '2017-08-02 13:05:44', NULL, NULL),
	(12, 'K2-012', 'Quis ducimus molestias laudantium aut magnam quae quia.', 4, '2017-08-02 13:05:44', NULL, NULL),
	(13, 'K2-013', 'Voluptatem ex eaque occaecati.', 10, '2017-08-02 13:05:44', NULL, NULL),
	(14, 'K2-014', 'Quia ex architecto deleniti blanditiis asperiores.', 14, '2017-08-02 13:05:44', NULL, NULL),
	(15, 'K2-015', 'Dolores minus rerum veritatis vero consequatur.', 9, '2017-08-02 13:05:44', NULL, NULL),
	(16, 'K2-016', 'Aut repudiandae sint sint voluptatem maxime fugit.', 5, '2017-08-02 13:05:44', NULL, NULL),
	(17, 'K2-017', 'Et dolores temporibus sit voluptatem rerum.', 11, '2017-08-02 13:05:44', NULL, NULL),
	(18, 'K2-018', 'Magni ex in sapiente temporibus.', 14, '2017-08-02 13:05:44', NULL, NULL),
	(19, 'K2-019', 'Deserunt perspiciatis consequatur quibusdam autem laboriosam.', 3, '2017-08-02 13:05:44', NULL, NULL),
	(20, 'K2-020', 'Modi quaerat cum occaecati repellendus provident.', 14, '2017-08-02 13:05:44', NULL, NULL),
	(21, 'K2-021', 'Sit aut qui et et vero.', 14, '2017-08-02 13:05:44', NULL, NULL),
	(22, 'K2-022', 'Dicta et rerum ut autem laborum.', 17, '2017-08-02 13:05:44', NULL, NULL),
	(23, 'K2-023', 'Facere labore ab quo.', 19, '2017-08-02 13:05:44', NULL, NULL),
	(24, 'K2-024', 'Repudiandae nobis aut ullam consequatur delectus.', 11, '2017-08-02 13:05:44', NULL, NULL),
	(25, 'K2-025', 'Asperiores vel voluptatem saepe eligendi aut.', 9, '2017-08-02 13:05:44', NULL, NULL),
	(26, 'K2-026', 'Nihil quisquam cum quaerat ut ab.', 5, '2017-08-02 13:05:44', NULL, NULL),
	(27, 'K2-027', 'Incidunt provident dolorem et quidem architecto alias.', 5, '2017-08-02 13:05:44', NULL, NULL),
	(28, 'K2-028', 'Omnis quaerat cumque possimus quis omnis.', 7, '2017-08-02 13:05:44', NULL, NULL),
	(29, 'K2-029', 'Totam eos et doloribus.', 17, '2017-08-02 13:05:44', NULL, NULL),
	(30, 'K2-030', 'Placeat nisi iste nesciunt quasi nesciunt.', 3, '2017-08-02 13:05:44', NULL, NULL),
	(31, 'K2-031', 'Est dicta totam harum.', 18, '2017-08-02 13:05:44', NULL, NULL),
	(32, 'K2-032', 'Ut facilis voluptatem ipsum.', 5, '2017-08-02 13:05:44', NULL, NULL),
	(33, 'K2-033', 'Omnis quo sapiente qui cupiditate.', 15, '2017-08-02 13:05:44', NULL, NULL),
	(34, 'K2-034', 'Eum et molestiae et id.', 18, '2017-08-02 13:05:45', NULL, NULL),
	(35, 'K2-035', 'Aut vero unde modi.', 5, '2017-08-02 13:05:45', NULL, NULL),
	(36, 'K2-036', 'In sint optio laboriosam voluptatem quis enim.', 17, '2017-08-02 13:05:45', NULL, NULL),
	(37, 'K2-037', 'Dolore eveniet velit explicabo magni ut.', 15, '2017-08-02 13:05:45', NULL, NULL),
	(38, 'K2-038', 'Ea perspiciatis vel voluptatem odit.', 11, '2017-08-02 13:05:45', NULL, NULL),
	(39, 'K2-039', 'Eaque nihil nam quos dolorem.', 8, '2017-08-02 13:05:45', NULL, NULL),
	(40, 'K2-040', 'Consequatur qui sapiente rerum id non.', 3, '2017-08-02 13:05:45', NULL, NULL),
	(41, 'K2-041', 'Exercitationem in dicta voluptatem sed.', 3, '2017-08-02 13:05:45', NULL, NULL),
	(42, 'K2-042', 'Eligendi est ut et magnam nulla expedita.', 13, '2017-08-02 13:05:45', NULL, NULL),
	(43, 'K2-043', 'Iusto sed non suscipit est.', 20, '2017-08-02 13:05:45', NULL, NULL),
	(44, 'K2-044', 'Temporibus incidunt aut optio possimus praesentium.', 5, '2017-08-02 13:05:45', NULL, NULL),
	(45, 'K2-045', 'Qui qui iure nam molestiae.', 19, '2017-08-02 13:05:45', NULL, NULL),
	(46, 'K2-046', 'Corporis voluptas et at.', 5, '2017-08-02 13:05:45', NULL, NULL),
	(47, 'K2-047', 'Officiis ut voluptatem possimus.', 2, '2017-08-02 13:05:45', NULL, NULL),
	(48, 'K2-048', 'Quaerat alias provident ex eius.', 11, '2017-08-02 13:05:45', NULL, NULL),
	(49, 'K2-049', 'Fugit dolorem corporis nisi.', 19, '2017-08-02 13:05:45', NULL, NULL),
	(50, 'K2-050', 'Asperiores rem quo eaque perferendis rem.', 15, '2017-08-02 13:05:45', NULL, NULL),
	(51, 'K2-051', 'In corrupti et error atque quo.', 17, '2017-08-02 13:05:45', NULL, NULL),
	(52, 'K2-052', 'Nihil non voluptatibus qui.', 18, '2017-08-02 13:05:45', NULL, NULL),
	(53, 'K2-053', 'Vitae rem occaecati expedita.', 6, '2017-08-02 13:05:45', NULL, NULL),
	(54, 'K2-054', 'Fugiat labore nesciunt amet iusto et.', 19, '2017-08-02 13:05:45', NULL, NULL),
	(55, 'K2-055', 'Deleniti non numquam consequuntur nihil eius.', 5, '2017-08-02 13:05:45', NULL, NULL),
	(56, 'K2-056', 'Error molestiae harum omnis veritatis cum.', 4, '2017-08-02 13:05:45', NULL, NULL),
	(57, 'K2-057', 'Et molestiae earum itaque culpa rem ut.', 19, '2017-08-02 13:05:45', NULL, NULL),
	(58, 'K2-058', 'Fugiat voluptatem voluptatem qui in.', 5, '2017-08-02 13:05:45', NULL, NULL),
	(59, 'K2-059', 'Dolorum harum praesentium maiores illo.', 18, '2017-08-02 13:05:45', NULL, NULL),
	(60, 'K2-060', 'Accusamus cum ipsa quos in at.', 15, '2017-08-02 13:05:45', NULL, NULL);
/*!40000 ALTER TABLE `jabatan_fungsional_umum` ENABLE KEYS */;

-- Dumping structure for table simpeg.jabatan_struktural
CREATE TABLE IF NOT EXISTS `jabatan_struktural` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jabatan` varchar(100) DEFAULT NULL,
  `id_skpd` int(11) DEFAULT NULL,
  `id_eselon` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_skpd` (`id_skpd`),
  KEY `id_eselon` (`id_eselon`),
  CONSTRAINT `FK_jabatan_struktural_eselon` FOREIGN KEY (`id_eselon`) REFERENCES `eselon` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_jabatan_struktural_skpd` FOREIGN KEY (`id_skpd`) REFERENCES `skpd` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Master jabatan (struktural)';

-- Dumping data for table simpeg.jabatan_struktural: ~2 rows (approximately)
/*!40000 ALTER TABLE `jabatan_struktural` DISABLE KEYS */;
INSERT INTO `jabatan_struktural` (`id`, `jabatan`, `id_skpd`, `id_eselon`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Kepala Seksi Sarana dan Prasarana BKD', 2, 1, '2017-07-12 11:33:52', '2017-08-18 07:39:33', NULL),
	(2, 'Kepala Bidang Fisik dan Prasarana BAPPEDA', 1, 1, '2017-07-27 21:23:57', NULL, NULL);
/*!40000 ALTER TABLE `jabatan_struktural` ENABLE KEYS */;

-- Dumping structure for table simpeg.klp_jabatan_fungsional_tertentu
CREATE TABLE IF NOT EXISTS `klp_jabatan_fungsional_tertentu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kelompok` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Kelompok jabatan fungsional tertentu';

-- Dumping data for table simpeg.klp_jabatan_fungsional_tertentu: ~5 rows (approximately)
/*!40000 ALTER TABLE `klp_jabatan_fungsional_tertentu` DISABLE KEYS */;
INSERT INTO `klp_jabatan_fungsional_tertentu` (`id`, `kelompok`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Penata Ruang', '2017-07-26 16:43:23', '2017-07-26 17:45:01', NULL),
	(2, 'Penilai Pajak Bumi dan Bangunan', '2017-07-26 16:48:38', NULL, NULL),
	(3, 'Adikara Siaran', '2017-07-26 16:49:10', '2017-07-26 17:37:19', NULL),
	(5, 'Administrator Kesehatan', '2017-07-26 17:01:52', '2017-07-26 17:14:39', NULL);
/*!40000 ALTER TABLE `klp_jabatan_fungsional_tertentu` ENABLE KEYS */;

-- Dumping structure for table simpeg.klp_jabatan_fungsional_umum_1
CREATE TABLE IF NOT EXISTS `klp_jabatan_fungsional_umum_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(50) DEFAULT NULL,
  `kelompok` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `kode` (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='Kelompok jabatan fungsional umum (level 1)';

-- Dumping data for table simpeg.klp_jabatan_fungsional_umum_1: ~20 rows (approximately)
/*!40000 ALTER TABLE `klp_jabatan_fungsional_umum_1` DISABLE KEYS */;
INSERT INTO `klp_jabatan_fungsional_umum_1` (`id`, `kode`, `kelompok`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'K1-001', 'Aut distinctio et quaerat.', '2017-07-29 07:05:05', NULL, NULL),
	(2, 'K1-002', 'Corporis rerum non qui impedit.', '2017-07-29 07:05:05', NULL, NULL),
	(3, 'K1-003', 'Aut rerum eligendi aut.', '2017-07-29 07:05:05', NULL, NULL),
	(4, 'K1-004', 'Odio et perspiciatis.', '2017-07-29 07:05:05', NULL, NULL),
	(5, 'K1-005', 'Quidem quidem qui nam nesciunt.', '2017-07-29 07:05:05', NULL, NULL),
	(6, 'K1-006', 'Quia atque labore nisi quia.', '2017-07-29 07:05:05', NULL, NULL),
	(7, 'K1-007', 'Recusandae aspernatur amet laborum.', '2017-07-29 07:05:05', NULL, NULL),
	(8, 'K1-008', 'Facilis cupiditate maxime excepturi quo consequatur.', '2017-07-29 07:05:05', NULL, NULL),
	(9, 'K1-009', 'Dolorum est velit odit deleniti.', '2017-07-29 07:05:05', NULL, NULL),
	(10, 'K1-010', 'Et voluptatem laborum molestias asperiores minima.', '2017-07-29 07:05:05', NULL, NULL),
	(11, 'K1-011', 'Tempore ut et at.', '2017-07-29 07:05:05', NULL, NULL),
	(12, 'K1-012', 'Quia quis a.', '2017-07-29 07:05:05', NULL, NULL),
	(13, 'K1-013', 'Et minus laborum qui totam.', '2017-07-29 07:05:05', NULL, NULL),
	(14, 'K1-014', 'Voluptas optio modi nulla quos.', '2017-07-29 07:05:05', NULL, NULL),
	(15, 'K1-015', 'Maiores ut alias.', '2017-07-29 07:05:05', NULL, NULL),
	(16, 'K1-016', 'Facilis error est eius in consequatur.', '2017-07-29 07:05:06', NULL, NULL),
	(17, 'K1-017', 'Perferendis iusto enim voluptatum.', '2017-07-29 07:05:06', NULL, NULL),
	(18, 'K1-018', 'Corrupti aspernatur quisquam soluta aliquid quia.', '2017-07-29 07:05:06', NULL, NULL),
	(19, 'K1-019', 'Non quia eveniet reprehenderit.', '2017-07-29 07:05:06', NULL, NULL),
	(20, 'K1-020', 'Aut tempore possimus recusandae quo aut.', '2017-07-29 07:05:06', NULL, NULL);
/*!40000 ALTER TABLE `klp_jabatan_fungsional_umum_1` ENABLE KEYS */;

-- Dumping structure for table simpeg.klp_jabatan_fungsional_umum_2
CREATE TABLE IF NOT EXISTS `klp_jabatan_fungsional_umum_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(50) DEFAULT NULL,
  `kelompok` varchar(100) DEFAULT NULL,
  `id_klp_1` int(11) DEFAULT NULL COMMENT 'Kelompok jabatan fungsional umum (level 1)',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_klp_1` (`id_klp_1`),
  CONSTRAINT `FK_klp_jabatan_fungsional_umum_2_klp_jabatan_fungsional_umum_1` FOREIGN KEY (`id_klp_1`) REFERENCES `klp_jabatan_fungsional_umum_1` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='Kelompok jabatan fungsional umum (level 2)';

-- Dumping data for table simpeg.klp_jabatan_fungsional_umum_2: ~21 rows (approximately)
/*!40000 ALTER TABLE `klp_jabatan_fungsional_umum_2` DISABLE KEYS */;
INSERT INTO `klp_jabatan_fungsional_umum_2` (`id`, `kode`, `kelompok`, `id_klp_1`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'K2-001', 'In quos ratione voluptates cum magnam.', 20, '2017-08-02 09:00:56', NULL, NULL),
	(2, 'K2-002', 'Corrupti aut nihil est enim.', 15, '2017-08-02 09:00:56', NULL, NULL),
	(3, 'K2-003', 'Aliquid optio quia recusandae placeat.', 18, '2017-08-02 09:00:56', '2017-08-09 14:01:12', NULL),
	(4, 'K2-004', 'Aspernatur consectetur voluptatem occaecati nostrum.', 14, '2017-08-02 09:00:56', NULL, NULL),
	(5, 'K2-005', 'Illo repellat qui repellendus quis qui.', 18, '2017-08-02 09:00:56', NULL, NULL),
	(6, 'K2-006', 'Ut a voluptate.', 13, '2017-08-02 09:00:56', NULL, NULL),
	(7, 'K2-007', 'Quia explicabo fuga.', 20, '2017-08-02 09:00:56', NULL, NULL),
	(8, 'K2-008', 'Quos earum voluptatem et magni.', 17, '2017-08-02 09:00:56', NULL, NULL),
	(9, 'K2-009', 'Fugit quidem quam.', 19, '2017-08-02 09:00:57', NULL, NULL),
	(10, 'K2-010', 'Omnis repudiandae ut autem.', 11, '2017-08-02 09:00:57', NULL, NULL),
	(11, 'K2-011', 'Eos officia possimus alias quibusdam molestiae.', 19, '2017-08-02 09:00:57', NULL, NULL),
	(12, 'K2-012', 'Ducimus maiores libero dolorem.', 20, '2017-08-02 09:00:57', NULL, NULL),
	(13, 'K2-013', 'Eos quibusdam sunt amet itaque facilis.', 15, '2017-08-02 09:00:57', NULL, NULL),
	(14, 'K2-014', 'Fugit rerum sit eveniet nemo aut.', 16, '2017-08-02 09:00:57', NULL, NULL),
	(15, 'K2-015', 'Debitis quia occaecati quis facilis.', 20, '2017-08-02 09:00:57', NULL, NULL),
	(16, 'K2-016', 'Voluptas perferendis fugiat eos.', 10, '2017-08-02 09:00:57', NULL, NULL),
	(17, 'K2-017', 'Natus consequatur nobis.', 16, '2017-08-02 09:00:57', NULL, NULL),
	(18, 'K2-018', 'Ullam assumenda numquam non.', 20, '2017-08-02 09:00:57', NULL, NULL),
	(19, 'K2-019', 'Hic asperiores iste quia.', 11, '2017-08-02 09:00:57', NULL, NULL),
	(20, 'K2-020', 'Ut fuga debitis vel libero aut.', 15, '2017-08-02 09:00:57', NULL, NULL);
/*!40000 ALTER TABLE `klp_jabatan_fungsional_umum_2` ENABLE KEYS */;

-- Dumping structure for table simpeg.login_attempts
CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table simpeg.login_attempts: ~0 rows (approximately)
/*!40000 ALTER TABLE `login_attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `login_attempts` ENABLE KEYS */;

-- Dumping structure for table simpeg.pegawai
CREATE TABLE IF NOT EXISTS `pegawai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nip_lama` varchar(100) DEFAULT NULL,
  `nip_baru` varchar(100) DEFAULT NULL,
  `nama` varchar(200) DEFAULT NULL,
  `gelar_depan` varchar(200) NOT NULL DEFAULT '',
  `gelar_belakang` varchar(200) NOT NULL DEFAULT '',
  `tmp_lahir` varchar(100) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `jk` enum('L','P') DEFAULT NULL,
  `alamat` text,
  `nik` varchar(100) DEFAULT NULL,
  `npwp` varchar(100) DEFAULT NULL,
  `no_karpeg` varchar(100) DEFAULT NULL,
  `tmt_cpns` date DEFAULT NULL,
  `tmt_pns` date DEFAULT NULL,
  `no_katalog` varchar(50) DEFAULT NULL,
  `tipe_jabatan` enum('Struktural','Fungsional Umum','Fungsional Tertentu') DEFAULT NULL,
  `id_jabatan` int(11) DEFAULT NULL,
  `id_golongan` int(11) DEFAULT NULL,
  `foto` varchar(300) DEFAULT NULL,
  `id_pendidikan_jurusan` int(11) DEFAULT NULL,
  `id_skpd` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_jabatan` (`id_jabatan`),
  KEY `id_pendidikan_jurusan` (`id_pendidikan_jurusan`),
  KEY `id_skpd` (`id_skpd`),
  KEY `FK_pegawai_golongan` (`id_golongan`),
  CONSTRAINT `FK_pegawai_golongan` FOREIGN KEY (`id_golongan`) REFERENCES `golongan` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_pegawai_pendidikan_jurusan` FOREIGN KEY (`id_pendidikan_jurusan`) REFERENCES `pendidikan_jurusan` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_pegawai_skpd` FOREIGN KEY (`id_skpd`) REFERENCES `skpd` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Data pokok pegawai';

-- Dumping data for table simpeg.pegawai: ~0 rows (approximately)
/*!40000 ALTER TABLE `pegawai` DISABLE KEYS */;
INSERT INTO `pegawai` (`id`, `nip_lama`, `nip_baru`, `nama`, `gelar_depan`, `gelar_belakang`, `tmp_lahir`, `tgl_lahir`, `jk`, `alamat`, `nik`, `npwp`, `no_karpeg`, `tmt_cpns`, `tmt_pns`, `no_katalog`, `tipe_jabatan`, `id_jabatan`, `id_golongan`, `foto`, `id_pendidikan_jurusan`, `id_skpd`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, '123456789101112131415', '123456789101112131415', 'John Doe', '', 'S.Kom', 'Palopo', '1993-02-03', 'L', 'Jl. Akasia No.11, Lemo-Lemo Indah', '7373898292819980001', '7373827772828292920', '1234567263728171', '2017-06-12', '2017-07-12', 'A1', 'Struktural', 1, NULL, NULL, 7, 1, '2017-07-12 11:53:15', NULL, NULL);
/*!40000 ALTER TABLE `pegawai` ENABLE KEYS */;

-- Dumping structure for table simpeg.pendidikan
CREATE TABLE IF NOT EXISTS `pendidikan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pendidikan` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='Master pendidikan';

-- Dumping data for table simpeg.pendidikan: ~10 rows (approximately)
/*!40000 ALTER TABLE `pendidikan` DISABLE KEYS */;
INSERT INTO `pendidikan` (`id`, `pendidikan`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'SD', '2017-07-12 19:35:26', NULL, NULL),
	(2, 'SMP', '2017-07-12 11:35:50', NULL, NULL),
	(3, 'SMA', '2017-07-12 11:35:56', NULL, NULL),
	(4, 'SMK', '2017-07-12 11:48:42', NULL, NULL),
	(5, 'MA', '2017-07-12 11:48:46', NULL, NULL),
	(6, 'D1', '2017-07-12 11:49:49', NULL, NULL),
	(7, 'D2', '2017-07-12 11:49:52', NULL, NULL),
	(8, 'D3', '2017-07-12 11:49:55', NULL, NULL),
	(9, 'D4', '2017-07-12 11:49:59', NULL, NULL),
	(10, 'S1', '2017-07-12 11:50:06', NULL, NULL),
	(11, 'S2', '2017-07-12 11:50:09', NULL, NULL),
	(12, 'S3', '2017-07-12 11:50:12', NULL, NULL);
/*!40000 ALTER TABLE `pendidikan` ENABLE KEYS */;

-- Dumping structure for table simpeg.pendidikan_jurusan
CREATE TABLE IF NOT EXISTS `pendidikan_jurusan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jurusan` varchar(50) DEFAULT NULL,
  `id_pendidikan` int(11) DEFAULT NULL,
  `id_bidang_keilmuan` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_pendidikan` (`id_pendidikan`),
  KEY `id_bidang_keilmuan` (`id_bidang_keilmuan`),
  CONSTRAINT `FK_pendidikan_jurusan_bidang_keilmuan` FOREIGN KEY (`id_bidang_keilmuan`) REFERENCES `bidang_keilmuan` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_pendidikan_jurusan_pendidikan` FOREIGN KEY (`id_pendidikan`) REFERENCES `pendidikan` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='Master pendidikan jurusan';

-- Dumping data for table simpeg.pendidikan_jurusan: ~7 rows (approximately)
/*!40000 ALTER TABLE `pendidikan_jurusan` DISABLE KEYS */;
INSERT INTO `pendidikan_jurusan` (`id`, `jurusan`, `id_pendidikan`, `id_bidang_keilmuan`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'SD', 1, NULL, '2017-07-12 11:37:17', NULL, NULL),
	(2, 'SMP', 2, NULL, '2017-07-12 11:37:41', NULL, NULL),
	(3, 'IPA', 3, NULL, '2017-07-12 11:38:10', NULL, NULL),
	(4, 'IPS', 3, NULL, '2017-07-12 11:42:29', NULL, NULL),
	(5, 'Rekayasa Perangkat Lunak', 4, 1, '2017-07-12 11:50:43', NULL, NULL),
	(6, 'Teknik Komputer Jaringan', 4, 1, '2017-07-12 11:51:04', NULL, NULL),
	(7, 'Teknik Informatika', 10, 1, '2017-07-12 11:59:15', NULL, NULL);
/*!40000 ALTER TABLE `pendidikan_jurusan` ENABLE KEYS */;

-- Dumping structure for table simpeg.skpd
CREATE TABLE IF NOT EXISTS `skpd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skpd` varchar(300) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Master SKPD';

-- Dumping data for table simpeg.skpd: ~0 rows (approximately)
/*!40000 ALTER TABLE `skpd` DISABLE KEYS */;
INSERT INTO `skpd` (`id`, `skpd`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Badan Perencanaan Pembangunan Daerah', '2017-07-12 11:34:57', NULL, NULL),
	(2, 'Badan Kepegawaian Daerah', '2017-07-18 16:12:26', '2017-07-21 06:34:16', NULL);
/*!40000 ALTER TABLE `skpd` ENABLE KEYS */;

-- Dumping structure for table simpeg.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `nip` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `nip` (`nip`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- Dumping data for table simpeg.users: ~4 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `name`, `nip`) VALUES
	(1, '127.0.0.1', 'admin', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, 'moT/vobF9yd17eKrZs.1/u', 1268889823, 1503039294, 1, 'Administrator', NULL),
	(16, '127.0.0.1', 'akbar', '$2y$08$4TlzcBiblfoZNNHtmA1Ib.RElqlWlSANQ34.01sFQOqoujP3CWTe.', NULL, 'aksa.uncp@gmail.com', NULL, NULL, NULL, NULL, 1500237845, 1500295816, 1, 'Akbar Syarif', NULL),
	(17, '127.0.0.1', 'nita', '$2y$08$mjVQZLU1tnzJvlIR5Kvjg.cEsbenSNDRJrZp6zPDryhV4VV6HuUa2', NULL, 'nita@gmail.com', NULL, NULL, NULL, NULL, 1500294110, 1500372231, 1, 'Yunita Lestari', NULL),
	(18, '127.0.0.1', 'fajar', '$2y$08$b2LAsChrxCsjjzhbiKxe8OfYk9ADot.4BtDnLUY7lwJDH3GMUaNw2', NULL, 'fajar@gmail.com', NULL, NULL, NULL, NULL, 1500295640, 1500295780, 1, 'Fajar Hidayat', NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table simpeg.users_groups
CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- Dumping data for table simpeg.users_groups: ~4 rows (approximately)
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;
INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
	(1, 1, 1),
	(14, 16, 2),
	(15, 17, 1),
	(16, 18, 1);
/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;

-- Dumping structure for view simpeg.v_eselon
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_eselon` (
	`id` INT(11) NOT NULL,
	`eselon` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`gol_min` INT(11) NULL COMMENT 'Golongan minimal',
	`created_at` TIMESTAMP NULL,
	`updated_at` TIMESTAMP NULL,
	`deleted_at` TIMESTAMP NULL,
	`golongan_minimal` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`pangkat` VARCHAR(100) NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view simpeg.v_golongan
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_golongan` (
	`id` INT(11) NOT NULL,
	`golongan` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`pangkat` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`created_at` TIMESTAMP NULL,
	`updated_at` TIMESTAMP NULL,
	`deleted_at` TIMESTAMP NULL
) ENGINE=MyISAM;

-- Dumping structure for view simpeg.v_jabatan_fungsional_tertentu
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_jabatan_fungsional_tertentu` (
	`id` INT(11) NOT NULL,
	`jabatan` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`tingkat` ENUM('Terampil','Ahli') NULL COLLATE 'utf8_general_ci',
	`id_klp` INT(11) NULL,
	`id_golongan` INT(11) NULL,
	`golongan` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`pangkat` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`kelompok` VARCHAR(100) NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view simpeg.v_jabatan_fungsional_umum
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_jabatan_fungsional_umum` (
	`id` INT(11) NOT NULL,
	`kode` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`jabatan` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`id_klp_2` INT(11) NULL COMMENT 'Kelompok jabatan fungsional (level 2)',
	`kode2` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`kelompok2` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`id_klp_1` INT(11) NULL COMMENT 'Kelompok jabatan fungsional umum (level 1)',
	`kode1` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`kelompok1` VARCHAR(100) NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view simpeg.v_jabatan_struktural
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_jabatan_struktural` (
	`id` INT(11) NOT NULL,
	`jabatan` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`id_skpd` INT(11) NULL,
	`id_eselon` INT(11) NULL,
	`created_at` TIMESTAMP NULL,
	`eselon` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`skpd` VARCHAR(300) NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view simpeg.v_klp_jabatan_fungsional_tertentu
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_klp_jabatan_fungsional_tertentu` (
	`id` INT(11) NOT NULL,
	`kelompok` VARCHAR(100) NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view simpeg.v_klp_jabatan_fungsional_umum_1
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_klp_jabatan_fungsional_umum_1` (
	`id` INT(11) NOT NULL,
	`kode` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`kelompok` VARCHAR(100) NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view simpeg.v_klp_jabatan_fungsional_umum_2
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_klp_jabatan_fungsional_umum_2` (
	`id` INT(11) NOT NULL,
	`kode` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`kelompok` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`id_klp_1` INT(11) NULL COMMENT 'Kelompok jabatan fungsional umum (level 1)',
	`kode1` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`kelompok1` VARCHAR(100) NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view simpeg.v_pegawai
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_pegawai` (
	`id` INT(11) NOT NULL,
	`nip_lama` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`nip_baru` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`nama` VARCHAR(200) NULL COLLATE 'utf8_general_ci',
	`gelar_depan` VARCHAR(200) NOT NULL COLLATE 'utf8_general_ci',
	`gelar_belakang` VARCHAR(200) NOT NULL COLLATE 'utf8_general_ci',
	`nama_lengkap` TEXT NULL COLLATE 'utf8_general_ci',
	`tmp_lahir` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`tgl_lahir` DATE NULL,
	`jk` ENUM('L','P') NULL COLLATE 'utf8_general_ci',
	`alamat` TEXT NULL COLLATE 'utf8_general_ci',
	`nik` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`npwp` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`tmt_pns` DATE NULL,
	`no_katalog` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`tipe_jabatan` ENUM('Struktural','Fungsional Umum','Fungsional Tertentu') NULL COLLATE 'utf8_general_ci',
	`id_jabatan` INT(11) NULL,
	`foto` VARCHAR(300) NULL COLLATE 'utf8_general_ci',
	`id_pendidikan_jurusan` INT(11) NULL,
	`id_skpd` INT(11) NULL,
	`created_at` TIMESTAMP NULL,
	`updated_at` TIMESTAMP NULL,
	`deleted_at` TIMESTAMP NULL,
	`jurusan` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`id_pendidikan` INT(11) NULL,
	`id_bidang_keilmuan` INT(11) NULL,
	`pendidikan` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`bidang_keilmuan` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`skpd` VARCHAR(300) NULL COLLATE 'utf8_general_ci',
	`tmt_cpns` DATE NULL,
	`no_karpeg` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`id_golongan` INT(11) NULL
) ENGINE=MyISAM;

-- Dumping structure for view simpeg.v_skpd
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_skpd` (
	`id` INT(11) NOT NULL,
	`skpd` VARCHAR(300) NULL COLLATE 'utf8_general_ci',
	`created_at` TIMESTAMP NULL,
	`updated_at` TIMESTAMP NULL,
	`deleted_at` TIMESTAMP NULL
) ENGINE=MyISAM;

-- Dumping structure for view simpeg.v_user
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_user` (
	`id` INT(11) UNSIGNED NOT NULL,
	`ip_address` VARCHAR(45) NOT NULL COLLATE 'utf8_general_ci',
	`username` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`password` VARCHAR(255) NOT NULL COLLATE 'utf8_general_ci',
	`salt` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`email` VARCHAR(100) NOT NULL COLLATE 'utf8_general_ci',
	`activation_code` VARCHAR(40) NULL COLLATE 'utf8_general_ci',
	`forgotten_password_code` VARCHAR(40) NULL COLLATE 'utf8_general_ci',
	`remember_code` VARCHAR(40) NULL COLLATE 'utf8_general_ci',
	`active` TINYINT(1) UNSIGNED NULL,
	`name` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`nip` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`forgotten_password_time` DATETIME NULL,
	`created_on` DATETIME NULL,
	`last_login` DATETIME NULL,
	`group_id` MEDIUMINT(8) UNSIGNED NOT NULL,
	`group_name` VARCHAR(20) NOT NULL COLLATE 'utf8_general_ci',
	`group_description` VARCHAR(100) NOT NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view simpeg.v_eselon
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_eselon`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `v_eselon` AS SELECT
	eselon.id,
	eselon.eselon,
	eselon.gol_min,
	eselon.created_at,
	eselon.updated_at,
	eselon.deleted_at,
	golongan.golongan AS golongan_minimal,
	golongan.pangkat
FROM
	eselon
INNER JOIN golongan ON eselon.gol_min = golongan.id ;

-- Dumping structure for view simpeg.v_golongan
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_golongan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `v_golongan` AS SELECT
golongan.id,
golongan.golongan,
golongan.pangkat,
golongan.created_at,
golongan.updated_at,
golongan.deleted_at
FROM
golongan ;

-- Dumping structure for view simpeg.v_jabatan_fungsional_tertentu
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_jabatan_fungsional_tertentu`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `v_jabatan_fungsional_tertentu` AS SELECT
jabatan_fungsional_tertentu.id,
jabatan_fungsional_tertentu.jabatan,
jabatan_fungsional_tertentu.tingkat,
jabatan_fungsional_tertentu.id_klp,
jabatan_fungsional_tertentu.id_golongan,
golongan.golongan,
golongan.pangkat,
klp_jabatan_fungsional_tertentu.kelompok
FROM
jabatan_fungsional_tertentu
INNER JOIN golongan ON jabatan_fungsional_tertentu.id_golongan = golongan.id
INNER JOIN klp_jabatan_fungsional_tertentu ON jabatan_fungsional_tertentu.id_klp = klp_jabatan_fungsional_tertentu.id
ORDER BY
klp_jabatan_fungsional_tertentu.kelompok ASC,
jabatan_fungsional_tertentu.tingkat ASC,
golongan.golongan ASC,
jabatan_fungsional_tertentu.jabatan ASC ;

-- Dumping structure for view simpeg.v_jabatan_fungsional_umum
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_jabatan_fungsional_umum`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `v_jabatan_fungsional_umum` AS SELECT
jabatan_fungsional_umum.id,
jabatan_fungsional_umum.kode,
jabatan_fungsional_umum.jabatan,
jabatan_fungsional_umum.id_klp_2,
klp_jabatan_fungsional_umum_2.kode AS kode2,
klp_jabatan_fungsional_umum_2.kelompok AS kelompok2,
klp_jabatan_fungsional_umum_2.id_klp_1,
klp_jabatan_fungsional_umum_1.kode AS kode1,
klp_jabatan_fungsional_umum_1.kelompok AS kelompok1
FROM
jabatan_fungsional_umum
INNER JOIN klp_jabatan_fungsional_umum_2 ON jabatan_fungsional_umum.id_klp_2 = klp_jabatan_fungsional_umum_2.id
INNER JOIN klp_jabatan_fungsional_umum_1 ON klp_jabatan_fungsional_umum_2.id_klp_1 = klp_jabatan_fungsional_umum_1.id ;

-- Dumping structure for view simpeg.v_jabatan_struktural
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_jabatan_struktural`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `v_jabatan_struktural` AS SELECT
jabatan_struktural.id,
jabatan_struktural.jabatan,
jabatan_struktural.id_skpd,
jabatan_struktural.id_eselon,
jabatan_struktural.created_at,
eselon.eselon,
skpd.skpd
FROM
jabatan_struktural
INNER JOIN eselon ON jabatan_struktural.id_eselon = eselon.id
INNER JOIN skpd ON jabatan_struktural.id_skpd = skpd.id
ORDER BY
jabatan_struktural.jabatan ASC ;

-- Dumping structure for view simpeg.v_klp_jabatan_fungsional_tertentu
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_klp_jabatan_fungsional_tertentu`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `v_klp_jabatan_fungsional_tertentu` AS SELECT
klp_jabatan_fungsional_tertentu.id,
klp_jabatan_fungsional_tertentu.kelompok
FROM
klp_jabatan_fungsional_tertentu ;

-- Dumping structure for view simpeg.v_klp_jabatan_fungsional_umum_1
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_klp_jabatan_fungsional_umum_1`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `v_klp_jabatan_fungsional_umum_1` AS SELECT
klp_jabatan_fungsional_umum_1.id,
klp_jabatan_fungsional_umum_1.kode,
klp_jabatan_fungsional_umum_1.kelompok
FROM
klp_jabatan_fungsional_umum_1 ;

-- Dumping structure for view simpeg.v_klp_jabatan_fungsional_umum_2
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_klp_jabatan_fungsional_umum_2`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `v_klp_jabatan_fungsional_umum_2` AS SELECT
klp_jabatan_fungsional_umum_2.id,
klp_jabatan_fungsional_umum_2.kode,
klp_jabatan_fungsional_umum_2.kelompok,
klp_jabatan_fungsional_umum_2.id_klp_1,
klp_jabatan_fungsional_umum_1.kode AS kode1,
klp_jabatan_fungsional_umum_1.kelompok AS kelompok1
FROM
klp_jabatan_fungsional_umum_2
INNER JOIN klp_jabatan_fungsional_umum_1 ON klp_jabatan_fungsional_umum_2.id_klp_1 = klp_jabatan_fungsional_umum_1.id ;

-- Dumping structure for view simpeg.v_pegawai
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_pegawai`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `v_pegawai` AS SELECT
pegawai.id,
pegawai.nip_lama,
pegawai.nip_baru,
pegawai.nama,
pegawai.gelar_depan,
pegawai.gelar_belakang,
CONCAT(pegawai.gelar_depan,' ',pegawai.nama,' ',pegawai.gelar_belakang) AS nama_lengkap,
pegawai.tmp_lahir,
pegawai.tgl_lahir,
pegawai.jk,
pegawai.alamat,
pegawai.nik,
pegawai.npwp,
pegawai.tmt_pns,
pegawai.no_katalog,
pegawai.tipe_jabatan,
pegawai.id_jabatan AS id_jabatan,
pegawai.foto,
pegawai.id_pendidikan_jurusan,
pegawai.id_skpd,
pegawai.created_at,
pegawai.updated_at,
pegawai.deleted_at,
pendidikan_jurusan.jurusan,
pendidikan_jurusan.id_pendidikan,
pendidikan_jurusan.id_bidang_keilmuan,
pendidikan.pendidikan,
bidang_keilmuan.bidang_keilmuan,
skpd.skpd,
pegawai.tmt_cpns,
pegawai.no_karpeg,
pegawai.id_golongan
FROM pegawai
LEFT JOIN pendidikan_jurusan ON pendidikan_jurusan.id = pegawai.id_pendidikan_jurusan
LEFT JOIn pendidikan ON pendidikan.id = pendidikan_jurusan.id_pendidikan
LEFT JOIN bidang_keilmuan ON bidang_keilmuan.id = pendidikan_jurusan.id_bidang_keilmuan
LEFT JOIN skpd ON skpd.id = pegawai.id_skpd ;

-- Dumping structure for view simpeg.v_skpd
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_skpd`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `v_skpd` AS SELECT
skpd.id,
skpd.skpd,
skpd.created_at,
skpd.updated_at,
skpd.deleted_at
FROM
skpd ;

-- Dumping structure for view simpeg.v_user
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_user`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `v_user` AS SELECT
	users.id AS id, users.ip_address, users.username, users.password, users.salt, users.email, users.activation_code, users.forgotten_password_code, users.remember_code, users.active,
	-- additional data
	users.name, users.nip,
	-- forgotten password timestamp
	from_unixtime(users.forgotten_password_time) AS forgotten_password_time,
	-- created on
	from_unixtime(users.created_on) AS created_on,
	-- last login
	from_unixtime(users.last_login) AS last_login,
	-- group
	users_groups.group_id,groups.name AS group_name, groups.description AS group_description
FROM users
JOIN users_groups ON users_groups.user_id = users.id
JOIN groups ON groups.id = users_groups.group_id ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
